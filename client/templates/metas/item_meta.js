Template.itemMeta.events({
    'click #botonMeta': function(e) {
        e.preventDefault();
        
    }
});

Template.itemMeta.helpers({
    cursos: function() {
        var cursos = this.cursosMeta;
        var tamaño = cursos.length;
        
        var i = 0;
        while (i < tamaño){
            var cursoId = cursos[i];
            cursos[i] = Cursos.findOne({_id: cursoId});
            i++;
        }
        return cursos;
    },
    
    contadorCursos: function() {
        return this.cursosMeta.length;
    },
    
    metaAprobada: function() {
        var userId = Meteor.userId();
        
        var metaId = this._id;
        
        var desarrollo = Desarrollos.findOne({userId: userId, metaId: metaId});
        
        if(!desarrollo) {
            return false;
        } else {
            if(desarrollo.aprobado)
                return true;
        }
    }
});