Template.crearMetas.onCreated(function() {
    Session.set('erroresCrearMeta', {});
});

Template.crearMetas.events({
    'submit form': function (e) {
        e.preventDefault();
        
        var strings = [];
        var radio = "";
        var checkbox = document.forms[0];
        
        //recorremos la variable checkbox identificando si los checkbox del form estan checkeados, 
        //si es así, los guardamos en su string correspondiente
        var pos = -1;
        for (var i = 0; i < checkbox.length; i++){
            if (checkbox[i].checked) {
                if(!checkbox[i+1].textContent)
                    radio = checkbox[i].nextSibling.data;
                else {
                    pos++;
                    strings[pos] = checkbox[i+1].textContent;
                }
            }
        }
        
        //validacion de selección minima de cursos
        var seleccion = strings[1];
        if (!seleccion)
            return lanzarError('No has seleccionado la cantidad mínima de cursos');
        
        var meta = {
            nombreMeta: $(e.target).find('[id=meta]').val(),
            descripcion: $(e.target).find('[id=descripcion]').val(),
            dificultad: radio,
            cursosMeta: strings
        };
        
        var errores = validarMeta(meta);
        if (errores.nombreMeta || errores.descripcion || errores.dificultad)
            return Session.set('erroresCrearMeta', errores);
        
        
        //llamada hacia metodo insertarMeta que funciona desde el servidor
        Meteor.call('insertarMeta', meta, function(error, result) {
            if (error)
                return lanzarError(error.reason);
                
            if (result.existeMeta)
                lanzarError('Esta meta ya ha sido creada');
                
            Router.go('listadoMetas', {_id: result._id});
        });
    },
    
    'click #descripcion': function() {
        var noClick = document.getElementById('descripcion').value;
        if(noClick==="Ingresa una descripción de la Meta...")
            document.getElementById('descripcion').value = "";
    }
});

Template.crearMetas.helpers({
    cursos: function() {
        //cuando el administrador publique cursos modificar la funcion
        return Cursos.find({estado: "publicado"});
    },
    
    nombreArea: function() {
        var area = Areas.findOne({_id: this.areaId});
        return area.nombreArea;
    },
    
    prerrequisitos: function () {
        var pre = this.prerrequisitos;
        var tamaño = pre.length;
        
        var i = 0;
        while (i < tamaño) {
            pre[i] = Cursos.findOne({_id: pre[i]});
            i++;
        }
        return pre;
    },
    
    mensajeError: function(field) {
        return Session.get('erroresCrearMeta') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresCrearMeta') [field] ? 'has-error' : '';
    }
});