Template.itemPregunta.events({

});

Template.itemPregunta.helpers({
    alternativas: function() {
        return this.tipo=="Alternativas";
    },

    desarrollo: function() {
        return this.tipo=="Desarrollo";
    },

    verdadero: function() {
        return this.tipo=="Verdadero y Falso";
    },

    numero: function() {
        return Number(this._id)+1;
    }
});