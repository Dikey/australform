Template.paginaContenido.events({
    'click #boton-atras': function(e) {
        e.preventDefault();

        var orden = this.orden;

        orden--;
        
        if(orden == 0 ) {

            return Router.go('paginaModulo', {_id: this.moduloId});

        } else {

            var contenidoAnterior = Contenidos.findOne( { $and: [ {moduloId: this.moduloId}, {orden: orden} ] });
            
            var contenidoAnteriorId = contenidoAnterior._id;
            
            return Router.go('paginaContenido', {_id: contenidoAnteriorId});
        }
    },

    'click #boton-siguiente': function(e) {
        e.preventDefault();

        var orden = this.orden;
        
        orden++;

        var cantidadContenidos = Contenidos.find({moduloId: this.moduloId}).count();

        // ------------- Funcionalidad Timer -----------
        
        if(Roles.userIsInRole(Meteor.user(), 'user')) {
            var desarrollo = {
                contenidoId: this._id
            }
        
            Meteor.call('realizarContenido', desarrollo, function(error, result) {
                if (error)
                return lanzarError(error.reason);
                if (result.yaDesarrollado)
                return lanzarError('Este contenido ya ha sido realizado');
            });
        }
         
        // -------------------- Fin --------------------------

        //si se terminaron los contenidos redireccionar a prueba del módulo
        if(orden>cantidadContenidos) {

            var moduloId = Modulos.findOne({_id: this.moduloId})._id;

            Router.go('paginaPruebaModulo', {_id: moduloId});

        } else {

            
            var contenidoSiguiente = Contenidos.findOne( { $and: [ {moduloId: this.moduloId}, {orden: orden} ] });
            
            //preguntar si el siguiente está aprobado
            
            var contenidoSiguienteId = contenidoSiguiente._id;

            Router.go('paginaContenido', {_id: contenidoSiguienteId});
            
        }
    },

    'click #pruebaModulo': function(e) {
        e.preventDefault();

        var moduloId = Modulos.findOne({_id: this.moduloId})._id;

        Router.go('paginaPruebaModulo', {_id: moduloId});
    },

    'click #botonPaginaModulo': function(e) {
        e.preventDefault();

        var moduloId = Modulos.findOne({_id: this.moduloId})._id;

        Router.go('paginaModulo', {_id: moduloId});
    },

    'click #addPdf': function(e) {
        e.preventDefault();

        var contenido = this;

        Router.go('addArchivo', {_id: contenido._id});
    },

    'click #addAudio': function(e) {
        e.preventDefault();

        var contenido = this;

        Router.go('addAudio', {_id: contenido._id});
    },

    'click #addVideo': function(e) {
        e.preventDefault();

        var contenido = this;

        Router.go('addVideo', {_id: contenido._id});
    },

    //vistas de edicion

    'click #edicionArchivo': function(e) {
        e.preventDefault();

        Router.go('edicionArchivo', {_id: this.pdfId});
    },

    'click #edicionAudio': function(e) {
        e.preventDefault();

        Router.go('edicionAudio', {_id: this.audioId});
    },

    'click #edicionVideo': function(e) {
        e.preventDefault();

        //var url = this.url;

        //var newUrl = url.substring(32, 43);

        //Router.go('edicionVideo', {url: newUrl});

        // Realizar el método 'click #eliminar' directamente desde aquí

        // Nuevo método eliminar ----------------------

        var contenidoId = document.getElementById("idContenido").textContent;

        var url = this.url;
        var ordenRecurso = this.ordenRecurso;
        var ordenVideo = this.ordenVideo;

        var videos = {
            url: url,
            idContenido: contenidoId,
            ordenRecurso: ordenRecurso,
            ordenVideo: ordenVideo
        }
        
        if (confirm("¿Seguro quieres eliminar este video?")) {

            //llamada al método de edición de Pdf desde el servidor
            Meteor.call('eliminarRecursoVideo', videos, function(error, result) {
                if (error)
                    return lanzarError(error.reason);
                
                //Router.go('paginaContenido', {_id: idContenido});
            });
        }

        // Fin nuevo método eliminar ----------------------

    },

    'click #subirRecurso': function(e) {
        var ordenRecurso = e.currentTarget.name;
        var idRecurso = e.currentTarget.getAttribute("code");
        var tipo = e.currentTarget.getAttribute("tipo");
        var idContenido = document.getElementById("idContenido").textContent;

        recurso = {
            ordenRecurso: Number(ordenRecurso),
            idRecurso: idRecurso,
            tipo: tipo,
            idContenido: idContenido
        }

        //llamada al método de edición de orden del recurso
        Meteor.call('subirRecurso', recurso, function(error, result) {
            if (error)
                return lanzarError(error.reason);
        });
    },

    'click #bajarRecurso': function(e) {
        var ordenRecurso = e.currentTarget.name;
        var idRecurso = e.currentTarget.getAttribute("code");
        var tipo = e.currentTarget.getAttribute("tipo");
        var idContenido = document.getElementById("idContenido").textContent;

        recurso = {
            ordenRecurso: Number(ordenRecurso),
            idRecurso: idRecurso,
            tipo: tipo,
            idContenido: idContenido
        }

        //llamada al método de edición de orden del recurso
        Meteor.call('bajarRecurso', recurso, function(error, result) {
            /*
            if (error)
                return lanzarError(error.reason);
            */
        });
    }
});

Template.paginaContenido.helpers({
    //funcion que permite verificar si eres el creador del contenido
    propioContenido: function() {
        return this.userId === Meteor.userId();
    },

    estadoPendiente: function() {
        var contenido = this;

        var modulo = Modulos.findOne({_id: contenido.moduloId});

        var curso = Cursos.findOne({_id: modulo.cursoId});

        if (curso.estado==="pendiente")
            return true;
    },

    link: function() {
        var video = this.url;
        
        var embed = "embed/";
        
        var link = video.substring(0,24) + embed + video.substring(32);
        return link;
    },

    videos: function() {
        var videos = this.videos;

        return videos;
    },

    pdfs: function() {
        return Pdfs.find({_id: this.pdfId});
    },
    
    audios: function() {
        return Audios.find({_id: this.audioId});
    },

    comentarios: function() {
        return Comentarios.find({contenidoId: this._id}, {sort: {fechaComentario: -1}});
    },
    
    contadorComentarios: function() {
        return Comentarios.find({contenidoId: this._id}).count();
    },

    ordenModulo: function() {
        var modulo = Modulos.findOne({_id: this.moduloId});

        return modulo.orden;
    },

    nombreModulo: function() {
        var modulo = Modulos.findOne({_id: this.moduloId});

        return modulo.nombreModulo;
    },

    contenidos: function() {

        var contenidoActualId = this._id;

        //Devuelve todos los contenidos del modulo a excepción del contenido actual
        //return Contenidos.find({ $and: [ {moduloId: this.moduloId}, {_id: { $ne: contenidoActualId}}]}, {sort: {fechaContenido: 1}});
        
        //Devuelve todos los contenidos del modulo
        return Contenidos.find({moduloId: this.moduloId}, {sort: {orden: 1}});
    },

    moduloTienePrueba: function() {
        var contenido = this;

        var modulo = Modulos.findOne({_id: contenido.moduloId});

        return modulo.prueba;
    },

    duracionPrueba: function() {
        var contenido = this;

        var modulo = Modulos.findOne({_id: contenido.moduloId});

        return modulo.prueba.duracion;
    },

    ordenModulo: function() {
        var contenido = this;

        var modulo = Modulos.findOne({_id: contenido.moduloId});

        return modulo.orden;
    },

    //nueva función de visualización de recursos en los contenidos
    recursos: function() {

        var recursos = [];

        var cantidadRecursos = this.pdfs.length+this.audios.length+this.videos.length;

        var posRecurso = 1;
        while(posRecurso<=cantidadRecursos){

            var j = 0;
            while(j<this.pdfs.length) {
                if(this.pdfs[j].ordenRecurso==posRecurso)
                    recursos[posRecurso-1] = this.pdfs[j];
                j++;      
            }

            var k = 0;
            while(k<this.audios.length) {
                if(this.audios[k].ordenRecurso==posRecurso)
                    recursos[posRecurso-1] = this.audios[k];
                k++;      
            }

            var l = 0;
            while(l<this.videos.length) {
                if(this.videos[l].ordenRecurso==posRecurso)
                    recursos[posRecurso-1] = this.videos[l];
                l++;
            }

            posRecurso++;
        }

        return recursos;
    },

    esVideo: function() {
        if(this.url)
            return true;
    },

    esPdf: function() {
        if(this.pdfId)
            return true;
    },

    esAudio: function() {
        if(this.audioId)
            return true;
    },

    idArchivo: function() {
        return this.pdfId;
    },

    idVideo: function() {
        return this.url;
    },

    idAudio: function() {
        return this.audioId;
    },

    noEsPrimero: function() {
        if (this.ordenRecurso!=1)
        return true;
    },

    noEsUltimoPdf: function() {

        var pdfId = this.pdfId;

        var contenido = Contenidos.findOne({ "pdfs.pdfId": pdfId});

        var sizeTotal = contenido.pdfs.length + contenido.audios.length + contenido.videos.length;

        if (this.ordenRecurso!=sizeTotal)
        return true
    },

    noEsUltimoAudio: function() {

        var audioId = this.audioId;

        var contenido = Contenidos.findOne({ "audios.audioId": audioId});

        var sizeTotal = contenido.pdfs.length + contenido.audios.length + contenido.videos.length;

        if (this.ordenRecurso!=sizeTotal)
        return true
    },

    noEsUltimoVideo: function() {

        var url = this.url;

        var contenido = Contenidos.findOne({ "videos.url": url});

        var sizeTotal = contenido.pdfs.length + contenido.audios.length + contenido.videos.length;

        if (this.ordenRecurso!=sizeTotal)
        return true
    },

    contenidoActual: function() {

        /*
        var contenidoActual = this;

        //var idContenido = template.$("#idContenido");

        var idContenido = document.getElementById('idContenido');

        if(contenidoActual._id == idContenido)
            return true;
        */
    }
});