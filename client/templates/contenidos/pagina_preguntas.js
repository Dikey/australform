Template.paginaPreguntas.events({

});

Template.paginaPreguntas.helpers({
    preguntas: function() {

        var contenido = this;
        var contenidoId = contenido._id;

        return Tests.find({contenidoId: contenidoId});
    },

    cantidad: function() {
        var contenido = this;
        var contenidoId = contenido._id;

        return Tests.find({contenidoId: contenidoId}).count();
    }
});