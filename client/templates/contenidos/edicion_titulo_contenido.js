Template.edicionTituloContenido.onCreated(function() {
    Session.set('erroresEditarContenido', {});
});

Template.edicionTituloContenido.events({
    'submit form': function(e)  {
        e.preventDefault();

        /*
        Nueva funcionalidad 
        

        var contenido = this;
        var cantidadPdfs = contenido.pdfs.length;

        var pos = 0;
        while(pos<cantidadPdfs) {
            var posOrden = pos+1;
            var archivoContenido = template.$("#archivoContenido"+posOrden).text;

            pos++;
        }

        var idContenido = this._id;

        var moduloId = Modulos.findOne({_id: this.moduloId})._id;

        var filesPdf = document.getElementById("pdfContenido").files;
        var filesAudio = document.getElementById("audioContenido").files;
        var linkVideo = $(e.target).find('[name=video]').val();

        var cantidadRecursos = 0;

        if(!filesPdf[0])
            var pdfs = [];
        else {
            for(var i = 0, ln = filesPdf.length; i < ln; i++) {
                var pdf = Pdfs.insert(filesPdf[i], function(err, fileObj) {
                    //Insertado nuevo documento con ID fileObj._id
                });
            }
            cantidadRecursos++;

            var pdfs = [
                {
                    pdfId: pdf._id,
                    ordenPdf: 1,
                    ordenRecurso: cantidadRecursos
                }
            ]
        }

        if(!filesAudio[0])
            var audios = [];
        else {
            for(var i = 0, ln = filesAudio.length; i < ln; i++) {
                var audio = Audios.insert(filesAudio[i], function(err, fileObj) {
                    //Insertado nuevo documento con ID fileObj._id
                });
            }
            cantidadRecursos++;

            var audios = [
                {
                    audioId: audio._id,
                    ordenAudio: 1,
                    ordenRecurso: cantidadRecursos
                }
            ]
        }

        if(!linkVideo)
            var videos = [];
        else {
            cantidadRecursos++;

            var videos = [
                {
                    url: linkVideo,
                    ordenVideo: 1,
                    ordenRecurso: cantidadRecursos
                }
            ]
        }

        //preguntamos por la cantidad de recursos media en los diferentes arreglos

        */

        var contenido = {
            tituloContenido: $(e.target).find('[name=titulo]').val(),
            duracionContenido: $(e.target).find('[name=duracion]').val()
        };

        var errores = validarContenido(contenido);
        if(errores.tituloContenido || errores.duracionContenido)
            return Session.set('erroresEditarContenido', errores);

       var idContenido = this._id;

        Contenidos.update(idContenido, {$set: contenido}, function(error) {
            if (error) {
                lanzarError(error.reason);
            } else {
                Router.go('paginaContenido', {_id: idContenido});
            }
        });
    },

    'click #eliminar': function(e) {
        /*
        e.preventDefault();
    
        var idContenido = this._id;

        var modulo = Modulos.findOne({_id: this.moduloId});

        var cursoId = Cursos.findOne({_id: modulo.cursoId})._id;
        
        if (confirm("¿Seguro quieres eliminarlo?")) {
                
            Contenidos.remove(idContenido);
            Router.go('paginaCurso', {_id: cursoId});
        }

        */
    }
});

Template.edicionTituloContenido.helpers({
    pdfs: function() {
        /*
        var pdfs = [];
        var cantidad = this.pdfs.length;

        var i = 0;
        while(i<cantidad) {
            pdfs[i] = Pdfs.findOne({_id: this.pdfs[i].pdfId});
            i++;
        }
        
        return pdfs;
        */

        return this.pdfs;
    },

    videos: function() {
        return this.videos;
    },

    audios: function() {
        /*
        var audios = [];
        var cantidad = this.audios.length;

        var i = 0;
        while(i<cantidad) {
            audios[i] = Audios.findOne({_id: this.audios[i].audioId});
            i++;
        }

        return audios;
        */

        return this.audios;
    },

    nombrePdf: function() {
        var pdfId = this.pdfId;

        var pdf = Pdfs.findOne({_id: pdfId});

        return pdf.original.name;
    },

    nombreAudio: function() {
        var audioId = this.audioId;

        var audio = Audios.findOne({_id: audioId});

        return audio.original.name;
    },
    
    mensajeError: function(field) {
        return Session.get('erroresEditarContenido') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresEditarContenido') [field] ? 'has-error' : '';
    }
});