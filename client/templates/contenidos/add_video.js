Template.addVideo.onCreated(function() {
    Session.set('erroresAddVideo', {});
});

Template.addVideo.events({
    'submit form': function(e) {
        e.preventDefault();

        var idContenido = this._id;

        var linkVideo = $(e.target).find('[name=video]').val();

        if(!linkVideo)
            var videos = {
                idContenido: idContenido,
                url: ""
            }
        else {
            var videos = {
                idContenido: idContenido,
                url: linkVideo
            }
        }

        var errores = validarVideo(videos);
        if(errores.url)
            return Session.set('erroresAddVideo', errores);

        //llamada al método de creación de contenido desde el servidor
        Meteor.call('insertarRecursoVideo', videos, function(error, result) {
            if (error)
                return lanzarError(error.reason);
            
            Router.go('paginaContenido', {_id: result._id});
        });
    }
});

Template.addVideo.helpers({
    mensajeError: function(field) {
        return Session.get('erroresAddVideo') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresAddVideo') [field] ? 'has-error' : '';
    }
});