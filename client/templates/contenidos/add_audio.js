Template.addAudio.onCreated(function() {
    Session.set('erroresAddAudio', {});
});

Template.addAudio.events({
    'submit form': function(e) {
        e.preventDefault();

        var idContenido = this._id;

        var filesAudio = document.getElementById("audioContenido").files;

        if(!filesAudio[0])
            var audios = {
                idContenido: idContenido,
                audioId: ""
            }
        else {
            for(var i = 0, ln = filesAudio.length; i < ln; i++) {
                var audio = Audios.insert(filesAudio[i], function(err, fileObj) {
                    //Insertado nuevo documento con ID fileObj._id
                });
                var audios = {
                    idContenido: idContenido,
                    audioId: audio._id
                }
            }
        }

        var errores = validarAudio(audios);
        if(errores.audioId)
            return Session.set('erroresAddAudio', errores);

        //llamada al método de creación de contenido desde el servidor
        Meteor.call('insertarRecursoAudio', audios, function(error, result) {
            if (error)
                return lanzarError(error.reason);
            
            Router.go('paginaContenido', {_id: result._id});
        });
    }
});

Template.addAudio.helpers({
    mensajeError: function(field) {
        return Session.get('erroresAddAudio') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresAddAudio') [field] ? 'has-error' : '';
    }
});