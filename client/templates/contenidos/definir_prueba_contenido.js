Template.definirPruebaContenido.onCreated(function() {
    Session.set('erroresDefinirPruebaContenido', {});
});

Template.definirPruebaContenido.events({
    'submit form': function(e, template) {
        e.preventDefault();

        var contenido = this;
        var idContenido = contenido._id;

        var total = Tests.find({contenidoId: idContenido}).count();
        var cantidadPreguntas = $(e.target).find('[id=cantidad-preguntas]').val();

        if(cantidadPreguntas>total)
            return lanzarError("No tienes la cantidad suficiente de preguntas agregadas");

        var check1 = template.$("#test1")[0];
        var check2 = template.$("#test2")[0];

        var alternativas = false;
        var verdadero = false;

        if (check1.checked)
            alternativas = true;

        if(check2.checked)
            verdadero = true;

        var prueba = {
            aceptaAlternativas: alternativas,
            aceptaVerdaderoYFalso: verdadero,
            cantidadPreguntas: cantidadPreguntas
        }

        //-->validación

        Contenidos.update(idContenido,  {$set: {prueba: prueba} }, function(error) {
            if (error) {
                lanzarError(error.reason);
            } else {
                Router.go('paginaContenido', {_id: idContenido});
            }
        });
    }
});

Template.definirPruebaContenido.helpers({
    cantidad: function() {
        var contenido = this;
        var contenidoId = contenido._id;

        return Tests.find({contenidoId: contenidoId}).count();
    },

    mensajeError: function(field) {
        return Session.get('erroresDefinirPruebaContenido') [field];
    },

    claseError: function(field) {
        return !!Session.get('erroresDefinirPruebaContenido') [field] ? 'has-error' : '';
    }
});