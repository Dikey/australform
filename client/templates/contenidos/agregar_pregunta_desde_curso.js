Template.agregarPreguntaDesdeCurso.onCreated(function() {
    Session.set('erroresAgregarPregunta', {});
});

Template.agregarPreguntaDesdeCurso.events({
    'submit form': function(e, template) {
        e.preventDefault();

        var cursoId = this._id;
        var tipo = "Desarrollo";

        var pregunta = {
            cursoId: cursoId,
            tipo: tipo,
            pregunta: $(e.target).find('[id=pregunta-desarrollo]').val()
        }

        var errores = validarPreguntaDesarrollo(pregunta);
        if(errores.cursoId || errores.tipo || errores.pregunta)
        return Session.set('erroresAgregarPregunta', errores);

        Meteor.call('insertarPreguntaDesarrollo', pregunta, function(error, result) {
            if (error)
                return lanzarError(error.reason);
                
            Router.go('paginaCurso', {_id: cursoId});
        });
    }
    
});

Template.agregarPreguntaDesdeCurso.helpers({

    cursos: function() {
        var userId = Meteor.userId();
        //return Cursos.find({ $and: [{userId: userId}, {estado: "pendiente"}] });
        return Cursos.find({userId: userId});
    },

    modulos: function() {
        var userId = Meteor.userId();
        return Modulos.find({userId: userId});
    },

    contenidos: function() {
        var userId = Meteor.userId();
        return Contenidos.find({userId: userId});
    },

    mensajeError: function(field) {
        return Session.get('erroresAgregarPregunta') [field];
    },

    claseError: function(field) {
        return !!Session.get('erroresAgregarPregunta') [field] ? 'has-error' : '';
    }
});