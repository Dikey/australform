Template.editarPregunta.onCreated(function() {
    Session.set('erroresEditarPregunta', {});
});

Template.editarPregunta.events({
    'submit form': function(e, template) {
        e.preventDefault();

        var idPregunta = this._id;
        var tipo = this.tipo;
        var idContenido = this.contenidoId;

        if(tipo=="Alternativas") {
            var pregunta = {
                contenidoId: idContenido,
                tipo: tipo,
                enunciado: $(e.target).find('[id=enunciado]').val(),
                correcta: $(e.target).find('[id=correcta]').val(),
                alt1: $(e.target).find('[id=alt1]').val(),
                alt2: $(e.target).find('[id=alt2]').val(),
                alt3: $(e.target).find('[id=alt3]').val()
            }

            var errores = validarPreguntaAlternativa(pregunta);
            if(errores.contenidoId || errores.tipo || errores.enunciado || errores.correcta || errores.alt1 || errores.alt2 || errores.alt3)
                return Session.set('erroresEditarPregunta', errores);

            Tests.update(idPregunta, {$set: pregunta}, function(error) {
                if (error) {
                    lanzarError(error.reason);
                } else {
                    Router.go('paginaPreguntas', {_id: idContenido});
                }
            });
        }

        if(tipo=="Verdadero y Falso") {
            var pregunta = {
                contenidoId: idContenido,
                tipo: tipo,
                sentencia: $(e.target).find('[id=sentencia]').val(),
                resultado: $(e.target).find('[id=seleccion-resultado]').val()
            }

            var errores = validarPreguntaVerdadero(pregunta);
            if(errores.contenidoId || errores.tipo || errores.sentencia || errores.resultado)
            return Session.set('erroresAgregarPregunta', errores);

            Tests.update(idPregunta, {$set: pregunta}, function(error) {
                if (error) {
                    lanzarError(error.reason);
                } else {
                    Router.go('paginaPreguntas', {_id: idContenido});
                }
            });
        }

        if(tipo=="Desarrollo") {

            var cursoId = this.cursoId;

            var pregunta = {
                cursoId: cursoId,
                tipo: tipo,
                pregunta: $(e.target).find('[id=pregunta-desarrollo]').val()
            }

            var errores = validarPreguntaDesarrollo(pregunta);
            if(errores.cursoId || errores.tipo || errores.pregunta)
            return Session.set('erroresAgregarPregunta', errores);

            Tests.update(idPregunta, {$set: pregunta}, function(error) {
                if (error) {
                    lanzarError(error.reason);
                } else {
                    Router.go('paginaPreguntasDesarrollo', {_id: cursoId});
                }
            });
        }
    },

    'click #eliminar': function(e) {
        e.preventDefault();

        var tipo = this.tipo;
        var idPregunta = this._id;

        if (confirm("¿Seguro quieres eliminar esta categoría?")) {
                
            Tests.remove(idPregunta);
        }

        if(tipo=="Desarrollo") {

            var cursoId = this.cursoId;
            Router.go('paginaPreguntasDesarrollo', {_id: cursoId});

        } else {

            var contenidoId = this.contenidoId;
            Router.go('paginaPreguntas', {_id: contenidoId});

        }
    }
});

Template.editarPregunta.helpers({
    esDesarrollo: function() {
        var tipo = this.tipo;

        if(tipo=="Desarrollo")
            return true;
    },

    esAlternativa: function() {
        var tipo = this.tipo;

        if(tipo=="Alternativas")
            return true;
    },

    esVerdadero: function() {
        var tipo = this.tipo;

        if(tipo=="Verdadero y Falso")
            return true;
    },

    mensajeError: function(field) {
        return Session.get('erroresEditarPregunta') [field];
    },

    claseError: function(field) {
        return !!Session.get('erroresEditarPregunta') [field] ? 'has-error' : '';
    }
})