Template.edicionVideo.onCreated(function() {
    Session.set('erroresEdicionVideo', {});
});

Template.edicionVideo.events({
    'submit form': function(e) {
        e.preventDefault();

        var url = this.url;

        var exp = "https://www.youtube.com/watch?v=";

        var newUrl = exp.concat(url);

        var vacio = document.getElementById("video").value;

        //--
        var contenido = Contenidos.findOne({ "videos.url": newUrl});

        var idContenido = contenido._id;
        //--

        if(!vacio)
            var videos = {
                antiguoVideo: newUrl,
                url: ""
            }
        else {

            if (vacio==newUrl)
            return Router.go('paginaContenido', {_id: idContenido});

            var videos = {
                antiguoVideo: newUrl,
                url: vacio
            }
        }

        var errores = validarVideo(videos);
        if(errores.url)
            return Session.set('erroresEdicionVideo', errores);

        //llamada al método de creación de contenido desde el servidor
        Meteor.call('editarRecursoVideo', videos, function(error, result) {
            if (error)
                return lanzarError(error.reason);
            
            Router.go('paginaContenido', {_id: idContenido});
        });
    },

    'click #eliminar': function(e) {
        e.preventDefault();

        var url = this.url;

        var exp = "https://www.youtube.com/watch?v=";

        var newUrl = exp.concat(url);

        var contenido = Contenidos.findOne({ "videos.url": newUrl});

        var idContenido = contenido._id;

        var videos = {
            url: newUrl,
            idContenido: idContenido
        }
        
        if (confirm("¿Seguro quieres eliminar este video?")) {

            //llamada al método de edición de Pdf desde el servidor
            Meteor.call('eliminarRecursoVideo', videos, function(error, result) {
                if (error)
                    return lanzarError(error.reason);
                
                Router.go('paginaContenido', {_id: idContenido});
            });
        }
    }
});

Template.edicionVideo.helpers({
    newUrl: function() {
        var url = this.url;

        var exp = "https://www.youtube.com/watch?v=";

        return exp.concat(url);
    },

    tituloContenido: function() {
        var url = this.url;
        var exp = "https://www.youtube.com/watch?v=";

        var con = exp.concat(url);

        var contenido = Contenidos.findOne({ "videos.url": con});

        return contenido.tituloContenido;
    },


    mensajeError: function(field) {
        return Session.get('erroresEdicionVideo') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresEdicionVideo') [field] ? 'has-error' : '';
    }
});