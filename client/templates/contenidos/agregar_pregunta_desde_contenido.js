Template.agregarPreguntaDesdeContenido.onCreated(function() {
    Session.set('erroresAgregarPregunta', {});
});

Template.agregarPreguntaDesdeContenido.events({
    'submit form': function(e, template) {
        e.preventDefault();

        var contenidoSeleccionado = this;
        var idContenido = contenidoSeleccionado._id;

        var tipo = $(e.target).find('[id=seleccion-tipo]').val();

        if(!tipo)
            return lanzarError("Debes elegir un tipo de alternativa");

        if(tipo=="Alternativas") {
            var pregunta = {
                contenidoId: idContenido,
                tipo: tipo,
                enunciado: $(e.target).find('[id=enunciado]').val(),
                correcta: $(e.target).find('[id=correcta]').val(),
                alt1: $(e.target).find('[id=alt1]').val(),
                alt2: $(e.target).find('[id=alt2]').val(),
                alt3: $(e.target).find('[id=alt3]').val()
            }

            var errores = validarPreguntaAlternativa(pregunta);
            if(errores.contenidoId || errores.tipo || errores.enunciado || errores.correcta || errores.alt1 || errores.alt2 || errores.alt3)
                return Session.set('erroresAgregarPregunta', errores);

            Meteor.call('insertarPreguntaAlternativa', pregunta, function(error, result) {
                if (error)
                    return lanzarError(error.reason);
                    
                Router.go('paginaContenido', {_id: idContenido});
            });
        }

        if(tipo=="Verdadero y Falso") {
            var pregunta = {
                contenidoId: idContenido,
                tipo: tipo,
                sentencia: $(e.target).find('[id=sentencia]').val(),
                resultado: $(e.target).find('[id=seleccion-resultado]').val()
            }

            var errores = validarPreguntaVerdadero(pregunta);
            if(errores.contenido || errores.tipo || errores.sentencia || errores.resultado)
            return Session.set('erroresAgregarPregunta', errores);

            Meteor.call('insertarPreguntaVerdadero', pregunta, function(error, result) {
                if (error)
                    return lanzarError(error.reason);
                    
                Router.go('paginaContenido', {_id: idContenido});
            });
        }
    },

    "change #seleccion-tipo": function(event, template) {

        var selectValue = template.$("#seleccion-tipo").val();

        if(selectValue=="Alternativas") {
            document.getElementById("div-alternativas").style.display = "block";
            document.getElementById("div-verdadero").style.display = "none";
        }
        
        if(selectValue=="Verdadero y Falso") {
            document.getElementById("div-verdadero").style.display = "block";
            document.getElementById("div-alternativas").style.display = "none";
        }

        if(selectValue=="") {
            document.getElementById("div-verdadero").style.display = "none";
            document.getElementById("div-alternativas").style.display = "none";
        }
    }
});

Template.agregarPreguntaDesdeContenido.helpers({
    contenidos: function() {
        var userId = Meteor.userId();
        return Contenidos.find({userId: userId});
    },

    mensajeError: function(field) {
        return Session.get('erroresAgregarPregunta') [field];
    },

    claseError: function(field) {
        return !!Session.get('erroresAgregarPregunta') [field] ? 'has-error' : '';
    }
});