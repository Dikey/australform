Template.edicionArchivo.onCreated(function() {
    Session.set('erroresEdicionArchivo', {});
});

Template.edicionArchivo.events({
    'submit form': function(e) {
        e.preventDefault();

        var pdfId = this._id;

        var contenido = Contenidos.findOne({ "pdfs.pdfId": pdfId});

        var idContenido = contenido._id;
        
        var filesPdf = document.getElementById("pdfContenido").files;

        var vacio = document.getElementById("nombrePdf").value;

        //si está vacío el campo
        if(!vacio)
            var pdfs = {
                idContenido: idContenido,
                pdfId: ""
            }
        else {

            if(!filesPdf[0])
            return Router.go('paginaContenido', {_id: idContenido});


            for(var i = 0, ln = filesPdf.length; i < ln; i++) {
                var pdf = Pdfs.insert(filesPdf[i], function(err, fileObj) {
                    //Insertado nuevo documento con ID fileObj._id
                });
                
                var pdfs = {
                    antiguoId: pdfId,
                    pdfId: pdf._id
                }
            }
        }

        var errores = validarPdf(pdfs);
        if(errores.pdfId)
            return Session.set('erroresEdicionArchivo', errores);

        //llamada al método de edición de Pdf desde el servidor
        Meteor.call('editarRecursoPdf', pdfs, function(error, result) {
            if (error)
                return lanzarError(error.reason);
            
            Router.go('paginaContenido', {_id: idContenido});
        });
    },

    'click #eliminar': function(e) {
        e.preventDefault();

        var pdfId = this._id;

        var contenido = Contenidos.findOne({ "pdfs.pdfId": pdfId});

        var idContenido = contenido._id;

        var pdfs = {
            pdfId: this._id,
            idContenido: idContenido
        }
        
        if (confirm("¿Seguro quieres eliminar este archivo?")) {

            //llamada al método de edición de Pdf desde el servidor
            Meteor.call('eliminarRecursoPdf', pdfs, function(error, result) {
                if (error)
                    return lanzarError(error.reason);
                
                Router.go('paginaContenido', {_id: idContenido});
            });
        }
    }
});

Template.edicionArchivo.helpers({
    pdfs: function() {
        var pdfId = this._id;

        return Pdfs.find({_id: pdfId});
    },

    tituloContenido: function() {
        var pdfId = this._id;

        var contenido = Contenidos.findOne({ "pdfs.pdfId": pdfId});

        return contenido.tituloContenido;
    },

    mensajeError: function(field) {
        return Session.get('erroresEdicionArchivo') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresEdicionArchivo') [field] ? 'has-error' : '';
    }
})