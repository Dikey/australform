Template.edicionTest.onCreated(function () {
    Session.set('erroresEditarTest', {});
});

Template.edicionTest.events({
    'submit form': function(e){
        e.preventDefault();
        
        var modulo = {
            nombreModulo: this.nombreModulo
        }
        
        var test = {
            tituloTest: $(e.target).find('[id=tituloTest]').val(),
            preguntas: []
        };
        
        var contador = 0;
        var pos = -1;
        
        var form = document.forms[0];
        
        for (var i = 0; i < form.length; i++){
            
            if (form[i].checked) {
                pos++;
                
                var pregunta = pos+1;
                var operador1 = pos*4+1;
                var operador2 = pos*4+2;
                var operador3 = pos*4+3;
                var operador4 = pos*4+4;
                
                var preguntas = {
                    _id: pos+1,
                    op1: operador1,
                    op2: operador2,
                    op3: operador3,
                    op4: operador4,
                    correcta: form[i+1].value,
                    enunciado: $(e.target).find('[id=pregunta'+pregunta+']').val(),
                    alt1: $(e.target).find('[id=alt'+operador1+']').val(),
                    alt2: $(e.target).find('[id=alt'+operador2+']').val(),
                    alt3: $(e.target).find('[id=alt'+operador3+']').val(),
                    alt4: $(e.target).find('[id=alt'+operador4+']').val()
                };
                
                test.preguntas[pos] = preguntas;
            }
        }
        
        //validamos la selección de un módulo y el ingreso de las preguntas y respuestas del test
        var errores = validarTest(modulo, test);
        if(errores.nombreModulo || errores.tituloTest || errores.pregunta1 || errores.respuestaCorrecta1 ||
          errores.pregunta2 || errores.respuestaCorrecta2 || errores.pregunta3 || errores.respuestaCorrecta3 ||
          errores.pregunta4 || errores.respuestaCorrecta4 || errores.pregunta5 || errores.respuestaCorrecta5 ||
          errores.pregunta6 || errores.respuestaCorrecta6 || errores.pregunta7 || errores.respuestaCorrecta7 ||
          errores.pregunta8 || errores.respuestaCorrecta8 || errores.pregunta9 || errores.respuestaCorrecta9 ||
          errores.pregunta10 || errores.respuestaCorrecta10)
            return Session.set('erroresEditarTest', errores);
        
        var idModulo = this._id;
        
        Modulos.update(idModulo, {$set: {test: test} }, function(error) {
            if (error) {
                lanzarError(error.reason);
            } else {
                Router.go('paginaModulo', {_id: idModulo});
            }
        });
    },
    
    'click .eliminar': function(e) {
        e.preventDefault();
    
        var idModulo = this._id;
        
        if (confirm("¿Seguro quieres eliminar el test?")) {
            Modulos.update(idModulo, {$unset: {test: 1} }, function(error) {
                if (error) {
                    lanzarError(error.reason);
                } else {
                    Router.go('paginaModulo', {_id: idModulo});
                }
            });
        }
    }
});

Template.edicionTest.helpers({
    modulos: function() {
        var userId = Meteor.userId();
        //realizamos una consulta que devuelve todos los documentos dentro de la colección módulos que son del profesor
        //y que no poseen un test ingresado
        return Modulos.find({$and: [{userId: userId}, {test: {$exists: false}}]});
    },
    
    preguntas: function() {
        return this.test.preguntas;
    },
    
    mensajeError: function(field) {
        var id = this._id;
        return Session.get('erroresEditarTest') [field+id];
    },
    
    claseError: function(field) {
        var id = this._id;
        return !!Session.get('erroresEditarTest') [field+id] ? 'has-error' : '';
    },
    
    mensajeErrorTitulo: function(field) {
        return Session.get('erroresEditarTest') [field];
    },
    
    claseErrorTitulo: function(field) {
        return !!Session.get('erroresEditarTest') [field] ? 'has-error' : '';
    }
});
