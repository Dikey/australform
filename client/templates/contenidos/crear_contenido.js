Template.crearContenido.onCreated(function () {
    Session.set('erroresCrearContenido', {});
});

Template.crearContenido.events({
    'submit form': function(e){
        e.preventDefault();

        //------- nuevo formulario de contenidos --------------

        var modulo = $(e.target).find('[id=seleccion-modulo]').val();

        if(!modulo) {
            var moduloId = "";
        } else {
            var moduloId = Modulos.findOne({nombreModulo: modulo})._id;
        }

        var filesPdf = document.getElementById("pdfContenido").files;
        var filesAudio = document.getElementById("audioContenido").files;
        var linkVideo = $(e.target).find('[name=video]').val();

        var cantidadRecursos = 0;

        if(!filesPdf[0])
            var pdfs = [];
        else {
            for(var i = 0, ln = filesPdf.length; i < ln; i++) {
                var pdf = Pdfs.insert(filesPdf[i], function(err, fileObj) {
                    //Insertado nuevo documento con ID fileObj._id
                });
            }
            cantidadRecursos++;

            var pdfs = [
                {
                    pdfId: pdf._id,
                    orden: 1,
                    ordenRecurso: cantidadRecursos
                }
            ]
        }

        if(!filesAudio[0])
            var audios = [];
        else {
            for(var i = 0, ln = filesAudio.length; i < ln; i++) {
                var audio = Audios.insert(filesAudio[i], function(err, fileObj) {
                    //Insertado nuevo documento con ID fileObj._id
                });
            }
            cantidadRecursos++;

            var audios = [
                {
                    audioId: audio._id,
                    orden: 1,
                    ordenRecurso: cantidadRecursos
                }
            ]
        }

        if(!linkVideo)
            var videos = [];
        else {
            cantidadRecursos++;

            var videos = [
                {
                    url: linkVideo,
                    orden: 1,
                    ordenRecurso: cantidadRecursos
                }
            ]
        }

        var contenido = {
            tituloContenido: $(e.target).find('[name=titulo]').val(),
            moduloId: moduloId,
            duracionContenido: $(e.target).find('[name=duracion]').val(),
            pdfs: pdfs,
            videos: videos,
            audios: audios
        };
        
        // --------------- Fin -----------------------------
        
        if (contenido.video) {
            var expReg = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
            var match = contenido.video.match(expReg);
                if (match && match[2].length == 11) {
                    //la url es válida
                } else {
                    //la url es inválida
                    return lanzarError("La URL del video ingresada es inválida");
                }
        }

        var errores = validarContenido(contenido);
        if(errores.tituloContenido || errores.moduloId || errores.duracionContenido)
            return Session.set('erroresCrearContenido', errores);

        //llamada al método de creación de contenido desde el servidor
        Meteor.call('insertarContenido', contenido, function(error, result) {
            if (error)
                return lanzarError(error.reason);
            
            if (result.existeContenido)
                lanzarError('Este contenido ya ha sido creado');
            
            Router.go('paginaContenido', {_id: result._id});
        });

    }
});

Template.crearContenido.helpers({
    modulos: function() {
        var userId = Meteor.userId();
        return Modulos.find({userId: userId});
    },
    
    mensajeError: function(field) {
        return Session.get('erroresCrearContenido') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresCrearContenido') [field] ? 'has-error' : '';
    }
});



