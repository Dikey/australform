Template.addArchivo.onCreated(function() {
    Session.set('erroresAddArchivo', {});
});

Template.addArchivo.events({
    'submit form': function(e) {
        e.preventDefault();

        var idContenido = this._id;

        var filesPdf = document.getElementById("pdfContenido").files;

        if(!filesPdf[0])
            var pdfs = {
                idContenido: idContenido,
                pdfId: ""
            }
        else {
            for(var i = 0, ln = filesPdf.length; i < ln; i++) {
                var pdf = Pdfs.insert(filesPdf[i], function(err, fileObj) {
                    //Insertado nuevo documento con ID fileObj._id
                });
                
                var pdfs = {
                    idContenido: idContenido,
                    pdfId: pdf._id
                }
            }
        }

        var errores = validarPdf(pdfs);
        if(errores.pdfId)
            return Session.set('erroresAddArchivo', errores);

        //llamada al método de creación de contenido desde el servidor
        Meteor.call('insertarRecursoPdf', pdfs, function(error, result) {
            if (error)
                return lanzarError(error.reason);
            
            Router.go('paginaContenido', {_id: result._id});
        });
    }
});

Template.addArchivo.helpers({
    mensajeError: function(field) {
        return Session.get('erroresAddArchivo') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresAddArchivo') [field] ? 'has-error' : '';
    }
})