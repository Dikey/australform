Template.agregarPregunta.onCreated(function() {
    Session.set('erroresAgregarPregunta', {});
});

Template.agregarPregunta.events({
    'submit form': function(e, template) {
        e.preventDefault();

        var contenido = $(e.target).find('[id=seleccion-contenido]').val();
        if(!contenido) {
            var idContenido = "";
        } else {
            var contenidoSeleccionado = Contenidos.findOne({tituloContenido: contenido});
            var idContenido = contenidoSeleccionado._id;
        }

        var tipo = $(e.target).find('[id=seleccion-tipo]').val();

        if(!tipo)
            return lanzarError("Debes elegir un tipo de alternativa");

        if(tipo=="Alternativas") {
            var pregunta = {
                contenidoId: idContenido,
                tipo: tipo,
                enunciado: $(e.target).find('[id=enunciado]').val(),
                correcta: $(e.target).find('[id=correcta]').val(),
                alt1: $(e.target).find('[id=alt1]').val(),
                alt2: $(e.target).find('[id=alt2]').val(),
                alt3: $(e.target).find('[id=alt3]').val()
            }

            var errores = validarPreguntaAlternativa(pregunta);
            if(errores.contenidoId || errores.tipo || errores.enunciado || errores.correcta || errores.alt1 || errores.alt2 || errores.alt3)
                return Session.set('erroresAgregarPregunta', errores);

            Meteor.call('insertarPreguntaAlternativa', pregunta, function(error, result) {
                if (error)
                    return lanzarError(error.reason);
                    
                Router.go('paginaContenido', {_id: idContenido});
            });
        }

        if(tipo=="Verdadero y Falso") {
            var pregunta = {
                contenidoId: idContenido,
                tipo: tipo,
                sentencia: $(e.target).find('[id=sentencia]').val(),
                resultado: $(e.target).find('[id=seleccion-resultado]').val()
            }

            var errores = validarPreguntaVerdadero(pregunta);
            if(errores.contenidoId || errores.tipo || errores.sentencia || errores.resultado)
            return Session.set('erroresAgregarPregunta', errores);

            Meteor.call('insertarPreguntaVerdadero', pregunta, function(error, result) {
                if (error)
                    return lanzarError(error.reason);
                    
                Router.go('paginaContenido', {_id: idContenido});
            });
        }

        if(tipo=="Desarrollo") {

            var nombreCurso = $(e.target).find('[id=seleccion-curso]').val()

            if(!nombreCurso) {
                var cursoId = "";
            } else {
                var cursoId = Cursos.findOne({nombreCurso: nombreCurso})._id;
            }

            var pregunta = {
                cursoId: cursoId,
                tipo: tipo,
                pregunta: $(e.target).find('[id=pregunta-desarrollo]').val()
            }

            var errores = validarPreguntaDesarrollo(pregunta);
            if(errores.cursoId || errores.tipo || errores.pregunta)
            return Session.set('erroresAgregarPregunta', errores);

            Meteor.call('insertarPreguntaDesarrollo', pregunta, function(error, result) {
                if (error)
                    return lanzarError(error.reason);
                    
                Router.go('paginaCurso', {_id: cursoId});
            });
        }
    },

    "change #seleccion-curso": function(e, template) {
        document.getElementById("div-tipo").style.display = "block";

        var select = document.getElementById("seleccion-modulo");

        select.options.length = 0;

        select.options[select.options.length] = new Option("Selecciona un Módulo", "");

        var nombreCurso = template.$("#seleccion-curso").val();

        var cursoId = Cursos.findOne({nombreCurso: nombreCurso})._id;

        var modulosSize = Modulos.find({cursoId: cursoId}).count();

        var i = 0;
        while(i < modulosSize) {
            var nombreModulo = Modulos.findOne({cursoId: cursoId}, {skip: i}).nombreModulo;

            select.options[select.options.length] = new Option(nombreModulo, nombreModulo);
            i++;
        }
    },

    "change #seleccion-modulo": function(event, template) {

        document.getElementById("div-contenidos").style.display = "block";

        var select = document.getElementById("seleccion-contenido");

        select.options.length = 0;

        select.options[select.options.length] = new Option("Selecciona un Contenido", "");

        var nombreModulo = template.$("#seleccion-modulo").val();

        var moduloId = Modulos.findOne({nombreModulo: nombreModulo})._id;

        var contenidosSize = Contenidos.find({moduloId: moduloId}).count();

        var i = 0;
        while(i < contenidosSize) {
            var tituloContenido = Contenidos.findOne({moduloId: moduloId}, {skip: i}).tituloContenido;

            select.options[select.options.length] = new Option(tituloContenido, tituloContenido);
            i++;
        }
    },

    "change #seleccion-tipo": function(event, template) {

        var selectValue = template.$("#seleccion-tipo").val();

        if(selectValue=="Alternativas") {
            document.getElementById("div-modulos").style.display = "block";

            document.getElementById("div-alternativas").style.display = "block";
            document.getElementById("div-verdadero").style.display = "none";
            document.getElementById("div-desarrollo").style.display = "none";
        }
        
        if(selectValue=="Verdadero y Falso") {
            document.getElementById("div-modulos").style.display = "block";

            document.getElementById("div-verdadero").style.display = "block";
            document.getElementById("div-alternativas").style.display = "none";
            document.getElementById("div-desarrollo").style.display = "none";
        }

        if(selectValue=="Desarrollo") {
            document.getElementById("div-modulos").style.display = "none";
            document.getElementById("div-contenidos").style.display = "none";

            document.getElementById("div-verdadero").style.display = "none";
            document.getElementById("div-alternativas").style.display = "none";
            document.getElementById("div-desarrollo").style.display = "block";
        }

        if(selectValue=="") {
            document.getElementById("div-modulos").style.display = "none";

            document.getElementById("div-verdadero").style.display = "none";
            document.getElementById("div-alternativas").style.display = "none";
            document.getElementById("div-desarrollo").style.display = "none";
        }
    }
});

Template.agregarPregunta.helpers({

    cursos: function() {
        var userId = Meteor.userId();
        //return Cursos.find({ $and: [{userId: userId}, {estado: "pendiente"}] });
        return Cursos.find({userId: userId});
    },

    modulos: function() {
        var userId = Meteor.userId();
        return Modulos.find({userId: userId});
    },

    contenidos: function() {
        var userId = Meteor.userId();
        return Contenidos.find({userId: userId});
    },

    mensajeError: function(field) {
        return Session.get('erroresAgregarPregunta') [field];
    },

    claseError: function(field) {
        return !!Session.get('erroresAgregarPregunta') [field] ? 'has-error' : '';
    }
});