Template.edicionAudio.onCreated(function() {
    Session.set('erroresEdicionAudio', {});
});

Template.edicionAudio.events({
    'submit form': function(e) {
        e.preventDefault();

        var audioId= this._id;

        var contenido = Contenidos.findOne({ "audios.audioId": audioId});

        var idContenido = contenido._id;

        var filesAudio = document.getElementById("audioContenido").files;

        var vacio = document.getElementById("audio").value;

        if(!vacio)
            var audios = {
                idContenido: idContenido,
                audioId: ""
            }
        else {

            if(!filesAudio[0])
                return Router.go('paginaContenido', {_id: idContenido});

            for(var i = 0, ln = filesAudio.length; i < ln; i++) {
                var audio = Audios.insert(filesAudio[i], function(err, fileObj) {
                    //Insertado nuevo documento con ID fileObj._id
                });
                var audios = {
                    antiguoAudio: audioId,
                    audioId: audio._id
                }
            }
        }

        var errores = validarAudio(audios);
        if(errores.audioId)
            return Session.set('erroresEdicionAudio', errores);

        //llamada al método de edición de Audio desde el servidor
        Meteor.call('editarRecursoAudio', audios, function(error, result) {
            if (error)
                return lanzarError(error.reason);
            
            Router.go('paginaContenido', {_id: idContenido});
        });
    },

    'click #eliminar': function(e) {
        e.preventDefault();

        var audioId= this._id;

        var contenido = Contenidos.findOne({ "audios.audioId": audioId});

        var idContenido = contenido._id;

        var audios = {
            audioId: this._id,
            idContenido: idContenido
        }
        
        if (confirm("¿Seguro quieres eliminar este audio?")) {

            //llamada al método de edición de Pdf desde el servidor
            Meteor.call('eliminarRecursoAudio', audios, function(error, result) {
                if (error)
                    return lanzarError(error.reason);
                
                Router.go('paginaContenido', {_id: idContenido});
            });
        }
    }
});

Template.edicionAudio.helpers({
    audios: function() {
        var audioId = this._id;

        return Audios.find({_id: audioId});
    },

    tituloContenido: function() {
        var audioId = this._id;

        var contenido = Contenidos.findOne({ "audios.audioId": audioId});

        return contenido.tituloContenido;
    },

    mensajeError: function(field) {
        return Session.get('erroresEdicionAudio') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresEdicionAudio') [field] ? 'has-error' : '';
    }
});