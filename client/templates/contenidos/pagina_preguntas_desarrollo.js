Template.paginaPreguntasDesarrollo.events({

});

Template.paginaPreguntasDesarrollo.helpers({
    preguntasDesarrollo: function() {

        var curso = this;
        var cursoId = curso._id;

        return Tests.find({cursoId: cursoId});
    },

    cantidadDesarrollo: function() {
        var curso = this;
        var cursoId = curso._id;

        return Tests.find({cursoId: cursoId}).count();
    },

    cantidad: function() {
        var contenidoId = this._id;

        return Tests.find({contenidoId: contenidoId}).count();
    },

    modulos: function() {
        var curso = this;
        var cursoId = curso._id;

        return Modulos.find({cursoId: cursoId});
    },

    contenidos: function() {
        return Contenidos.find({moduloId: this._id});
    },

    preguntas: function() {
        var contenidoId = this._id;

        return Tests.find({contenidoId: contenidoId});
    }
});