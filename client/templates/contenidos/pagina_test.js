Template.paginaTest.events({
    'submit form': function(e){
        e.preventDefault();
        var strings = [];
        var pos = -1;
        
        //guardamos los elementos del form en una variable
        var radios = document.forms[0];
        var boton = -5;
        
        //recorremos la variable identificando si los radios estan checkeados, 
        //si es así, los guardamos en su string correspondiente
        for (var i = 0; i < radios.length; i++){
            if (radios[i].checked) {
                pos++;
                boton += 5;
                
                var objeto = {
                    pregunta: radios[boton].textContent,
                    respuesta: radios[i].nextSibling.data
                };
                strings[pos] = objeto;
            }
        }
        
        //guardamos el tamaño del arreglo de respuestas correctas
        var tamañoRespuestas = this.test.preguntas.length-5;
        
        var cantidadCorrectas = 0;
        
        //comparamos las alterntivas elegidas con las respuestas correctas
        var ciclo1 = 0;
        while (ciclo1 < tamañoRespuestas){
            var pregunta = strings[ciclo1].pregunta-1;
            if (strings[ciclo1].respuesta===this.test.preguntas[pregunta].correcta)
                cantidadCorrectas += 1;
            ciclo1++;
        }
        
        //preguntamos si tiene la totalidad de las respuestas correctas
        if (cantidadCorrectas===tamañoRespuestas){
            
            finalizarDesarrolloModulo(this);
            
            //buscamos el módulo que depende de este
            var siguiente = Modulos.findOne({requisitoModuloId: this._id});
            
            //nos redirigimos al listado de areas
            Router.go('paginaCurso', {_id: this.cursoId});
            
            //si no hay más modulos que dependan, se habrá llegado a la pruefa final del curso
            if(!siguiente)
                lanzarMensaje("Felicitaciones!, Has aprobado el módulo "+this.nombreModulo+" Has desbloqueado la Prueba Final del curso");
                else {
            
                //lanzamos el mensaje de aprobación del módulo
                lanzarMensaje("Felicitaciones!, Has aprobado el módulo "+this.nombreModulo+" Has desbloqueado el módulo "
                          +siguiente.nombreModulo);
                }
            
        } else {
            //mensaje de no aprobado y redirijimos al contenido del mismo módulo
            Router.go('paginaModulo', {_id: this._id});
            
            lanzarMensaje("Ups!, no has aprobado el módulo, pero no te desanimes puedes intentarlo las veces que quieras");
        }
    }
});

Template.paginaTest.helpers({
    tests: function() {
        var i = 0;
        var arreglo = [];
        var tamaño = this.test.preguntas.length;
        
        var numerosUsados = [];
        
        while (i < tamaño-5) {
            
            var random = Math.floor(Math.random() * (tamaño-0)) + 0;
            var ciclo = 0;
            var repetido = true;
            while (repetido != false){
                while(ciclo < numerosUsados.length) {
                    if (random==numerosUsados[ciclo]) {
                        random = Math.floor(Math.random() * (tamaño-0)) + 0;
                        ciclo = 0;
                    } else {
                        ciclo++;
                    }
                }
                repetido = false;
            }
            numerosUsados[i] = random;
            
            var test = {
                _id: i,
                numero: this.test.preguntas[random]._id,
                pregunta: this.test.preguntas[random].enunciado,
                alt1: this.test.preguntas[random].alt1,
                alt2: this.test.preguntas[random].alt2,
                alt3: this.test.preguntas[random].alt3,
                alt4: this.test.preguntas[random].alt4
            };
            
            arreglo[i] = test;
            i++;
        }
        
        return arreglo;
    },
    
    id: function() {
        return this._id+1;
    },
    
    numero: function() {
        return this.numero;
    }
});