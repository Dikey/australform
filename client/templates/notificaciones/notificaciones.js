Template.notificaciones.helpers({
    notificaciones: function() {
        return Notificaciones.find({userId: Meteor.userId(), leido: false});
    },
    
    contadorNotificaciones: function(){
        return Notificaciones.find({userId: Meteor.userId(), leido: false}).count();
    }
});

Template.itemNotificacion.helpers({
    rutaContenidoNotificacion: function() {
        return Router.routes.paginaContenido.path({_id: this.contenidoId});
    },
    
    rutaCursoNotificacion: function() {
        return Router.routes.paginaCurso.path({_id: this.cursoId});
    },
    
    fueAdministrador: function() {
        if(this.nombreQuienAcepto)
            return true;
    },

    cursoCreado: function() {
        return this.tipo=="curso creado";
    },

    cursoAceptado: function() {
        return this.tipo=="curso aceptado";
    },

    comentario: function() {
        return this.tipo=="comentario";
    },

    inicioCurso: function() {
        return this.tipo=="curso iniciado";
    },

    terminoCurso: function() {
        return this.tipo=="curso terminado";
    },

    entregaTrabajo: function() {
        return this.tipo=="entrega trabajo";
    },
});

Template.itemNotificacion.events({
    'click a': function() {
        Notificaciones.update(this._id, {$set: {leido: true}});
    }
});