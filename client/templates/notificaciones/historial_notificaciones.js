Template.historialNotificaciones.events({

});

Template.historialNotificaciones.helpers({
    notificaciones: function() {
        return Notificaciones.find({}, {sort: {fecha: -1}});
    },

    comentarios: function() {
        return Comentarios.find({contenidoId: this._id}, {sort: {fechaComentario: -1}});
    }
});

Template.itemHistorial.events({

});

Template.itemHistorial.helpers({
    rutaContenidoNotificacion: function() {
        return Router.routes.paginaContenido.path({_id: this.contenidoId});
    },
    
    rutaCursoNotificacion: function() {
        return Router.routes.paginaCurso.path({_id: this.cursoId});
    },

    cursoCreado: function() {
        return this.tipo=="curso creado";
    },

    cursoAceptado: function() {
        return this.tipo=="curso aceptado";
    },

    comentario: function() {
        return this.tipo=="comentario";
    },

    inicioCurso: function() {
        return this.tipo=="curso iniciado";
    },

    terminoCurso: function() {
        return this.tipo=="curso terminado";
    },

    entregaTrabajo: function() {
        return this.tipo=="entrega trabajo";
    },

    cursoComentado: function() {
        var moduloId = Contenidos.findOne({_id: this.contenidoId}).moduloId;
        var cursoId = Modulos.findOne({_id: moduloId}).cursoId;
        var cursoComentado = Cursos.findOne({_id: cursoId}).nombreCurso;

        return cursoComentado;
    },

    fechaNotificacion: function() {
        var fecha = this.fecha;

        var dia = fecha.getDate();
        var mes = fecha.getMonth();
        var año = fecha.getFullYear();

        var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

        return dia + " de " + meses[mes] + " de " + año;
    }
});