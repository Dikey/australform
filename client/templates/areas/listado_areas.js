Template.listadoAreas.events({
    'click #cerrar': function() {
        
    },
    
    'click .miModal': function() {
        
    }
});

Template.listadoAreas.helpers({
    areas: function() {
        return Areas.find();
    },

    //Devolvemos solo las áreas en las que está inscrito el usuario
    areasInscritas: function() {
        var userId = Meteor.userId();
        var areas = [];

        var inscripciones = Inscripciones.find({usuarioId: userId});
        var cantidad = inscripciones.count();

        var i = 0;
        while(i<cantidad) {
            var inscripcion = Inscripciones.findOne({usuarioId: userId}, {skip: i});
            var cursoId = inscripcion.cursoId;
            var curso = Cursos.findOne({_id: cursoId});
            var areaId = curso.areaId;

            var j = 0;
            var repetido = false;
            while(j < areas.length) {

                if( areaId == areas[j]._id ) {
                    repetido = true;
                }
                
                j++;
            }

            if(!repetido)
                areas[i] = Areas.findOne({_id: areaId});

            i++;
        }

        return areas;
    }
});