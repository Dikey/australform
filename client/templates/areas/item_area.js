Template.itemArea.events({
});

//modificar esta función para que no muestre flechas donde no hay cursos
Template.itemArea.helpers({
    mostrarCursos: function() {
        var areaActual = this._id;
        var cursosDeEstaArea = Cursos.find({areaId: areaActual});
        if(cursosDeEstaArea.count()===0)
            return false
        else
            return true;
    }
});