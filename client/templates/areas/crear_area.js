Template.crearArea.onCreated(function() {
    Session.set('erroresCrearArea', {});
});

Template.crearArea.events({
    'submit form': function (e) {
        e.preventDefault();
        
        var area = {
            nombreArea: $(e.target).find('[id=ingresoArea]').val()
        };
        
        var errores = validarArea(area);
        if (errores.nombreArea)
            return Session.set('erroresCrearArea', errores);
        
        //llamada hacia metodo del servidor insertarArea para insertar areas
        Meteor.call('insertarArea', area, function(error, result) {
            if (error)
                return lanzarError(error.reason);
                
            if (result.existeArea)
                lanzarError('Esta área ya ha sido creada');
                
            Router.go('listadoAreas', {_id: result._id});
        });
    }
});

Template.crearArea.helpers({
    mensajeError: function(field) {
        return Session.get('erroresCrearArea') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresCrearArea') [field] ? 'has-error' : '';
    }
});