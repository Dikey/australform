Template.crearArea.onCreated(function() {
    Session.set('erroresEdicionArea', {});
});

Template.edicionArea.events({
    'submit form': function(e) {
        e.preventDefault();
        
        var idArea = this._id;
        
        var area = {
            nombreArea: $(e.target).find('[id=ingresoArea]').val()
        };
        
        var errores = validarArea(area);
        if (errores.nombreArea)
            return Session.set('erroresEdicionArea', errores);
        
        Areas.update(idArea, {$set: area}, function(error) {
            if (error) {
                lanzarError(error.reason);
            } else {
                Router.go('listadoAreas');
            }
        });
    },
    
    'click #eliminar': function(e) {
        e.preventDefault();

        var idArea = this._id;
        
        if (confirm("¿Seguro quieres eliminar esta categoría?")) {
                
            Areas.remove(idArea);
            Router.go('listadoAreas');
        }
    }
});

Template.edicionArea.helpers({
    mensajeError: function(field) {
        return Session.get('erroresEdicionArea') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresEdicionArea') [field] ? 'has-error' : '';
    }
});