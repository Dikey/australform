Template.crearProfesor.onCreated(function() {
    Session.set('erroresCrearProfesor', {});
});

Template.crearProfesor.events({
    'submit form': function(e) {
        e.preventDefault();
        
        var usuario = {
            username: $('#usuario').val(),
            email: $('#email').val(),
            password: $('#contraseña').val(),
            profile: {
                fullname: $('#nombre').val(),
                cargo: $('#cargo').val(),
                descripcion: $('#descripcion').val(),
                linkedin: $('#linkedin').val()
            }
        };

        var errores = validarUsuario(usuario);
        if(errores.username || errores.email || errores.fullname || errores.password || errores.cargo || errores.descripcion)
            return Session.set('erroresCrearProfesor', errores);
        
        var verificacion = $('#verificacion').val();
        
        if (verificacion === usuario.password) {

            Meteor.call('crearProfesor', usuario, function(error) {
                if (error) return lanzarError(error.reason);
            });

            Router.go('listadoAreas');

        } else {
            lanzarError('Las contraseñas no coinciden');
        }
    }
});

Template.crearProfesor.helpers({
    profesores: function() {
        return Meteor.users.find({roles: "profesor"});
    },

    nombreUsuario: function() {
        return this.profile.fullname;
    },

    correo: function() {
        return this.emails;
    },

    nombreCorreo:function() {
        return this.address;
    },

    cargo: function() {
        return this.profile.cargo;
    },

    cursoAsignado: function() {
        var userId = this._id;
        return Cursos.find({userId: userId});
    },

    nombreCursoAsignado: function() {
        return this.nombreCurso;
    },

    mensajeError: function(field) {
        return Session.get('erroresCrearProfesor') [field];
    },

    claseError: function(field) {
        return !!Session.get('erroresCrearProfesor') [field] ? 'has-error' : '';
    }
})