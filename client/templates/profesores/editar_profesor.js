Template.editarProfesor.onCreated(function() {
    Session.set('erroresEditarProfesor', {});
});

Template.editarProfesor.events({
    'submit form': function(e) {
        e.preventDefault();

        var userId = this._id;

        var usuario = {
            username: $('#usuario').val(),
            email: $('#email').val(),
            password: $('#contraseña').val(),
            profile: {
                fullname: $('#nombre').val(),
                cargo: $('#cargo').val(),
                descripcion: $('#descripcion').val(),
                linkedin: $('#linkedin').val()
            }
        };

        var errores = validarUsuario(usuario);
        if(errores.username || errores.email || errores.fullname || errores.cargo || errores.descripcion)
            return Session.set('erroresEditarProfesor', errores);

        var verificacion = $('#verificacion').val();
        
        if (usuario.password && verificacion != usuario.password)
            return lanzarError('Las contraseñas no coinciden');

        Meteor.call('editarProfesor',userId, usuario, function(error) {
            //if (error) return lanzarError(error.reason);
        });

        Router.go('listadoAreas');
    }
});

Template.editarProfesor.helpers({
    mensajeError: function(field) {
        return Session.get('erroresEditarProfesor') [field];
    },

    claseError: function(field) {
        return !!Session.get('erroresEditarProfesor') [field] ? 'has-error' : '';
    }
});