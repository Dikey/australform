Template.definirPruebaModulo.onCreated(function() {
    Session.set('erroresDefinirPruebaModulo', {});
});

Template.definirPruebaModulo.events({
    'submit form': function(e, template) {
        e.preventDefault();

        var modulo = this;
        var idModulo = modulo._id;

        //funcion cantidad de helper
        var cantidad = 0;

        var pos = 0;
        var contenidos = Contenidos.find({moduloId: idModulo}).count();

        while(pos<contenidos) {
            var uno = Contenidos.findOne({moduloId: idModulo}, {skip: pos});
            cantidad += Tests.find({contenidoId: uno._id}).count();
            pos++;
        }
        //------------------------------

        var total = cantidad;
        var cantidadPreguntas = $(e.target).find('[id=cantidad-preguntas]').val();

        if(cantidadPreguntas>total)
            return lanzarError("No tienes la cantidad suficiente de preguntas agregadas");

        var check1 = template.$("#test1")[0];
        var check2 = template.$("#test2")[0];

        var alternativas = false;
        var verdadero = false;

        if (check1.checked)
            alternativas = true;

        if(check2.checked)
            verdadero = true;

        var duracion = $(e.target).find('[id=duracion]').val();

        var prueba = {
            aceptaAlternativas: alternativas,
            aceptaVerdaderoYFalso: verdadero,
            cantidadPreguntas: cantidadPreguntas,
            duracion: duracion
        }

        //-->validación

        /*
        Meteor.call('definirPruebaModulo', prueba, function(error, result) {
            if (error)
                return lanzarError(error.reason);
            
            Router.go('crearContenidoDesdeModulo', {_id: result._id});
        });*/

        Modulos.update(idModulo,  {$set: {prueba: prueba} }, function(error) {
            if (error) {
                lanzarError(error.reason);
            } else {
                Router.go('paginaModulo', {_id: idModulo});
            }
        });
    }
});

Template.definirPruebaModulo.helpers({
    cantidad: function() {

        var cantidad = 0;

        var pos = 0;
        var contenidos = Contenidos.find({moduloId: this._id}).count();

        while(pos<contenidos) {
            var uno = Contenidos.findOne({moduloId: this._id}, {skip: pos});
            cantidad += Tests.find({contenidoId: uno._id}).count();
            pos++;
        }

        return cantidad;
    },

    mensajeError: function(field) {
        return Session.get('erroresDefinirPruebaModulo') [field];
    },

    claseError: function(field) {
        return !!Session.get('erroresDefinirPruebaModulo') [field] ? 'has-error' : '';
    }
});