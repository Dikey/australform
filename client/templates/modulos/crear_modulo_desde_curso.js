Template.crearModuloDesdeCurso.onCreated(function() {
    Session.set('erroresCrearModulo', {});
});

Template.crearModuloDesdeCurso.events({
    'submit form': function(e) {
        e.preventDefault();
        
        var cursoId = this._id;
        
        var contadorModulos = Modulos.find({cursoId: cursoId}).count();
        
        if(contadorModulos===0) {
            var requisitoModuloId = "";
        } else {
            var requisitoModulo = $(e.target).find('[id=seleccion-modulo]').val();
            if(!requisitoModulo) {
                var requisitoModuloId = "";
            } else {
                var requisitoModuloId = Modulos.findOne({nombreModulo: requisitoModulo})._id;
            }
        }
                
        var modulo = {
            nombreModulo: $(e.target).find('[name=modulo]').val(),
            cursoId: cursoId,
            introduccionModulo: $(e.target).find('[name=introduccion]').val(),
            requisitoModuloId: requisitoModuloId
        }
        
        var errores = validarModulo(modulo);
        if (errores.nombreModulo || errores.cursoId || errores.introduccionModulo)
            return Session.set('erroresCrearModulo', errores);
        
        //llamada al método de creación de módulo desde el servidor
        Meteor.call('insertarModulo', modulo, function(error, result) {
            if (error)
                return lanzarError(error.reason);
            
            if (result.existeModulo)
                lanzarError('Este módulo ya ha sido creado');
            
            Router.go('crearContenidoDesdeModulo', {_id: result._id});
        });
    }
});

Template.crearModuloDesdeCurso.helpers({
    cursos: function() {
        var userId = Meteor.userId();
        return Cursos.find({userId: userId});
    },
    
    nombreCurso: function() {
        return this.nombreCurso;
    },
    
    modulos: function() {
        var userId = Meteor.userId();
        var cursoId = this._id;

        return Modulos.find( { $and: [{userId: userId}, {cursoId: cursoId}] });
    },
    
    mensajeError: function(field) {
        return Session.get('erroresCrearModulo') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresCrearModulo') [field] ? 'has-error' : '';
    }
});
