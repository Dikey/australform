Template.itemModulo.events({
    'click #botonModulo': function() {
        
        iniciarDesarrolloModulo(this);
        
        //preguntamos si no tiene requisito para saber si es el primer módulo del curso
        if (!this.requisitoModuloId) {
            //buscamos el curso al cuál está asociado el módulo
            var curso = Cursos.findOne({_id: this.cursoId});
        
            iniciarDesarrolloCurso(curso);
        }
    }
});

Template.itemModulo.helpers ({
    previoAprobado: function() {
        //pregunto si el actual tiene modulo requisito, si no tiene se devuelve el requisito aprobado
        if (this.requisitoModuloId==='')
            return true;
        
        //si tiene requisito, busco a este modulo y lo guardo en una variable llamada moduloPrevio
        var moduloPrevio = Modulos.findOne({_id:this.requisitoModuloId});
        
        //tomamos el id del mismo y lo guardamos en una nueva variable
        var moduloPrevioId = moduloPrevio._id;
        
        //guardamos el id del usuario actual
        var userId = Meteor.userId();
        
        //buscamos en la colección desarrollos un documento con el usuario actual y el id del módulo previo
        var desarrolloIngresado = Desarrollos.findOne({ $and: [{userId: userId}, {moduloId: moduloPrevioId}]});
        
        //si el documento está vacío devolvemos falso
        if (!desarrolloIngresado)
            return false;
        
        //si no devolvemos el resultado de la variable aprobado
        return desarrolloIngresado.aprobado;
    },
    /*
    moduloAprobado: function() {
        //guardamos el id del usuario actual
        var userId = Meteor.userId();
        
        //buscamos en la colección desarrollos un documento con el usuario actual y el id del módulo
        var desarrolloIngresado = Desarrollos.findOne({ $and: [{userId: userId}, {moduloId: this._id}]});
        
        //si el documento está vacío devolvemos falso
        if (!desarrolloIngresado)
            return false;
        
        //si no devolvemos el resultado de la variable aprobado
        return desarrolloIngresado.aprobado;
    },*/
    
    //funcion que permite verificar si eres el creador del modulo
    propioModulo: function() {
        return this.userId === Meteor.userId();
    },

    // Nuevas funciones ------------------------

    //función que devuelve la cantidad total de contenidos del módulo
    cantidadContenidos: function() {
        var moduloId = this._id;
        var contenidos = Contenidos.find({moduloId: moduloId}).count();
        return contenidos;
    },

    //función que devuelve la cantidad total de contenidos aprobados del módulo
    cantidadContenidosAprobados: function() {
        var cantidadAprobados = 0;

        return cantidadAprobados;
    },

    //función que devuelve los contenidos
    contenidos: function() {
        var moduloId = this._id;
        var contenidos = Contenidos.find({moduloId: moduloId}, {sort: {orden: 1}});
        return contenidos;
    },

    //devuelve el orden del módulo al cual esta asociado el contenido
    ordenModulo: function() {
        var contenido = this;
        var ordenModulo = Modulos.findOne({_id: contenido.moduloId}).orden;

        return ordenModulo;
    },

    moduloAprobado: function() {
        var modulo = this;
        var desarrollo = Desarrollos.findOne({ $and: [ {moduloId: modulo._id}, {userId: Meteor.userId()} ]});

        if(!desarrollo)
        return false;

        if(desarrollo.aprobado) {
            return true;
        } else {
            return false;
        }
    },

    moduloBloqueado: function() {
        var moduloPrerrequisitoId = this.requisitoModuloId;

        if(!moduloPrerrequisitoId) {
            return false;
        }

        var desarrollo = Desarrollos.findOne({ $and: [ {moduloId: moduloPrerrequisitoId}, {userId: Meteor.userId()} ]});

        if(!desarrollo)
        return true;

        if(!desarrollo.aprobado) {
            return true
        } else {
            return false;
        }
    },

    tienePrueba: function() {
        if(this.prueba){
            return true;
        } else {
            return false;
        }
    },

    duracionPrueba: function() {
        return this.prueba.duracion;
    }
});