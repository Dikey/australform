Template.paginaPruebaModulo.onCreated(function() {
    Session.set('erroresPaginaPruebaModulo', {});
});

Template.paginaPruebaModulo.events({
    'submit form': function(e, template) {
        e.preventDefault();

        var check = template.$("#checkbox-confirmacion")[0];

        if (!check.checked) {
            return lanzarError("Debes aceptar el campo al final de la prueba para enviar tus resultados");
        } else {

            var tiempo = document.getElementById("demo").innerHTML;

            if(tiempo == "Tiempo Finalizado")
                return lanzarError("Se ha agotado el tiempo para realizar tu prueba, debes empezar nuevamente");

            var cantidadPreguntas = this.prueba.cantidadPreguntas;
            var preguntasCorrectas = 0;

            var pos = 0;
            while(pos<cantidadPreguntas) {

                var posId = pos+1;
                /* preguntamos el tipo de pregunta */
                var idPregunta = template.$("#idPregunta"+posId)[0].text;
                var tipo = Tests.findOne({_id: idPregunta}).tipo;
                
                if(tipo=="Alternativas") {
                    var cantidad = 4;
                    var correcta = Tests.findOne({_id: idPregunta}).correcta;
                } else {
                    var cantidad = 2;
                    var correcta = Tests.findOne({_id: idPregunta}).resultado;
                }    
                
                for(var n = 1; n<=cantidad; n++) {
                    
                    var number1 = Number((pos*4)+n);
                    var concat1 = "#radio" + number1;
                    var radio1 = template.$(concat1)[0];
                    
                    if(radio1.checked) {
                        var concat2 = "#alt" + number1;
                        var text = template.$(concat2)[0].textContent;
                        
                        if(text==correcta) {
                            preguntasCorrectas++;
                        }
                        
                    }
                }

                pos++;
            }

            //insertamos en la tabla desarrollo el id del usuario el id del módulo, la cantidad de preguntas buenas y el total de preguntas
            
            var desarrollo = {
                moduloId: this._id,
                preguntasCorrectas: preguntasCorrectas,
                totalPreguntas: cantidadPreguntas
            }

            //validaciones

            Meteor.call('realizarPruebaModulo', desarrollo, function(error, result) {
                if (error)
                    return lanzarError(error.reason);
                    
                Router.go('paginaResultados', {_id: result._id});
            });
        }
    }
});

Template.paginaPruebaModulo.helpers({
    cantidadPreguntas: function() {
        return this.prueba.cantidadPreguntas;
    },

    esAlternativa: function() {
        if(this.tipo=="Alternativas") {
            return true;
        } else {
            return false;
        }
    },

    idPregunta: function() {
        return this._id;
    },

    idRadio1: function() {
        var resultado = ((this.posicion-1)*4)+1;

        return resultado;
    },

    idRadio2: function() {
        var resultado = ((this.posicion-1)*4)+2;

        return resultado;
    },

    idRadio3: function() {
        var resultado = ((this.posicion-1)*4)+3;

        return resultado;
    },

    idRadio4: function() {
        var resultado = ((this.posicion-1)*4)+4;

        return resultado;
    },

    nombreUsuario: function() {
        var usuario = Meteor.user();

        return usuario.profile.fullname;
    },

    duracion: function() {
        return this.prueba.duracion;
    },

    preguntas: function() {

        var stop = this.prueba.cantidadPreguntas;
        var aceptaAlternativas = this.prueba.aceptaAlternativas;
        var aceptaVerdaderoYFalso = this.prueba.aceptaVerdaderoYFalso;
        var preguntas = [];
        var preguntasRandomizadas = 0;
        var numeroRandom = 0;
        var pos = 0;

        while(preguntasRandomizadas<stop){
            /* Realizar busqueda por tipo de pregunta */
            var contenido = Contenidos.findOne( { $and: [ {moduloId: this._id}, {orden: pos+1} ] });
            if(!contenido){
                pos = 0;
            } else {
                var cantidadPreguntas = Tests.find({contenidoId: contenido._id}).count();
                if(cantidadPreguntas!=0) {

                    /* Capturar excepcion de repetición de números randoms */

                    
                    //obtenemos la pregunta del pool de preguntas de acuerdo al random y al tipo de pregunta
                    if(aceptaAlternativas && aceptaVerdaderoYFalso) {
                        //generamos un número randómico
                        numeroRandom = Math.floor(Math.random() * cantidadPreguntas) +1;
                        var variable = Tests.findOne( { $and: [ {contenidoId: contenido._id}, {orden: numeroRandom} ] });
                    }

                    if(aceptaAlternativas && !aceptaVerdaderoYFalso) {
                        var cantidad = Tests.find( { $and: [ {contenidoId: contenido._id}, {tipo: "Alternativas"} ] }).count();
                        var i = 0;
                        var arreglo = [];
                        while(i<cantidad) {
                            arreglo[i] = Tests.findOne( { $and: [ {contenidoId: contenido._id}, {tipo: "Alternativas"} ] }, {skip: i});
                            i++;
                        }
                        numeroRandom = Math.floor(Math.random() * cantidad);
                        var variable = arreglo[numeroRandom];
                    }

                    if(!aceptaAlternativas && aceptaVerdaderoYFalso) {
                        var cantidad = Tests.find( { $and: [ {contenidoId: contenido._id}, {tipo: "Verdadero y Falso"} ] }).count();
                        var i = 0;
                        var arreglo = [];
                        while(i<cantidad) {
                            arreglo[i] = Tests.findOne( { $and: [ {contenidoId: contenido._id}, {tipo: "Verdadero y Falso"} ] }, {skip: i});
                            i++;
                        }
                        numeroRandom = Math.floor(Math.random() * cantidad);
                        var variable = arreglo[numeroRandom];
                    }
                    
                    //creamos una pregunta
                    if(variable.tipo=="Alternativas") {

                        var pregunta = {
                            _id: variable._id,
                            contenidoId: variable.contenidoId,
                            tipo: variable.tipo,
                            enunciado: variable.enunciado,
                            posicion: preguntasRandomizadas+1
                        }

                        randomAlt = Math.floor(Math.random() * 4 ) + 1;

                        if(randomAlt==1) {
                            pregunta.alt1 = variable.correcta;
                            pregunta.alt2 = variable.alt1;
                            pregunta.alt3 = variable.alt2;
                            pregunta.alt4 = variable.alt3;
                        }

                        if(randomAlt==2) {
                            pregunta.alt1 = variable.alt1;
                            pregunta.alt2 = variable.correcta;
                            pregunta.alt3 = variable.alt2;
                            pregunta.alt4 = variable.alt3;
                        }

                        if(randomAlt==3) {
                            pregunta.alt1 = variable.alt1;
                            pregunta.alt2 = variable.alt2;
                            pregunta.alt3 = variable.correcta;
                            pregunta.alt4 = variable.alt3;
                        }

                        if(randomAlt==4) {
                            pregunta.alt1 = variable.alt1;
                            pregunta.alt2 = variable.alt2;
                            pregunta.alt3 = variable.alt3;
                            pregunta.alt4 = variable.correcta;
                        }

                    } else {

                        var pregunta = {
                            _id: variable._id,
                            contenidoId: variable.contenidoId,
                            tipo: variable.tipo,
                            sentencia: variable.sentencia,
                            resultado: variable.resultado,
                            posicion: preguntasRandomizadas+1
                        }
                    }

                    //se agrega la pregunta al array de preguntas a mostrar
                    preguntas[preguntasRandomizadas] = pregunta;
                    //----------------------

                    preguntasRandomizadas++;
                }
                pos++;
            }
        }

        return preguntas;
    }
});