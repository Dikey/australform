Template.mapaCurso.events({

});

Template.mapaCurso.helpers({
    nodos: function() {
        var modulos = this;

        return modulos;
    },

    ultimoNodo: function() {
        var modulo = this;

        // Función división
        var cursoId = this.cursoId;

        var curso;
        var cantidadModulos;

        if(!cursoId) {
            curso = this;
            cantidadModulos = Modulos.find({cursoId: this._id}).count();
        } else {
            curso = Cursos.findOne({_id: cursoId});
            cantidadModulos = Modulos.find({cursoId: cursoId}).count();
        }

        if(curso) {
            if(curso.pruebaFinal) {
                cantidadModulos++;
            }
        }

        if(cantidadModulos>3)
        cantidadModulos=4;

        // ----- Fin función
        if(modulo.orden%cantidadModulos==0)
            return true;
    },

    tienePrueba: function() {
        var curso = Cursos.findOne({_id: this.cursoId});
        var tienePruebaFinal = curso.pruebaFinal;

        if(tienePruebaFinal)
            return true;
    },

    esPrueba: function() {
        var esModulo = this.nombreModulo;

        if(!esModulo)
            return true;
    },

    moduloActual: function() {
        var modulo = this;
        
        var moduloAnterior = this.requisitoModuloId;

        var desarrollo = Desarrollos.findOne({ $and: [ {moduloId: modulo._id}, {userId: Meteor.userId()} ]});
        
        if(!desarrollo) {

            if(!moduloAnterior) {
                return true;

            } else {

                var desarrolloAnterior = Desarrollos.findOne({ $and: [ {moduloId: moduloAnterior}, {userId: Meteor.userId()} ]});
                
                if(desarrolloAnterior) {
                    if(desarrolloAnterior.aprobado)
                    return true;
                }
            }

        } else {
            if(desarrollo.aprobado) {
                return false;
            } else {
                return true;
            }
        }

    },

    moduloAprobado: function() {
        var modulo = this;
        var desarrollo = Desarrollos.findOne({ $and: [ {moduloId: modulo._id}, {userId: Meteor.userId()} ]});

        if(!desarrollo)
        return false;

        if(desarrollo.aprobado) {
            return true;
        } else {
            return false;
        }
    },

    modulosAprobados: function() {

        var cantidadModulos = Modulos.find({cursoId: this._id}).count();
        
        var cantidadAprobados = 0;
        var pos = 0;
        while(pos<cantidadModulos) {
            var moduloId = Modulos.findOne({ $and: [{cursoId: this._id}, {orden: pos+1}] })._id;
            var desarrollo = Desarrollos.findOne({ $and: [{moduloId: moduloId}, {userId: Meteor.userId()}] });

            if(!desarrollo) {
                return false;
            } else {
                if(desarrollo.aprobado) {
                    cantidadAprobados++;
                } else {
                    return false;
                }
            }
            pos++;
        }

        if(cantidadAprobados==cantidadModulos) {
            return true;
        } else {
            return false;
        }
    },

    pruebaAprobada: function() {
        var cursoId = this._id;
        var desarrollo = Desarrollos.findOne({ $and: [ {cursoId: cursoId}, {userId: Meteor.userId()} ]});

        if(desarrollo) {
            if(desarrollo.aprobado)
                return true;
        }
    },

    filas: function() {
        var filas = [];
        var arrayModulos = [];

        var cursoId = this.cursoId;
        var cantidadModulos = Modulos.find({cursoId: cursoId}).count();

        var curso = Cursos.findOne({_id: cursoId});
        var tienePruebaFinal = curso.pruebaFinal;

        var modulosInsertados = 0;
        var i = 0;
        while(i < cantidadModulos) {

            if( arrayModulos.length!=0 && arrayModulos.length%4==0 ) {
                filas.push(arrayModulos);
                arrayModulos = [];
            } else {
                arrayModulos.push(Modulos.findOne({ $and: [ {cursoId: cursoId}, {orden: i+1}] }));
                modulosInsertados++;
                i++;
            }

            if(i == cantidadModulos) {

                // Mejorar ??
                if(arrayModulos.length!=0) {
                    if(arrayModulos.length%4==0) {
                        filas.push(arrayModulos);
                        arrayModulos = [];
                        if(tienePruebaFinal) {
                            arrayModulos.push(curso); 
                            filas.push(arrayModulos);
                            arrayModulos = [];
                        }
                    } else {
                        if(tienePruebaFinal) {
                            arrayModulos.push(curso); 
                            filas.push(arrayModulos);
                            arrayModulos = [];
                        } else {
                            filas.push(arrayModulos);
                            arrayModulos = [];
                        }
                    }
                } else {
                    if(tienePruebaFinal) {
                        arrayModulos.push(curso); 
                        filas.push(arrayModulos);
                        arrayModulos = [];
                    }
                }
                //-----------
            } 
        }

        return filas;
    },

    division: function() {
        var cursoId = this.cursoId;

        var curso;
        var cantidadModulos;

        if(!cursoId) {
            curso = this;
            cantidadModulos = Modulos.find({cursoId: this._id}).count();
        } else {
            curso = Cursos.findOne({_id: cursoId});
            cantidadModulos = Modulos.find({cursoId: cursoId}).count();
        }

        if(curso) {
            if (curso.pruebaFinal) {
                cantidadModulos++;
            }
        }

        if(cantidadModulos>3)
        cantidadModulos=4;

        return Math.round(12/cantidadModulos);
    }
});