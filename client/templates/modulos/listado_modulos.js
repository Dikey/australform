Template.listadoModulos.helpers({
    
    modulos: function() {

        //preguntar si mostramos el listado desde curso o desde el módulo
        if(!this.nombreCurso) {
            var cursoId = this.cursoId;

            return Modulos.find({cursoId: cursoId}, {sort: {fechaModulo: 1}});
        }

        return Modulos.find({cursoId: this._id}, {sort: {fechaModulo: 1}});
    },
    
    tienePruebaFinal: function() {
        if(!this.pruebaFinal) {
            return false;
        } else {
            return true;
        }      
    },
    
    tituloPrueba: function() {
        return this.pruebaFinal.tituloPrueba;
    },
    
    modulosAprobados: function() {
        var user = Meteor.user();
        if (!user)
            return false;
        
        var cursoId = this._id;
        
        var tamañoModulos = Modulos.find({cursoId: this._id}).count();
        var modulosAprobados = 0;
        
        var ciclo = 0;
        while (ciclo < tamañoModulos) {
        
            var modulos = Modulos.findOne({cursoId: cursoId}, {skip: ciclo});
            var moduloId = modulos._id;
            var modulo = Desarrollos.findOne({ $and: [{userId: user._id}, {moduloId: moduloId}]});
    
            if(!modulo || !modulo.aprobado) {
                return false;
            } else {
                if (modulo.aprobado)
                    modulosAprobados++;
            }
            ciclo++;
        }
        
        if (modulosAprobados===tamañoModulos)
            return true;
    },
    
    propioModulo: function() {
        return this.userId === Meteor.userId();
    },
    
    estadoPendiente: function() {
        var estado = this.estado;
        if (estado==="pendiente")
            return true;
            else
                return false;
    },
    
    pruebaAprobada: function() {
        var user = Meteor.user();
        var curso = this;
        
        var desarrolloIngresado = Desarrollos.findOne({ $and: [{userId: user._id}, {cursoId: curso._id}]});
        
        if(!desarrolloIngresado.aprobado)
            return false;
            else
                return true;
    }
});