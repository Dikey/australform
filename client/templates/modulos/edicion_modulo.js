Template.edicionModulo.onCreated(function() {
    Session.set('erroresEditarModulo', {});
});

Template.edicionModulo.events({
    'submit form': function(e)  {
        e.preventDefault();
        
        var idModulo = this._id;

        var modulo = Modulos.findOne({_id: idModulo});

        var cursoId = Cursos.findOne({_id: modulo.cursoId})._id;
        
        var contadorModulos = Modulos.find({cursoId: modulo.cursoId}).count();
        
        if(contadorModulos===0) {
            var requisitoModuloId = "";
        } else {
            var requisitoModulo = $(e.target).find('[id=seleccion-modulo]').val();
            if(!requisitoModulo) {
                var requisitoModuloId = "";
            } else {
                var requisitoModuloId = Modulos.findOne({nombreModulo: requisitoModulo})._id;
            }
        }
                
        var modulo = {
            nombreModulo: $(e.target).find('[name=modulo]').val(),
            cursoId: cursoId,
            introduccionModulo: $(e.target).find('[name=introduccion]').val(),
            requisitoModuloId: requisitoModuloId
        }
        
        var errores = validarModulo(modulo);
        if (errores.nombreModulo || errores.cursoId || errores.introduccionModulo)
            return Session.set('erroresEditarModulo', errores);
        
        //realizamos la función que actualiza el módulo asignado
        Modulos.update(idModulo, {$set: modulo}, function(error) {
            if (error) {
                lanzarError(error.reason);
            } else {
                Router.go('paginaModulo', {_id: idModulo});
            }
        });
    },
    
    'click #eliminar': function(e) {
        e.preventDefault();
        
        if (confirm("¿Quieres eliminar este modulo?")) {
            var idModulo = this._id;
            Modulos.remove(idModulo);
            Router.go('listadoAreas');
        }
    }
});

Template.edicionModulo.helpers({
    cursos: function() {
        var userId = Meteor.userId();
        return Cursos.find({userId: userId});
    },
    
    nombreCurso: function() {
        return this.nombreCurso;
    },
    
    modulos: function() {
        var userId = Meteor.userId();

        var contenidoActualId = this._id;

        return Modulos.find({ $and: [ {userId: userId}, {cursoId: this.cursoId}, {_id: { $ne: contenidoActualId}}]});
    },

    requisitoModulo: function() {
        var requisitoModuloId = this.requisitoModuloId;

        if(!requisitoModuloId)
            return "Sin prerrequisito" ;

        return Modulos.findOne({_id: requisitoModuloId}).nombreModulo;
    },
    
    mensajeError: function(field) {
        return Session.get('erroresEditarModulo') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresEditarModulo') [field] ? 'has-error' : '';
    }
});





