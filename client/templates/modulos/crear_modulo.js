Template.crearModulo.onCreated(function() {
    Session.set('erroresCrearModulo', {});
});

Template.crearModulo.events({
    'submit form': function(e) {
        e.preventDefault();
        
        var $curso = $(e.target).find('[id=seleccion-curso]');
        var cursoSeleccionado = $curso.val();
        
        if(!cursoSeleccionado) {
            var cursoId = "";
            var requisitoModuloId = "";
        } else {
            var curso = Cursos.findOne({nombreCurso: cursoSeleccionado});
            var cursoId = curso._id;
        
            var contadorModulos = Modulos.find({cursoId: cursoId}).count();
        
            if(contadorModulos===0) {
                var requisitoModuloId = "";
            } else {
                var requisitoModulo = Modulos.findOne({cursoId: cursoId}, {skip: contadorModulos-1});
                var requisitoModuloId = requisitoModulo._id;
            }
        }
                
        var modulo = {
            nombreModulo: $(e.target).find('[name=modulo]').val(),
            cursoId: cursoId,
            requisitoModuloId: requisitoModuloId
        }
        
        var errores = validarModulo(modulo);
        if (errores.nombreModulo || errores.cursoId)
            return Session.set('erroresCrearModulo', errores);
        
        //llamada al método de creación de módulo desde el servidor
        Meteor.call('insertarModulo', modulo, function(error, result) {
            if (error)
                return lanzarError(error.reason);
            
            if (result.existeModulo)
                lanzarError('Este módulo ya ha sido creado');
            
            Router.go('crearContenidoDesdeModulo', {_id: result._id});
        });
    }
});

Template.crearModulo.helpers({
    cursos: function() {
        var userId = Meteor.userId();
        return Cursos.find({userId: userId});
    },
    
    modulos: function() {
        var userId = Meteor.userId();
        return Modulos.find({userId: userId});

        //return Modulos.find( { $and: [{userId: userId}, {}, {}] });
    },
    
    mensajeError: function(field) {
        return Session.get('erroresCrearModulo') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresCrearModulo') [field] ? 'has-error' : '';
    }
});



