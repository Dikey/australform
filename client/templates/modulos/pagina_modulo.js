
Template.paginaModulo.events({
    'click #boton-bienvenida': function(e) {
        e.preventDefault();

        var cursoId = this.cursoId;

        Router.go('paginaCurso', {_id: cursoId});

    },

    'submit form': function(e){
        e.preventDefault();

        var moduloId = this._id;

        var contenidos = Contenidos.findOne({moduloId: moduloId}, {sort: {orden: 1}});

        var contenidoId = contenidos._id;
        
        Router.go('paginaContenido', {_id: contenidoId});
    },

    'click #resultadosModulo': function() {

        var desarrollo = Desarrollos.findOne({ $and: [{moduloId: this._id}, {userId: Meteor.userId()}]});

        Router.go('paginaResultados', {_id: desarrollo._id});
    }
});



Template.paginaModulo.helpers({

     //funcion que permite verificar si eres el creador del curso
     propioModulo: function() {
        return this.userId === Meteor.userId();
    },

    tieneVideo: function() {
        if(!this.video)
            return false;
        else
            return true;
    },
    
    estadoPendiente: function() {
        var curso = Cursos.findOne({_id: this.cursoId});

        if (curso.estado==="pendiente")
            return true;
    },
    
    link: function() {
        var video = this.video;
        
        var embed = "embed/";
        
        var link = video.substring(0,24) + embed + video.substring(32);
        return link;
    },
    
    //función que devuelve los contenidos
    contenidos: function() {
        var moduloId = this._id;
        var contenidos = Contenidos.find({moduloId: moduloId}, {sort: {orden: 1}});
        return contenidos;
    },

    tieneTest: function() {
        if (!this.test)
            return false;
        else
            return true;
    },
    
    test: function() {
        return this.test;
    },
    
    //funcion que permite verificar si eres el creador del módulo
    propioModulo: function() {
        return this.userId === Meteor.userId();
    },
    
    comentarios: function() {
        return Comentarios.find({moduloId: this._id}, {sort: {submitted: -1}});
    },
    
    contadorComentarios: function() {
        return Comentarios.find({moduloId: this._id}).count();
    },


    // nuevas funciones ---------

    curso: function() {
        return Cursos.findOne({_id: this.cursoId});
    },

    tienePrueba: function() {
        return this.prueba;
    },

    duracionPrueba: function() {
        return this.prueba.duracion;
    },

    tieneDesarrollo: function() {
        var desarrollo = Desarrollos.findOne({ $and: [{moduloId: this._id}, {userId: Meteor.userId()}]});

        if(desarrollo)
            return true;
    }
});

