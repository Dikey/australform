Template.paginaResultados.events({
    'click #boton-retomar':function() {
        var moduloId = this.moduloId;

        Router.go('paginaPruebaModulo', {_id: moduloId});
    },

    'click #boton-siguiente': function() {
        var moduloId = this.moduloId;
        var modulo = Modulos.findOne({_id: moduloId});

        var ordenModulo = modulo.orden;

        var cantidadModulos = Modulos.find({cursoId: modulo.cursoId}).count();
        
        var cursoId = Modulos.findOne({_id: moduloId}).cursoId;

        var tienePrueba = Cursos.findOne({_id: cursoId}).pruebaFinal;
        var tieneTrabajo = Cursos.findOne({_id: cursoId}).trabajoFinal;

        if(ordenModulo==cantidadModulos) {

            //preguntamos si todos los módulos del curso están aprobados
            var cantidadAprobados = 0;
            var pos = 0;
            while(pos<cantidadModulos) {
                var moduloId = Modulos.findOne({ $and: [{cursoId: cursoId}, {orden: pos+1}] })._id;
                var desarrollo = Desarrollos.findOne({ $and: [{moduloId: moduloId}, {userId: Meteor.userId()}] });

                if(desarrollo) {
                    if(desarrollo.aprobado)
                        cantidadAprobados++;
                }
                pos++;
            }

            if(cantidadAprobados==cantidadModulos) {

                //preguntamos si tiene pruebaFinal, si tiene se redirije
                if(tienePrueba)
                    return Router.go('paginaPruebaCurso', {_id: cursoId});

                if(tieneTrabajo)
                    return Router.go('paginaTrabajoFinal', {_id: cursoId});

            } else {
                lanzarError('Debes aprobar todos los módulos para acceder a la prueba final del curso');
            }

        } else {
            
            var moduloSiguiente = Modulos.findOne({ $and: [{cursoId: cursoId}, {orden: ordenModulo+1}] })._id;

            Router.go('paginaModulo', {_id: moduloSiguiente});
        }
    },

    'click #boton-retomar-prueba':function() {

        Router.go('paginaPruebaCurso', {_id: this.cursoId});
    }
});

Template.paginaResultados.helpers({
    esModulo: function() {
        if(this.moduloId)
            return true;
    },

    esCurso: function() {
        if(this.cursoId)
            return true;
    },

    enEspera: function() {
        return this.enEspera;
    },

    ordenModulo: function() {
        var modulo = Modulos.findOne({_id: this.moduloId});

        return modulo.orden;
    },

    trabajo: function() {
        var trabajoFinalId = this.trabajoFinalId;

        return Pdfs.find({_id: trabajoFinalId});
    },

    porcentajeObtenido: function() {
        return Math.round((this.porcentajeObtenido*100)/100);
    },

    notaObtenida: function() {
        return Math.round((this.notaObtenida*100)/100);
    },

    nombreUsuario: function() {
        var usuario = Meteor.user();

        return usuario.profile.fullname;
    },

    nombreCurso: function() {

        var curso = Cursos.findOne({_id: this.cursoId});

        return curso.nombreCurso;
    }
});