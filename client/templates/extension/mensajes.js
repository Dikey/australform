Template.mensajes.events({
   
});

Template.mensajes.helpers({
    mensajes: function() {
        return Mensajes.find();
    } 
});

Template.mensaje.onRendered(function() {
    var mensaje = this.data;
    Meteor.setTimeout(function() {
        Mensajes.remove(mensaje._id);
    }, 4000);
});