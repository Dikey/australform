Template.contactarSoporte.onCreated(function() {
    Session.set('erroresSoporte', {});
});

Template.contactarSoporte.events({
    'submit form': function(e) {
        e.preventDefault();

        var mensaje = document.getElementById('mensajeSoporte').value;

        var user = Meteor.user();
        var username = user.username;
        var fullname = user.profile.fullname;

        var emailAutor = user.emails[0].address;
        var emailAdmin = "formaustral@gmail.com";
        var emailSuperAdmin = "diegofierrof@gmail.com";

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("demo").innerHTML = this.responseText; //puedes hacer lo que quieras con la respuesta.
            }
        };
        xhttp.open("POST", "https://www.incluyes.com/1pruebas/email.php", true); //true si espera la respuesta, false si solamente envía y no se queda a la espera.
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        //Modificar los parametros con los datos que deben ser enviados en el correo.

        //envía los datos
        xhttp.send("titulo=Mensaje a Soporte Técnico&destinatario="+emailAdmin+"&mensaje= El Usuario: "+fullname+", Cuenta: "
        +username+", Correo "+emailAutor+" ha enviado el siguiente mensaje a soporte: '"+mensaje+"'.");

        Router.go('listadoAreas');

        //----------------

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("demo").innerHTML = this.responseText; //puedes hacer lo que quieras con la respuesta.
            }
        };
        xhttp.open("POST", "https://www.incluyes.com/1pruebas/email.php", true); //true si espera la respuesta, false si solamente envía y no se queda a la espera.
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        //Modificar los parametros con los datos que deben ser enviados en el correo.

        //envía los datos
        xhttp.send("titulo=Mensaje a Soporte Técnico&destinatario="+emailSuperAdmin+"&mensaje= El Usuario: "+fullname+", Cuenta: "
        +username+", Correo "+emailAutor+" ha enviado el siguiente mensaje a soporte: '"+mensaje+"'.");

        Router.go('listadoAreas');

    }
});

Template.contactarSoporte.helpers({
    mensajeError: function(field) {
        return Session.get('erroresSoporte') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresSoporte') [field] ? 'has-error' : '';
    }
});