Template.errores.helpers({
  errores: function() {
    return Errores.find();
  }
});

Template.error.onRendered(function() {
    var error = this.data;
    Meteor.setTimeout(function() { 
        Errores.remove(error._id); 
    }, 3000);
});