Template.itemComentario.events({
    
});

Template.itemComentario.helpers({
    textoEnviado: function() {
        return this.fechaComentario.toString();
    },

    fecha: function() {
        var fecha = this.fechaComentario;

        var dia = fecha.getDate();
        var mes = fecha.getMonth();
        var año = fecha.getFullYear();

        var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

        return dia + " de " + meses[mes] + " de " + año;
    }
});