Template.editarComentario.events({
    'click #eliminar': function(e) {
        e.preventDefault();

        var comentarioId = this._id;
        var contenidoId = this.contenidoId;

        Comentarios.remove(comentarioId);
        Router.go('listadoComentarios', {_id: contenidoId});
    }
});

Template.editarComentario.helpers({

});