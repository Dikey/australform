Template.crearComentario.onCreated(function() {
    Session.set('erroresCrearComentario', {});
});

Template.crearComentario.events({
    'submit form': function(e, template) {
        e.preventDefault();
        
        var $texto = $(e.target).find('[id=crearComentario]');
        
        var comentario = {
            texto: $texto.val(),
            contenidoId: template.data._id
        };
        
        var errores = {};
        if(!comentario.texto) {
            errores.texto = "Por favor escribe un comentario";
            return Session.set('erroresCrearComentario', errores);
        }
        
        Meteor.call('insertarComentario', comentario, function(error, comentarioId) {
            if (error) {
                lanzarError(error.reason);
            } else {
                $texto.val('');
            }
        });

        //obtenemos el correo del destinatario
        var tituloContenido = this.tituloContenido;
        var moduloId = this.moduloId;
        var modulo = Modulos.findOne({_id: moduloId});
        var cursoId = modulo.cursoId;
        var curso = Cursos.findOne({_id: cursoId});
        var nombreCurso = curso.nombreCurso;
        var emailAutor = curso.emailAutor;
        var emailAdmin = "formaustral@gmail.com";
        var emailSuperAdmin = "diegofierrof@gmail.com";

        var ordenModulo = modulo.orden;
        var ordenContenido = this.orden;

        var emailComentador = Meteor.user().emails[0].address;
        var username = Meteor.user().username;
        var fullname = Meteor.user().profile.fullname;

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("demo").innerHTML = this.responseText; //puedes hacer lo que quieras con la respuesta.
            }
        };

        xhttp.open("POST", "https://www.incluyes.com/1pruebas/email.php", true); //true si espera la respuesta, false si solamente envía y no se queda a la espera.
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        //Modificar los parametros con los datos que deben ser enviados en el correo.

        //envía los datos
        xhttp.send("titulo=Comentario en FormAustral&destinatario="+emailAutor+"&mensaje= El Usuario: "+fullname+" Cuenta: " 
        +username+", Correo:"+emailComentador+" ha comentado en el contenido "+ordenModulo+"."+ordenContenido+". "+tituloContenido+
        " del curso "+nombreCurso+" el siguiente mensaje: '"+comentario.texto+"'.");

        //------------
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("demo").innerHTML = this.responseText; //puedes hacer lo que quieras con la respuesta.
            }
        };

        xhttp.open("POST", "https://www.incluyes.com/1pruebas/email.php", true); //true si espera la respuesta, false si solamente envía y no se queda a la espera.
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        //Modificar los parametros con los datos que deben ser enviados en el correo.

        //envía los datos
        xhttp.send("titulo=Comentario en FormAustral&destinatario="+emailAdmin+"&mensaje= El Usuario: "+fullname+" Cuenta: "
        +username+", Correo:"+emailComentador+" ha comentado en el contenido "+ordenModulo+"."+ordenContenido+". "+tituloContenido+
        " del curso "+nombreCurso+" el siguiente mensaje: '"+comentario.texto+"'.");

        //------------
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("demo").innerHTML = this.responseText; //puedes hacer lo que quieras con la respuesta.
            }
        };

        xhttp.open("POST", "https://www.incluyes.com/1pruebas/email.php", true); //true si espera la respuesta, false si solamente envía y no se queda a la espera.
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        //Modificar los parametros con los datos que deben ser enviados en el correo.

        //envía los datos
        xhttp.send("titulo=Comentario en FormAustral&destinatario="+emailSuperAdmin+"&mensaje= El Usuario: "+fullname+" Cuenta: "
        +username+", Correo:"+emailComentador+" ha comentado en el contenido "+ordenModulo+"."+ordenContenido+". "+tituloContenido+
        " del curso "+nombreCurso+" el siguiente mensaje: '"+comentario.texto+"'.");
    },
    
    'click #crearComentario': function() {
        var noClick = document.getElementById('crearComentario').value;
        if(noClick==="Escribe un comentario...")
            document.getElementById('crearComentario').value = "";
    }
});

Template.crearComentario.helpers({
    mensajeError: function(field) {
        return Session.get('erroresCrearComentario') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresCrearComentario') [field] ? 'has-error' : '';
    }
});