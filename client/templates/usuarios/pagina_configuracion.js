Template.paginaConfiguracion.onCreated(function() {
    Session.set('erroresPaginaConfiguracion', {});
});

Template.paginaConfiguracion.events({
    'submit form': function(e, template) {
        e.preventDefault();

        configuracion = {
            datos: $(e.target).find('[id=datos]').val(),
            descripcion: $(e.target).find('[id=descripcion]').val(),
            objetivo: $(e.target).find('[id=objetivo]').val(),
            curso: $(e.target).find('[id=curso]').val(),
            modulo: $(e.target).find('[id=modulo]').val(),
            contenido: $(e.target).find('[id=contenido]').val(),
            categoria: $(e.target).find('[id=categoria]').val(),
            usuario: $(e.target).find('[id=usuario]').val(),
            profesor: $(e.target).find('[id=profesor]').val()
        }

        //validaciones

        Meteor.call('updateConfig', configuracion, function(error, result) {
            if (error)
                return lanzarError(error.reason);
                
            Router.go('listadoAreas');
        });
    }
});

Template.paginaConfiguracion.helpers({
    mensajeError: function(field) {
        return Session.get('erroresPaginaConfiguracion') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresPaginaConfiguracion') [field] ? 'has-error' : '';
    }
});