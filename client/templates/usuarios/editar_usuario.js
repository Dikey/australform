Template.editarUsuario.onCreated(function() {
    Session.set('erroresEditarUsuario', {});
});

Template.editarUsuario.events({
    'submit form': function(e) {
        e.preventDefault();

        var userId = this._id;

        var usuario = {
            username: $('#usuario').val(),
            email: $('#email').val(),
            password: $('#contraseña').val(),
            profile: {
                fullname: $('#nombre').val(),
                rut: $('#rut').val(),
                nacionalidad: $('#nacionalidad').val(),
                direccion: $('#direccion').val(),
                telefono: $('#telefono').val(),
                escolaridad: $('#escolaridad').val(),
                fechaNacimiento: $('#date').val(),
                ocupacion: $('#ocupacion').val(),
                cargo: $('#cargo').val(),
                empresa: $('#empresa').val()
            }
        };
        
        var errores = validarUsuario(usuario);
        if(errores.username || errores.email || errores.fullname || errores.rut || errores.nacionalidad || errores.direccion ||
            errores.telefono || errores.escolaridad || errores.fechaNacimiento || errores.ocupacion || errores.cargo || errores.empresa)
            return Session.set('erroresEditarUsuario', errores);

        var verificacion = $('#verificacion').val();
    
        if (usuario.password && verificacion != usuario.password)
            return lanzarError('Las contraseñas no coinciden');

        Meteor.call('editarUsuario',userId, usuario, function(error) {
            //if (error) return lanzarError(error.reason);
        });

        Router.go('listadoAreas');
    }
});

Template.editarUsuario.helpers({
    mensajeError: function(field) {
        return Session.get('erroresEditarUsuario') [field];
    },

    claseError: function(field) {
        return !!Session.get('erroresEditarUsuario') [field] ? 'has-error' : '';
    }
});