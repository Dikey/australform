Template.cambiarProfesor.onCreated(function() {
    Session.set('erroresCambiarProfesor', {});
});

Template.cambiarProfesor.events({
    'submit form': function (e) {
        e.preventDefault();

        var curso = this;

        var username = $(e.target).find('[id=seleccion-profesor]').val();

        if(!username) {

            var usuario = {
                cursoId: this._id,
                userId: ""
            };

        } else {
            var usuarioFind = Meteor.users.findOne({username: username});
            var usuarioId = usuarioFind._id;

            var usuario = {
                cursoId: curso._id,
                userId: usuarioId,
                autorCurso: usuarioFind.profile.fullname,
                emailAutor: usuarioFind.emails[0].address,
                reseña: usuarioFind.profile.descripcion,
                linkedin: usuarioFind.profile.linkedin,
                cargo: usuarioFind.profile.cargo
            };
        }

        var errores = validarAsignacion(usuario);
        if (errores.cursoId || errores.userId)
            return Session.set('erroresCambiarProfesor', errores);

        //llamada al método de asignación de profesor desde el servidor
        Meteor.call('asignarProfesor', usuario, function(error, result) {
            if (error)
                return lanzarError(error.reason);
            
            if (result.existeProfesor)
                lanzarError('Este profesor ya está asignado al curso');

            Router.go('paginaCurso', {_id: result._id});
        });
    }
});

Template.cambiarProfesor.helpers({
    users: function() {
        return Meteor.users.find({roles: 'profesor'});
    },

    mensajeError: function(field) {
        return Session.get('erroresCambiarProfesor') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresCambiarProfesor') [field] ? 'has-error' : '';
    }
});