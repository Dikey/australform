Template.verificarPassword.onCreated(function() {
    Session.set('erroresUsuario', {});
});

Template.verificarPassword.events({
    'submit form': function(e) {
        e.preventDefault();

        var userId = this._id;

        var newPassword = $('#contraseña').val();

        var verificacion = $('#verificacion').val();

        var user = {
            userId: userId,
            newPassword: newPassword
        }

        var errores = validarPassword(user);
        if(!user.newPassword)
            return Session.set('erroresUsuario', errores);
        
        if (verificacion === newPassword) {
            
            Meteor.call('cambiarPassword', user, function(error, result) {
                if (error)
                    return lanzarError(error.reason);
                    
                Router.go('/');
            });

        } else {
            lanzarError('Las contraseñas no coinciden');
        }
    }
});

Template.verificarPassword.helpers({
    mensajeError: function(field) {
        return Session.get('erroresUsuario') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresUsuario') [field] ? 'has-error' : '';
    }
});