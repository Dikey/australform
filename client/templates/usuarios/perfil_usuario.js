Template.perfilUsuario.events({
    
});

Template.perfilUsuario.helpers({
    
    //funciones Cursos
    //---------------------------------------------------------------------------------------
    cursosAprobados: function() {
        var userId = Meteor.userId();
        
        //Obtenemos todos los Desarrollos del usuario logueado en donde la variable cursoId exista y
        //esté en estado aprobado
        return Desarrollos.find({ $and: [{userId: userId, aprobado: true}, {cursoId: {$exists: true}}] });
    },
    
    nombreCursosAprobados: function() {
        var cursoId = this.cursoId;
        var curso =  Cursos.findOne({_id: cursoId});
        
        return curso.nombreCurso;
    },
    
    nombreAreaCursos: function() {
        var cursoId = this.cursoId;
        var curso =  Cursos.findOne({_id: cursoId});
        var area = Areas.findOne({_id: curso.areaId});
        
        return area.nombreArea; 
    },
    
    notaObtenida: function() {
        return this.notaObtenida;
    },
    
    nombreCurso: function() {
        return this.nombreCurso;
    },
    
    nombreArea: function() {
        var area = Areas.findOne({_id: this.areaId});
        
        return area.nombreArea;
    },
    
    cursosEnDesarrollo: function() {
        var userId = Meteor.userId();
        
        var cursos = [];
        
        //Obtenemos todos los Desarrollos del usuario logueado en donde la variable cursoId exista y
        //la variable aprobado no
        var cursosAprobados = Desarrollos.find({ $and: [{userId: userId}, {cursoId: {$exists: true}}, {aprobado: {$exists: false}}] }).count();
        
        var i = 0;
        while (i < cursosAprobados) {
            var desarrollos = Desarrollos.findOne({ $and: [{userId: userId}, {cursoId: {$exists: true}}, {aprobado: {$exists: false}}] }, {skip: i});
            cursos[i] = Cursos.findOne({_id: desarrollos.cursoId});
            i++;
        }
        return cursos;
    },
    
    porcentajeDesarrollo: function() {
        var userId = Meteor.userId();
        
        var cursoId = this._id;
        
        var tamañoModulos = Modulos.find({cursoId: cursoId}).count();
        var modulosAprobados = 0;
        var porcentaje = 0;
        
        var ciclo = 0;
        while (ciclo < tamañoModulos) {
        
            var modulos = Modulos.findOne({cursoId: cursoId}, {skip: ciclo});
            var moduloId = modulos._id;
            var modulo = Desarrollos.findOne({ $and: [{userId: userId}, {moduloId: moduloId}]});
    
            if(!modulo || !modulo.aprobado) {
                
            } else {
                if (modulo.aprobado)
                    modulosAprobados++;
            }
            ciclo++;
        }
        
        //multiplicamos la cantidad de modulos aprobados por 100 y lo dividimos por el tamaño de modulos del curso + 1
        //redondeamos ese valor y lo guardamos en la variable porcentaje
        porcentaje = Math.round((modulosAprobados * 100)/(tamañoModulos+1));
        
        return porcentaje;
    },
    
    //funciones Metas
    //---------------------------------------------------------------------------------------
    metasFinalizadas: function() {
        var userId = Meteor.userId();
        
        var metaId = this._id;
        
        return Desarrollos.find({ $and: [{userId: userId, aprobado: true}, {metaId: {$exists: true}}] });
    },
    
    nombreMetas: function() {
        var meta = Metas.findOne({_id: this.metaId});
        
        return meta.nombreMeta;
    },
    
    notaObtenidaMeta: function() {
        return this.promedioMeta;
    },
    
    metasEnDesarrollo: function() {
        var userId = Meteor.userId();
            
        var desarrollos = Metas.find().count();
                                            
        var i = 0;
        var metas = [];
        while (i < desarrollos) {
            var meta = Metas.findOne({}, {skip: i});
            
            //preguntamos si la meta está finalizada
            var finalizada = Desarrollos.findOne({ $and: [{userId: userId, aprobado: true}, {metaId: meta._id}] });
            
            if (!finalizada)
                metas[i] = meta;
                                  
            i++;
        }
        return metas;
    },
    
    nombreMeta: function() {
        return this.nombreMeta
    },
    
    porcentajeDesarrolloMeta: function() {
        var userId = Meteor.userId();
        
        var cantidadModulos = 0;
        var cantidadModulosAprobados = 0;
        
        var cursos = this.cursosMeta.length;
        
        //ciclo que recorre el arreglo de _id de los cursos de una meta 
        var i = 0;
        while(i < cursos) {
            var curso = Cursos.findOne({_id: this.cursosMeta[i]});
                
            var desarrollo = Desarrollos.findOne({ $and: [{userId: userId, aprobado: true}, {cursoId: curso._id}] });
                
            if (desarrollo)
                cantidadModulosAprobados++;
            
            //ciclo que recorre los modulos asociados a cada curso
            var j = 0;
            var modulos = Modulos.find({cursoId: curso._id}).count();
            
            cantidadModulos += modulos+1;
            
            while(j < modulos) {
                var modulo = Modulos.findOne({cursoId: curso._id}, {skip: j});
                var moduloId = modulo._id;
                
                var desarrollo = Desarrollos.findOne({ $and: [{userId: userId, aprobado: true}, {moduloId: moduloId}] });
                
                if(desarrollo)
                    cantidadModulosAprobados++;
                    
                j++;
            }
            i++
        }
        
        var resultado = Math.round(cantidadModulosAprobados*100/cantidadModulos);
        
        return resultado;
    },
    
    //funciones perfil profesor
    //---------------------------------------------------------------------------------------
    
    cursosPublicados: function() {
        var userId = Meteor.userId();
        
        return Cursos.find({userId: userId, estado: "publicado"});
    },
    
    cursosFinalizados: function() {
        var userId = Meteor.userId();
        
        return Cursos.find({userId: userId, estado: "finalizado"});
    },
    
    cursosPendientes: function() {
        var userId = Meteor.userId();
        
        return Cursos.find({userId: userId, estado: "pendiente"});
    }
});
    
    
    
    
    