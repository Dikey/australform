Template.cambiarPassword.onCreated(function() {
    Session.set('erroresUsuario', {});
});

Template.cambiarPassword.events({
    'submit form': function(e) {
        e.preventDefault();

        var email = $('#email').val();

        if(email) {

            var findUser = Meteor.users.findOne({"emails.0.address": email});
            
            var userId = findUser._id;
            
            var user = {
                email: email
            }
        } else {
            var user = {
                email: ""
            }
        }

        var errores = validarEmail(user);
        if(!user.email)
            return Session.set('erroresUsuario', errores);

        //enviamos el correo con el link hacia la vista verificar_password al email ingresado
        //link: "cursosformaustral/<userId>/verificarPassword"

        var link = "https://cursosformaustral.cl/"+ userId +"/verificarPassword";

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("demo").innerHTML = this.responseText; //puedes hacer lo que quieras con la respuesta.
            }
        };
        xhttp.open("POST", "https://www.incluyes.com/1pruebas/email.php", true); //true si espera la respuesta, false si solamente envía y no se queda a la espera.
        xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        //Modificar los parametros con los datos que deben ser enviados en el correo.
        xhttp.send("titulo=Cambio de contraseña FormAustral&destinatario="+ email +"&mensaje=Para cambiar tu contraseña visita el siguiente link: "+ link); //envía los datos

        //Router.go('verificarPassword', {_id: userId});
    }
});

Template.cambiarPassword.helpers({
    mensajeError: function(field) {
        return Session.get('erroresUsuario') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresUsuario') [field] ? 'has-error' : '';
    }
});