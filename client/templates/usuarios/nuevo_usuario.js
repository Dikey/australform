Template.nuevoUsuario.onCreated(function() {
    Session.set('erroresNuevoUsuario', {});
});

Template.nuevoUsuario.events({
    'submit form': function(e) {
        e.preventDefault();
        
        var usuario = {
            username: $('#usuario').val(),
            email: $('#email').val(),
            password: $('#contraseña').val(),
            profile: {
                fullname: $('#nombre').val(),
                rut: $('#rut').val(),
                nacionalidad: $('#nacionalidad').val(),
                direccion: $('#direccion').val(),
                telefono: $('#telefono').val(),
                escolaridad: $('#escolaridad').val(),
                fechaNacimiento: $('#date').val(),
                ocupacion: $('#ocupacion').val(),
                cargo: $('#cargo').val(),
                empresa: $('#empresa').val()
            }
        };
        
        var errores = validarUsuario(usuario);
        if(errores.username || errores.email || errores.fullname || errores.password || errores.rut || errores.nacionalidad || errores.direccion ||
            errores.telefono || errores.escolaridad || errores.fechaNacimiento || errores.ocupacion || errores.cargo || errores.empresa)
            return Session.set('erroresNuevoUsuario', errores);
        
        var verificacion = $('#verificacion').val();
        
        if (verificacion === usuario.password) {

            Meteor.call('crearUsuario', usuario, function(error) {
                if (error) return lanzarError(error.reason);
            });
            
            Router.go('listadoAreas');

        } else {
            lanzarError('Las contraseñas no coinciden');
        }
    }
});

Template.nuevoUsuario.helpers({
    usuarios: function() {
        return Meteor.users.find({roles: 'user'});
    },

    cursos: function() {
        var userId = this._id;
        return Inscripciones.find({usuarioId: userId});
    },

    nombreCurso: function() {
        var curso = Cursos.findOne({_id: this.cursoId});
        return curso.nombreCurso;
    },

    avance: function() {
        var userId = this.usuarioId;
        var cursoId = this.cursoId;

        var desarrollo = Desarrollos.findOne({ $and: [{userId: userId}, {cursoId: cursoId}] });

        if(desarrollo) {
            if(desarrollo.aprobado)
                return 100;
        } else {

            var cantidadModulos = Modulos.find({cursoId: cursoId}).count();
            var aprobados = 0;

            var i = 0;
            while(i < cantidadModulos){
                var moduloId = Modulos.findOne({ $and: [{cursoId: cursoId}, {orden: i+1}] })._id;
                var des = Desarrollos.findOne({ $and: [{userId: userId}, {moduloId: moduloId}] });

                if(des)
                    if(des.aprobado)
                        aprobados++;

                i++;
            }

            return Math.round((aprobados*100)/(cantidadModulos+1));
        }

    },

    tiempo: function() {
        var userId = this.usuarioId;
        var cursoId = this.cursoId;

        var timer = Timers.findOne({ $and: [{userId: userId}, {cursoId: cursoId}] });

        if(timer)
            return Number(timer.duracionTimer) + " minutos";
    },

    nombreUsuario: function() {
        var usuario = Meteor.users.findOne({_id: this.usuarioId});
        return usuario.profile.fullname;
    },

    nombreUsuario2: function() {
        return this.profile.fullname;
    },

    usuario: function() {
        var usuario = Meteor.users.findOne({_id: this.usuarioId});
        return usuario.username;
    },

    correo: function() {
        return this.emails;
    },

    nombreCorreo:function() {
        return this.address;
    },

    
    // tabla maestra

    correosUsuario: function() {
        var usuario = Meteor.users.findOne({_id: this.usuarioId});
        return usuario.emails;
    },

    cargoUsuario: function() {
        var usuario = Meteor.users.findOne({_id: this.usuarioId});
        return usuario.profile.cargo;
    },

    empresaUsuario: function() {
        var usuario = Meteor.users.findOne({_id: this.usuarioId});
        return usuario.profile.empresa;
    },

    telefonoUsuario: function() {
        var usuario = Meteor.users.findOne({_id: this.usuarioId});
        return usuario.profile.telefono;
    },

    fecha: function() {
        var inscripciones = this.fechaInscripcion;

        var dia = inscripciones.getDate();
        var mes = inscripciones.getMonth();
        var año = inscripciones.getFullYear();

        var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

        return dia + " de " + meses[mes] + " de " + año;
    },

    fechaInicio: function() {
        var timer = Timers.findOne({ $and: [ {userId: this.usuarioId}, {cursoId: this.cursoId} ] });

        if(timer){

            
            var fechaInicio = timer.fechaInicio;
            
            var dia = fechaInicio.getDate();
            var mes = fechaInicio.getMonth();
            var año = fechaInicio.getFullYear();
            
            var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
            
            return dia + " de " + meses[mes] + " de " + año;
        }
    },

    fechaTermino: function() {
        var desarrollo = Desarrollos.findOne({ $and: [ {userId: this.usuarioId}, {cursoId: this.cursoId} ] });

        if(desarrollo) {

            var fechaTermino = desarrollo.fechaRevision;
            
            var dia = fechaTermino.getDate();
            var mes = fechaTermino.getMonth();
            var año = fechaTermino.getFullYear();
            
            var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
            
            return dia + " de " + meses[mes] + " de " + año;
        }
    },

    // --------------

    rut: function() {
        var usuario = Meteor.users.findOne({_id: this.usuarioId});
        return usuario.profile.rut;
    },

    rut2: function() {
        return this.profile.rut;
    },

    cargo: function() {
        return this.profile.cargo;
    },

    empresa: function() {
        return this.profile.empresa;
    },

    telefono: function() {
        return this.profile.telefono;
    },

    mensajeError: function(field) {
        return Session.get('erroresNuevoUsuario') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresNuevoUsuario') [field] ? 'has-error' : '';
    }
});