Template.inscribirUsuario.onCreated(function() {
    Session.set('erroresInscribirUsuario', {});
});

Template.inscribirUsuario.events({
    'submit form': function (e) {
        e.preventDefault();

        var $curso = $(e.target).find('[id=seleccion-curso]');
        var cursoSeleccionado = $curso.val();

        if(!cursoSeleccionado) {
            var cursoId = "";
        } else {
            var curso = Cursos.findOne({nombreCurso: cursoSeleccionado});
            var cursoId = curso._id;
        }

        var username = $(e.target).find('[id=seleccion-usuario]').val();

        if(!username) {
            var usuarioId = "";
        } else {
            var usuario = Meteor.users.findOne({username: username});
            var usuarioId = usuario._id;
        }

        var inscripcion = {
            cursoId: cursoId,
            usuarioId: usuarioId,
            username: usuario.profile.fullname,
            rut: usuario.profile.rut,
            cargo: usuario.profile.cargo
        }

        var errores = validarInscripcion(inscripcion);
        if (errores.cursoId || errores.usuarioId)
            return Session.set('erroresInscribirUsuario', errores);

        //llamada al método de creación de inscripcion desde el servidor
        Meteor.call('insertarInscripcion', inscripcion, function(error, result) {
            if (error)
                return lanzarError(error.reason);
            
            if (result.existeInscripcion)
                lanzarError('Este usuario ya ha sido inscrito al curso');
        });
    }   
});

Template.inscribirUsuario.helpers({
    cursos: function() {
        return Cursos.find({estado: 'publicado'});
    },

    users: function() {
        return Meteor.users.find({roles: "user"});
    },

    inscritos: function() {
        var inscritos = Inscripciones.find();

        return inscritos;
    },

    correo: function() {
        var usuario = Meteor.users.findOne({_id: this.usuarioId});
        return usuario.emails;
    },

    nombreCorreo:function() {
        return this.address;
    },

    telefonoUsuario: function() {
        var usuario = Meteor.users.findOne({_id: this.usuarioId});
        return usuario.profile.telefono;
    },

    /*
    nombreUsuario: function() {
        var usuarioId = this.usuarioId;

        var nombreUsuario = Meteor.users.findOne({_id: usuarioId}).profile.fullname;

        return nombreUsuario;
    },

    rut: function() {
        var usuarioId = this.usuarioId;

        var rutUsuario = Meteor.users.findOne({_id: usuarioId}).profile.rut;

        return rutUsuario;
    },

    cargo: function() {
        var usuarioId = this.usuarioId;

        var cargoUsuario = Meteor.users.findOne({_id: usuarioId}).profile.cargo;

        return cargoUsuario;
    },
    */

    cursoInscrito: function() {
        var inscripciones = Inscripciones.find({usuarioId: this.usuarioId});

        return inscripciones;
    },

    fecha: function() {
        var inscripciones = this.fechaInscripcion;

        var dia = inscripciones.getDate();
        var mes = inscripciones.getMonth();
        var año = inscripciones.getFullYear();

        var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

        return dia + " de " + meses[mes] + " de " + año;
    },

    nombreCursoInscrito: function() {
        var nombreCurso = Cursos.findOne({_id: this.cursoId}).nombreCurso;

        return nombreCurso;
    },

    mensajeError: function(field) {
        return Session.get('erroresInscribirUsuario') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresInscribirUsuario') [field] ? 'has-error' : '';
    }
});