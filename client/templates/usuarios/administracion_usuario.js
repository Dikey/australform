Template.administracionUsuario.onCreated(function() {
    Session.set('erroresUsuario', {});
});

Template.administracionUsuario.events({
    'submit form': function(e) {
        e.preventDefault();
        
        var usuario = $('#usuario').val();
        var contraseña = $('#contraseña').val();
        
        var usuarioIngresado = {
            username: usuario,
            password: contraseña
        }
        
        var errores = validarUsuarioIngresado(usuarioIngresado);
        if(!usuarioIngresado.username)
            return Session.set('erroresUsuario', errores);
        
        Meteor.loginWithPassword(usuario, contraseña, function(error) {
            //if(error) lanzarError(error.reason);
            
            if(error && error.reason==="Incorrect password") {
                return lanzarError("Contraseña incorrecta");
            }
            
            if(error && error.reason==="User not found") {
                return lanzarError("Usuario no encontrado");
            }
            
            Router.go('listadoAreas');
        });
    }
});

Template.administracionUsuario.helpers({
    mensajeError: function(field) {
        return Session.get('erroresUsuario') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresUsuario') [field] ? 'has-error' : '';
    }
});