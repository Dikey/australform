Template.itemCurso.helpers({
    estadoTerminado: function() {
        var estado = this.estado;
        if (estado==="finalizado")
            return true;
            else
                return false;
    },
    
    cursosPreviosAprobados: function() {
        if (!this.prerrequisitos)
            return true;
        else {
            var user = Meteor.user();
            if (!user)
                return false;
            
            var pre = this.prerrequisitos;
            var tamaño = pre.length;
        
            var aprobados = 0;
            var ciclo = 0;
            while (ciclo < tamaño) {
                var desarrollo = Desarrollos.findOne({ $and: [{userId: user._id}, {cursoId: pre[ciclo]}]});
                if (!desarrollo || !desarrollo.aprobado)
                    return false;
                else
                    if (desarrollo.aprobado)
                        aprobados++;
                ciclo++;
            }
            
            if(aprobados===tamaño)
                return true;
        }
    },
    
    prerrequisitos: function () {
        var pre = this.prerrequisitos;
        var tamaño = pre.length;
        
        var i = 0;
        while (i < tamaño) {
            pre[i] = Cursos.findOne({_id: pre[i]});
            i++;
        }
        return pre;
    },
    
    estaAprobado: function() {
        var userId = Meteor.userId();
        
        var desarrollo = Desarrollos.findOne({ $and: [{userId: userId}, {cursoId: this._id}]});
        
        if (!desarrollo || !desarrollo.aprobado)
            return false;
        else
            if (desarrollo.aprobado)
                return true;
    },

    estaInscrito: function() {
        var userId = Meteor.userId();

        var cursoId = this._id;

        var inscripciones = Inscripciones.findOne({ $and: [{cursoId: cursoId}, {usuarioId: userId}] })

        if (!inscripciones)
            return false;
        else
            return true;
    },

    // ---------------- Nuevas funciones ---------------- //

    //Devuelve todas las imágenes de la coleccion asociadas al ID del curso actual
    imagenes: function() {

        var imagenId = this.imagenId;

        return Imagenes.find({_id: imagenId});
    },

    cantidadModulos: function() {
        var cantidad = Modulos.find({cursoId: this._id}).count();

        if(cantidad==1)
            return cantidad + " módulo";
        else
            return cantidad + " módulos";
    },
    
    //devuelve la cantidad total de módulos aprobados en el curso
    cantidadModulosAprobados: function() {
        var cantidadModulos = Modulos.find({cursoId: this._id}).count();
        
        var cantidadAprobados = 0;
        var pos = 0;
        while(pos<cantidadModulos) {
            var moduloId = Modulos.findOne({ $and: [{cursoId: this._id}, {orden: pos+1}] })._id;
            var desarrollo = Desarrollos.findOne({ $and: [{moduloId: moduloId}, {userId: Meteor.userId()}] });

            if(desarrollo && desarrollo.aprobado)
                cantidadAprobados++;
            pos++;
        }

        return cantidadAprobados;
    },

    porcentajeAprobado: function() {

        var cantidadModulos = Modulos.find({cursoId: this._id}).count();
        
        var cantidadAprobados = 0;
        var pos = 0;
        while(pos<cantidadModulos) {
            var moduloId = Modulos.findOne({ $and: [{cursoId: this._id}, {orden: pos+1}] })._id;
            var desarrollo = Desarrollos.findOne({ $and: [{moduloId: moduloId}, {userId: Meteor.userId()}] });

            if(desarrollo) {
                if(desarrollo.aprobado)
                    cantidadAprobados++;
            } 
            pos++;
        }

        if(this.pruebaFinal) {
            cantidadModulos++;

            var prueba = Desarrollos.findOne({ $and: [{cursoId: this._id}, {userId: Meteor.userId()}, {tipo: "prueba"}] });
            if(prueba) {
                if(prueba.aprobado)
                    cantidadAprobados++;
            }
        }

        if(this.trabajoFinal) {
            cantidadModulos++;

            var trabajo = Desarrollos.findOne({ $and: [{cursoId: this._id}, {userId: Meteor.userId()}, {tipo: "trabajo"}] });
            if(trabajo) {
                if(trabajo.aprobado)
                    cantidadAprobados++;
            }
        }

        return Math.round( ((cantidadAprobados*100)/cantidadModulos)*100/100 );
    }
})




