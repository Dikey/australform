Template.paginaCertificado.events({

    'click #realizarEncuesta': function() {
        document.getElementById("div-confirmacion").style.display = "block";
    },
    
    'click #checkbox-confirmacion': function(e) {
        e.preventDefault();

        var curso = {
            cursoId: this.cursoId,
            tipo: "Encuesta"
        }
        
        Meteor.call('realizarEncuesta', curso, function(error, result) {
            if (error)
                return lanzarError(error.reason);
                
            if (result.encuestaRealizada)
                lanzarError('Ya has realizado la encuesta');
                
            Router.go('paginaCertificado', {_id: result._id});
        });
    }
});

Template.paginaCertificado.helpers({
    nombreUsuario: function() {
        var user = Meteor.user();
        return user.profile.fullname;
    },

    cargo: function() {
        var user = Meteor.user();
        return user.profile.cargo;
    },

    institucion: function() {
        var user = Meteor.user();
        return user.profile.empresa;
    },

    nombreCurso: function() {
        var cursoId = this.cursoId;

        return Cursos.findOne({_id: cursoId}).nombreCurso;
    },

    fechaAprobacion: function() {
        var fecha = this.fechaDesarrollo;

        var dia = fecha.getDate();
        var mes = fecha.getMonth();
        var año = fecha.getFullYear();

        var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

        return dia + " de " + meses[mes] + " de " + año;
    },

    encuestaRealizada: function() {
        var desarrollo = this;

        var cursoId = desarrollo.cursoId;

        var encuesta = Desarrollos.findOne( { $and: [ {cursoId: cursoId}, {userId: Meteor.userId()}, {tipo: "Encuesta"}] });

        if(encuesta)
            return true;
    }
});