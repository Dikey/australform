Template.paginaPruebaCurso.onCreated(function() {
    Session.set('erroresPaginaPruebaCurso', {});
});

Template.paginaPruebaCurso.events({
    'submit form': function(e, template) {
        e.preventDefault();

        var check = template.$("#checkbox-confirmacion")[0];

        if (!check.checked) {
            return lanzarError("Debes aceptar el campo al final de la prueba para enviar tus resultados");
        } else {

            var tiempo = document.getElementById("demo").innerHTML;

            if(tiempo == "Tiempo Finalizado") {
                return lanzarError("Se ha agotado el tiempo para realizar tu prueba, debes empezar nuevamente");
            }

            var cantidadPreguntas = this.pruebaFinal.cantidadPreguntas;
            var cantidadPreguntasDesarrollo = this.pruebaFinal.cantidadPreguntasDesarrollo;
            var preguntasCorrectas = 0;

            var pos = 0;
            while(pos<cantidadPreguntas) {

                var posId = pos+1;
                /* preguntamos el tipo de pregunta */
                var idPregunta = template.$("#idPregunta"+posId)[0].text;
                var tipo = Tests.findOne({_id: idPregunta}).tipo;
                
                if(tipo=="Alternativas") {
                    var cantidad = 4;
                    var correcta = Tests.findOne({_id: idPregunta}).correcta;
                } else {
                    var cantidad = 2;
                    var correcta = Tests.findOne({_id: idPregunta}).resultado;
                }    
                
                for(var n = 1; n<=cantidad; n++) {
                    
                    var number1 = Number((pos*4)+n);
                    var concat1 = "#radio" + number1;
                    var radio1 = template.$(concat1)[0];
                    
                    if(radio1.checked) {
                        var concat2 = "#alt" + number1;
                        var text = template.$(concat2)[0].textContent;
                        
                        if(text==correcta) {
                            preguntasCorrectas++;
                        }
                        
                    }
                }

                pos++;
            }

            var enEspera = true;

            if (cantidadPreguntasDesarrollo== 0)
                enEspera = false;
        
            var desarrollo = {
                cursoId: this._id,
                preguntasCorrectas: preguntasCorrectas,
                totalPreguntas: cantidadPreguntas,
                totalPreguntasDesarrollo: cantidadPreguntasDesarrollo,
                enEspera: enEspera,
                respuestasDesarrollo: []
            }
            
            var i = 0;

            //ingresamos las preguntas y respuestas del usuario dentro de una tabla llamada respuestasDesarrollo
            while(i < cantidadPreguntasDesarrollo) {

                var posicion = i+1;

                var enunciado = template.$("#pregunta-desarrollo"+posicion)[0].getAttribute('name');
                var respuesta = template.$("#respuesta-pregunta"+posicion)[0].value;

                desarrollo.respuestasDesarrollo[i] = {
                    enunciado: enunciado,
                    respuesta: respuesta,
                    posicion: posicion
                }

                i++;
            }

            //validaciones

            Meteor.call('realizarPruebaCurso', desarrollo, function(error, result) {
                if (error)
                    return lanzarError(error.reason);
                    
                Router.go('paginaResultados', {_id: result._id});
            });
        }
    }
});

Template.paginaPruebaCurso.helpers({
    cantidadPreguntas: function() {
        return this.pruebaFinal.cantidadPreguntas;
    },

    cantidadPreguntasDesarrollo: function() {
        return this.pruebaFinal.cantidadPreguntasDesarrollo;
    },

    esAlternativa: function() {
        if(this.tipo=="Alternativas") {
            return true;
        } else {
            return false;
        }
    },

    //Solo me devuelve una Id de una sola pregunta
    idPregunta: function() {
        return this._id;
    },

    idRadio1: function() {
        var resultado = ((this.posicion-1)*4)+1;

        return resultado;
    },

    idRadio2: function() {
        var resultado = ((this.posicion-1)*4)+2;

        return resultado;
    },

    idRadio3: function() {
        var resultado = ((this.posicion-1)*4)+3;

        return resultado;
    },

    idRadio4: function() {
        var resultado = ((this.posicion-1)*4)+4;

        return resultado;
    },

    nombreUsuario: function() {
        var usuario = Meteor.user();

        return usuario.profile.fullname;
    },

    enunciadoPrueba: function() {
        return this.pruebaFinal.enunciadoPrueba;
    },

    duracion: function() {
        return this.pruebaFinal.duracion;
    },
    
    preguntas: function() {
        var stop = this.pruebaFinal.cantidadPreguntas;
        var aceptaAlternativas = this.pruebaFinal.aceptaAlternativas;
        var aceptaVerdaderoYFalso = this.pruebaFinal.aceptaVerdaderoYFalso;
        var preguntas = [];
        var preguntasRandomizadas = 0;
        var numeroRandom = 0;
        var pos = 0;

        while(preguntasRandomizadas<stop){

            var posModulo = 0;
            var modulos = Modulos.find({cursoId: this._id}).count();
            while(posModulo<modulos && preguntasRandomizadas<stop) {
                var moduloUno = Modulos.findOne({ $and: [ {cursoId: this._id}, {orden: posModulo+1} ] });

                var contenido = Contenidos.findOne( { $and: [ {moduloId: moduloUno._id}, {orden: pos+1} ] });
                
                if(!contenido){
                    pos = 0;
                    posModulo++;
                } else {
                    var cantidadPreguntas = Tests.find({contenidoId: contenido._id}).count();
                    if(cantidadPreguntas!=0) {

                        /* Capturar excepcion de repetición de números randoms */

                        
                        //obtenemos la pregunta del pool de preguntas de acuerdo al random y al tipo de pregunta
                        if(aceptaAlternativas && aceptaVerdaderoYFalso) {
                            //generamos un número randómico
                            numeroRandom = Math.floor(Math.random() * cantidadPreguntas) +1;
                            var variable = Tests.findOne( { $and: [ {contenidoId: contenido._id}, {orden: numeroRandom} ] });
                        }

                        if(aceptaAlternativas && !aceptaVerdaderoYFalso) {
                            var cantidad = Tests.find( { $and: [ {contenidoId: contenido._id}, {tipo: "Alternativas"} ] }).count();
                            var i = 0;
                            var arreglo = [];
                            while(i<cantidad) {
                                arreglo[i] = Tests.findOne( { $and: [ {contenidoId: contenido._id}, {tipo: "Alternativas"} ] }, {skip: i});
                                i++;
                            }
                            numeroRandom = Math.floor(Math.random() * cantidad);
                            var variable = arreglo[numeroRandom];
                        }

                        if(!aceptaAlternativas && aceptaVerdaderoYFalso) {
                            var cantidad = Tests.find( { $and: [ {contenidoId: contenido._id}, {tipo: "Verdadero y Falso"} ] }).count();
                            var i = 0;
                            var arreglo = [];
                            while(i<cantidad) {
                                arreglo[i] = Tests.findOne( { $and: [ {contenidoId: contenido._id}, {tipo: "Verdadero y Falso"} ] }, {skip: i});
                                i++;
                            }
                            numeroRandom = Math.floor(Math.random() * cantidad);
                            var variable = arreglo[numeroRandom];
                        }

                        //creamos una pregunta
                        if(variable.tipo=="Alternativas") {

                            var pregunta = {
                                _id: variable._id,
                                contenidoId: variable.contenidoId,
                                tipo: variable.tipo,
                                enunciado: variable.enunciado,
                                posicion: preguntasRandomizadas+1
                            }

                            randomAlt = Math.floor(Math.random() * 4 ) + 1;

                            if(randomAlt==1) {
                                pregunta.alt1 = variable.correcta;
                                pregunta.alt2 = variable.alt1;
                                pregunta.alt3 = variable.alt2;
                                pregunta.alt4 = variable.alt3;
                            }

                            if(randomAlt==2) {
                                pregunta.alt1 = variable.alt1;
                                pregunta.alt2 = variable.correcta;
                                pregunta.alt3 = variable.alt2;
                                pregunta.alt4 = variable.alt3;
                            }

                            if(randomAlt==3) {
                                pregunta.alt1 = variable.alt1;
                                pregunta.alt2 = variable.alt2;
                                pregunta.alt3 = variable.correcta;
                                pregunta.alt4 = variable.alt3;
                            }

                            if(randomAlt==4) {
                                pregunta.alt1 = variable.alt1;
                                pregunta.alt2 = variable.alt2;
                                pregunta.alt3 = variable.alt3;
                                pregunta.alt4 = variable.correcta;
                            }

                        } else {

                            var pregunta = {
                                _id: variable._id,
                                contenidoId: variable.contenidoId,
                                tipo: variable.tipo,
                                sentencia: variable.sentencia,
                                resultado: variable.resultado,
                                posicion: preguntasRandomizadas+1
                            }
                        }

                        //se agrega la pregunta al array de preguntas a mostrar
                        preguntas[preguntasRandomizadas] = pregunta;
                        //----------------------

                        preguntasRandomizadas++;
                    }
                    pos++;
                }//final else (!contenido)

            }

        }//final while (preguntasRandomizadas<stop)

        return preguntas;
    },

    preguntasDesarrollo: function() {

        var cursoId = this._id;

        var cantidadPreguntasDesarrollo = this.pruebaFinal.cantidadPreguntasDesarrollo;

        var cantidadTotal = Tests.find({cursoId: cursoId}).count();

        var arreglo = [];

        var i = 0;
        while(i < cantidadPreguntasDesarrollo) {

            var numeroRandom = Math.floor(Math.random() * cantidadTotal) +1;

            var pregunta = Tests.findOne({ $and: [ {cursoId: cursoId}, {orden: numeroRandom} ] });
            pregunta.posicion = i+1;

            arreglo[i] = pregunta;

            i++;
        }

        return arreglo;
    }
});