Template.pruebaCurso.events({
    'submit form': function(e){
        e.preventDefault();
        var strings = [];
        var pos = -1;
        
        //guardamos los elementos del form en una variable
        var radios = document.forms[0];
        var boton = -5;
        
        //recorremos la variable identificando si los radios estan checkeados, 
        //si es así, los guardamos en su string correspondiente
        for (var i = 0; i < radios.length; i++){
            if (radios[i].checked) {
                pos++;
                boton += 5;
                
                var objeto = {
                    pregunta: radios[boton].textContent,
                    respuesta: radios[i].nextSibling.data
                };
                strings[pos] = objeto;
            }
        }
        
        //guardamos el tamaño del arreglo de respuestas correctas
        var tamañoRespuestas = this.pruebaFinal.preguntas.length;
        
        var cantidadCorrectas = 0;
        
        //comparamos las alterntivas elegidas con las respuestas correctas
        var ciclo1 = 0;
        while (ciclo1 < tamañoRespuestas){
            var pregunta = strings[ciclo1].pregunta-1;
            if (strings[ciclo1].respuesta===this.pruebaFinal.preguntas[ciclo1].preguntas[pregunta].correcta)
                cantidadCorrectas += 1;
            ciclo1++;
        }
        
        
        //guardamos la nota de aprobación del curso en una variable
        var notaAprobacion = Number(this.notaAprobacion)*10;
        
        var porcentaje = (cantidadCorrectas*100)/tamañoRespuestas;
        
        //preguntamos si tiene la totalidad de las respuestas correctas 
        if (porcentaje>=notaAprobacion){
            var userId = Meteor.userId();
            
            //Cálculo de nota obtenida
            //---------------------------
            var valorPregunta = 7/tamañoRespuestas;
        
            var notaObtenida = cantidadCorrectas*valorPregunta;
            //---------------------------
            
            //var notaObtenida = Math.round(porcentaje/10); 
        
            finalizarDesarrolloCurso(this, notaObtenida);
            
            lanzarMensaje("Felicitaciones!, has aprobado el Curso "+this.nombreCurso+
                          " con nota: "+notaObtenida);
            
            //preguntamos si la meta a la que está asociada este curso tiene todos sus cursos aprobados
            var metas = Metas.find().count();
            
            var i = 0;
            while(i < metas){
                var meta = Metas.findOne({}, {skip: i});
                var cursos = meta.cursosMeta;
                
                var userId = Meteor.userId();
                //preguntamos si la meta está finalizada
                var finalizada = Desarrollos.findOne({ $and: [{userId: userId, aprobado: true}, {metaId: meta._id}] });
                
                if (!finalizada) {
                
                    var cursosAprobados = 0;
                    var promedio = 0;
                    var j = 0;
                    while(j < cursos.length){
                        var desarrollo = Desarrollos.findOne({userId: userId ,cursoId: cursos[j]});
                    
                        if (!desarrollo || !desarrollo.aprobado){
                            j = cursos.length;
                        } else {
                            if (desarrollo.aprobado) {
                                cursosAprobados++;
                                promedio += desarrollo.notaObtenida;
                            }
                        }
                        j++;
                    }
                
                    if (cursosAprobados === cursos.length) {
                        var promedioMeta = promedio/cursos.length;  
                    
                        //Insertamos en la tabla Desarrollos que la meta ha sido finalizada
                        finalizarDesarrolloMeta(meta, promedioMeta);
                        lanzarMensaje("Felicitaciones!, has finalizado la Meta "+meta.nombreMeta+
                            " con nota: "+promedioMeta);
                    }
                }
                i++;
            }
            
            //nos redirigimos al listado de areas
            Router.go('listadoAreas');
            
        }
        else {
            //mensaje de no aprobado y redirijimos al listado de areas
            Router.go('listadoAreas');
            
            lanzarMensaje("Ups!, no has aprobado el curso, pero no te desanimes puedes intentarlo las veces que quieras");
        }
    }
});

Template.pruebaCurso.helpers({
    pruebas: function() {
        var i = 0;
        var arreglo = [];
        
        var numerosUsados = [];
        var tamaño = this.pruebaFinal.preguntas.length;
        var tamañoRandom = this.pruebaFinal.preguntas[0].preguntas.length;
        
        while (i < tamaño) {
            
            var random = Math.floor(Math.random() * (tamañoRandom-0)) + 0;
            var ciclo = 0;
            var repetido = true;
            while (repetido != false){
                while(ciclo < numerosUsados.length) {
                    if (random==numerosUsados[ciclo]) {
                        random = Math.floor(Math.random() * (tamañoRandom-0)) + 0;
                        ciclo = 0;
                    } else {
                        ciclo++;
                    }
                }
                repetido = false;
            }
            numerosUsados[i] = random;
            
            var prueba = {
                _id: i,
                numero: this.pruebaFinal.preguntas[i].preguntas[random]._id,
                correcta: this.pruebaFinal.preguntas[i].preguntas[random].correcta,
                pregunta: this.pruebaFinal.preguntas[i].preguntas[random].enunciado,
                alt1: this.pruebaFinal.preguntas[i].preguntas[random].alt1,
                alt2: this.pruebaFinal.preguntas[i].preguntas[random].alt2,
                alt3: this.pruebaFinal.preguntas[i].preguntas[random].alt3,
                alt4: this.pruebaFinal.preguntas[i].preguntas[random].alt4
            };
            
            arreglo[i] = prueba;
            i++;
        }
        
        return arreglo;
    },
    
    id: function() {
        return this._id+1;
    },
    
    numero: function() {
        return this.numero;
    }
});