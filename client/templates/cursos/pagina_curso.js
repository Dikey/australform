Template.paginaCurso.events({
    'submit form': function(e, template) {
        e.preventDefault();

        if(this.tipo=="prueba") {

            var cantidadDesarrollos = Number(this.totalPreguntasDesarrollo);
            var puntosObtenidos = 0;
            
            var i = 0;
            while(i<cantidadDesarrollos) {
                var posicion = i+1;
                
                var buena = template.$("#buena" + posicion)[0];
                var media = template.$("#media" + posicion)[0];
                
                if(buena.checked)
                    puntosObtenidos+=2;
                
                if(media.checked)
                    puntosObtenidos+=1;
                
                i++;
            }
            
            var desarrollo = {
                desarrolloId: this._id,
                cursoId: this.cursoId,
                userId: this.userId,
                preguntasCorrectas: this.preguntasCorrectas,
                porcentajeObtenido: this.porcentajeObtenido,
                notaObtenida: this.notaObtenida,
                totalPreguntasSeleccion: this.totalPreguntas,
                puntosDesarrolloObtenidos: puntosObtenidos,
                totalPreguntasDesarrollo: cantidadDesarrollos
            }
            
            //validaciones
            
            Meteor.call('realizarEvaluacionPrueba', desarrollo, function(error, result) {
                if (error)
                return lanzarError(error.reason);
                
                //Router.go('paginaCurso', {_id: this.cursoId});
            });
        }

        if(this.tipo=="trabajo") {

            var notaTrabajo = $(e.target).find('[id=nota-trabajo]').val();

            var desarrollo = {
                desarrolloId: this._id,
                cursoId: this.cursoId,
                userId: this.userId,
                trabajoFinalId: this.trabajoFinalId,
                notaTrabajoFinal: notaTrabajo,
                enEspera: false
            }

            //validaciones
            
            
            Meteor.call('realizarEvaluacionTrabajo', desarrollo, function(error, result) {
                if (error)
                return lanzarError(error.reason);
                
                //Router.go('paginaCurso', {_id: this.cursoId});
            });
        }
    },

    'click #publicarCurso': function() {
        
        //guardamos todos los modulos pertenecientes al curso dentro de la variable modulos
        var modulos = Modulos.find({cursoId: this._id});

        var cantidadModulos = modulos.count();
        
        if (!cantidadModulos)
            return lanzarError("Para publicar el curso debes crear módulos");
        
        var stringContenidos = "";
        var stringTest = "";
        
        var parametrosPruebaFinal = this.pruebaFinal;
        var parametrosTrabajoFinal = this.trabajoFinal;

        if(!parametrosPruebaFinal && !parametrosTrabajoFinal)
            return lanzarError("Para publicar el curso debes definir los parámetros de la prueba final del curso");
        
        var i = 0;
        while (i < cantidadModulos) {
            var modulo = Modulos.findOne({cursoId: this._id}, {skip: i});

            var contenido = Contenidos.findOne({moduloId: modulo._id});

            if (!contenido)
                stringContenidos = stringContenidos+" "+modulo.nombreModulo;

            var parametrosPrueba = modulo.prueba;
                
            if (!parametrosPrueba)
                stringTest = stringTest+" "+modulo.nombreModulo;
            i++;
        }
        
        if (stringContenidos)
            return lanzarError("Para publicar el curso debes añadir contenido al módulo "+stringContenidos);
        if (stringTest)
            return lanzarError("Para publicar el curso debes añadir un test al módulo "+stringTest);
        
        var idCurso = this._id;
        
        //updateamos este curso con una nueva variable de estado: finalizado
        Cursos.update(idCurso, {$set: {estado: "finalizado"}}, function(error) {
            if (error)
                return lanzarError(error.reason);
        });
        
        //creamos la notificacion para el administrador en la coleccion Notificaciones
        crearNotificacionCurso(this);
    },
    
    'click #publicarCursoAdmin': function() {

        var idCurso = this._id;
        
        //updateamos este curso con la variable de estado: publicado
        Cursos.update(idCurso, {$set: {estado: "publicado"}}, function(error) {
            if (error)
                lanzarError(error.reason);
        });
        
        //creamos la notificacion para el profesor en la coleccion notificaciones
        crearNotificacionPublicado(this);
    },

    'click #resultadosCurso': function() {

        var desarrollo = Desarrollos.findOne({ $and: [{cursoId: this._id}, {userId: Meteor.userId()}]});

        Router.go('paginaResultados', {_id: desarrollo._id});
    },

    'click #paginaCertificado': function() {

        var desarrollo = Desarrollos.findOne({ $and: [{cursoId: this._id}, {userId: Meteor.userId()}]});

        Router.go('paginaCertificado', {_id: desarrollo._id});
    },

    'click #deshabilitarCurso': function() {

        var idCurso = this._id;
        
        //updateamos este curso con la variable de estado: pendiente
        Cursos.update(idCurso, {$set: {estado: "pendiente"}}, function(error) {
            if (error)
                lanzarError(error.reason);
        });

    }
});

Template.paginaCurso.helpers({

    //funcion que permite verificar si eres el creador del curso
    propioCurso: function() {
        return this.userId === Meteor.userId();
    },
    
    estadoTerminado: function() {
        if (this.estado==="finalizado")
            return true;
        else
            return false;
    },
    
    estadoPublicado: function() {
        if (this.estado==="publicado")
            return true;
        else
            return false;
    },
    
    estadoPendiente: function() {
        if (this.estado==="pendiente")
            return true;
        else 
            return false;
    },
    
    prerrequisitos: function () {
        var pre = this.prerrequisitos;
        var tamaño = pre.length;
        
        
        var i = 0;
        while (i < tamaño) {
            pre[i] = Cursos.findOne({_id: pre[i]}); 
            i++;
        }
        return pre;
    },

    // Nuevas funciones

    //Devuelve todas las imágenes de la coleccion asociadas al ID del curso actual
    imagenes: function() {

        var imagenId = this.imagenId;

        return Imagenes.find({_id: imagenId});
    },

    //devuelve la cantidad total de módulos en el curso
    cantidadModulos: function() {
        var modulos = Modulos.find({cursoId: this._id}).count();

        return modulos;
    },

    //devuelve la cantidad total de módulos aprobados en el curso
    cantidadModulosAprobados: function() {
        var cantidadModulos = Modulos.find({cursoId: this._id}).count();
        
        var cantidadAprobados = 0;
        var pos = 0;
        while(pos<cantidadModulos) {
            var moduloId = Modulos.findOne({ $and: [{cursoId: this._id}, {orden: pos+1}] })._id;
            var desarrollo = Desarrollos.findOne({ $and: [{moduloId: moduloId}, {userId: Meteor.userId()}] });

            if(desarrollo && desarrollo.aprobado)
                cantidadAprobados++;
            pos++;
        }

        return cantidadAprobados;
    },

    porcentajeModulosAprobados: function() {
        var cantidadModulos = Modulos.find({cursoId: this._id}).count();
        
        var cantidadAprobados = 0;
        var pos = 0;
        while(pos<cantidadModulos) {
            var moduloId = Modulos.findOne({ $and: [{cursoId: this._id}, {orden: pos+1}] })._id;
            var desarrollo = Desarrollos.findOne({ $and: [{moduloId: moduloId}, {userId: Meteor.userId()}] });

            if(desarrollo && desarrollo.aprobado)
                cantidadAprobados++;
            pos++;
        }

        return (cantidadAprobados*100)/cantidadModulos;
    },

    //devuelve la sumatoria de horas de duracion de todos los videos del curso
    horasVideos: function() {
        var videos = 0;

        return videos;
    },

    tienePruebaFinal: function() {
        if (!this.pruebaFinal) {
            return false;
        } else {
            return true;
        }
    },

    tieneTrabajoFinal: function() {
        if(!this.trabajoFinal) {
            return false;
        } else {
            return true;
        }
    },

    tienePruebayTrabajo:function() {
        if(this.pruebaFinal && this.trabajoFinal)
            return true;
    },

    pruebaAprobada: function() {

        var desarrollo = Desarrollos.findOne({ $and: [{cursoId: this._id}, {userId: Meteor.userId()}, {tipo: "prueba"}]});

        //var desarrollo = Desarrollos.findOne({ $and: [{cursoId: this._id}, {userId: Meteor.userId()}]});

        if(desarrollo){
            if(desarrollo.aprobado)
            return true;
        }
    },

    trabajoAprobado: function() {
        var desarrollo = Desarrollos.findOne({ $and: [{cursoId: this._id}, {userId: Meteor.userId()}, {tipo: "trabajo"}]});

        if(desarrollo){
            if(desarrollo.aprobado)
            return true;
        }
    },

    porcentajePrueba: function() {
        var desarrollo = Desarrollos.findOne({ $and: [{cursoId: this._id}, {userId: Meteor.userId()}, {tipo: "prueba"}]});

        if(desarrollo) {
            if(desarrollo.aprobado)
            return 100;
        }
    },

    porcentajeTrabajo: function() {
        var desarrollo = Desarrollos.findOne({ $and: [{cursoId: this._id}, {userId: Meteor.userId()}, {tipo: "trabajo"}]});

        if(desarrollo) {
            if(desarrollo.aprobado)
            return 100;
        }
    },


    tieneDesarrollo: function() {
        var desarrollo = Desarrollos.findOne({ $and: [{cursoId: this._id}, {userId: Meteor.userId()}]});

        if(desarrollo)
            return true;
    },

    hayEvaluaciones: function() {
        var desarrollo = Desarrollos.findOne({cursoId: this._id});

        if(desarrollo)
            return true;
    },

    evaluaciones: function() {
        return Desarrollos.find({cursoId: this._id});
    },

    esPrueba: function() {
        if(this.tipo=="prueba")
            return true;
    },

    esTrabajo: function() {
        if(this.tipo=="trabajo")
            return true;
    },

    enEspera: function() {
        return this.enEspera;
    },

    trabajo: function() {
        var trabajoFinalId = this.trabajoFinalId;

        return Pdfs.find({_id: trabajoFinalId});
    },

    duracionPrueba: function() {
        return this.pruebaFinal.duracion + " minutos"; // o horas dependiendo
    },

    duracionTrabajo: function() {
        return this.trabajoFinal.duracionTrabajo;
    },

    preguntasDesarrollo: function() {

        var desarrolloId = this._id;

        var cantidadDesarrollos = Desarrollos.findOne({_id: desarrolloId});

        if(cantidadDesarrollos) {
            return cantidadDesarrollos.respuestasDesarrollo;
        }
    },

    totalPreguntasDesarrollo: function() {

        var desarrolloId = this._id;

        var cantidadDesarrollos = Desarrollos.findOne({_id: desarrolloId});

        if(cantidadDesarrollos) {
            return cantidadDesarrollos.totalPreguntasDesarrollo;
        }
    },

    modulosAprobados: function() {

        var cantidadModulos = Modulos.find({cursoId: this._id}).count();
        
        var cantidadAprobados = 0;
        var pos = 0;
        while(pos<cantidadModulos) {
            var moduloId = Modulos.findOne({ $and: [{cursoId: this._id}, {orden: pos+1}] })._id;
            var desarrollo = Desarrollos.findOne({ $and: [{moduloId: moduloId}, {userId: Meteor.userId()}] });

            if(!desarrollo) {
                return false;
            } else {
                if(desarrollo.aprobado) {
                    cantidadAprobados++;
                } else {
                    return false;
                }
            }
            pos++;
        }

        if(cantidadAprobados==cantidadModulos) {
            return true;
        } else {
            return false;
        }
    },
    
    //variables de configuracion
    confDatos: function() {
        return Configuracion.findOne({}).datos;
    },
    
    confDescripcion: function() {
        return Configuracion.findOne({}).descripcion;
    },
    
    confObjetivo: function() {
        return Configuracion.findOne({}).objetivo;
    },
    
    confCurso: function() {
        return Configuracion.findOne({}).curso;
    },
    
    confModulo: function() {
        return Configuracion.findOne({}).modulo;
    },
    
    confContenido: function() {
        return Configuracion.findOne({}).contenido;
    },
    
    confCategoria: function() {
        return Configuracion.findOne({}).categoria;
    },
    
    confUsuario: function() {
        return Configuracion.findOne({}).usuario;
    },
    
    confProfesor: function() {
        return Configuracion.findOne({}).profesor;
    }
});