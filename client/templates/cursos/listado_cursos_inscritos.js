Template.listadoCursosInscritos.helpers({
    
    //Devolvemos solo las cursos en las que está inscrito el usuario
    cursosUsuarioInscritos: function() {
        var userId = Meteor.userId();
        var cursos = [];

        var inscripciones = Inscripciones.find({usuarioId: userId});
        var cantidad = inscripciones.count();

        var i = 0;
        while(i<cantidad) {
            var inscripcion = Inscripciones.findOne({usuarioId: userId}, {skip: i});
            var cursoId = inscripcion.cursoId;
            cursos[i] = Cursos.findOne({_id: cursoId});
            i++;
        }

        return cursos;
    }
});