Template.listadoCursos.helpers({
    cursosAdmin: function() {
        return Cursos.find({areaId: this._id, estado: "finalizado"});
    },
    
    cursos: function() {
        var userId = Meteor.userId();
        return Cursos.find({areaId: this._id, userId: userId});
    },
    
    cursosUsuario: function() {
        return Cursos.find({areaId: this._id, estado: "publicado"});
    }
});