Template.crearCurso.onCreated(function() {
    Session.set('erroresCrearCurso', {});
});

Template.crearCurso.events({
    'submit form': function (e) {
        e.preventDefault();
        
        var $area =  $(e.target).find('[id=seleccion-area]');
        var areaSeleccionada = $area.val();
        
        if(!areaSeleccionada) {
            var areaId = "";
        } else {
            var area = Areas.findOne({nombreArea: areaSeleccionada});
            var areaId = area._id;
        }
        
        var strings = [];
        var checkbox = document.forms[0];
        
        //recorremos la variable checkbox identificando si los checkbox del form estan checkeados, 
        //si es así, los guardamos en su string correspondiente
        var pos = -1;
        for (var i = 0; i < checkbox.length; i++){
            if (checkbox[i].checked) {
                pos++;
                strings[pos] = checkbox[i+1].textContent;
            }
        }

        var files = document.getElementById("imagenCurso").files;

        if(!files)
            var imagenId = "";
        else {
            for(var i = 0, ln = files.length; i < ln; i++) {
                var imagen = Imagenes.insert(files[i], function(err, fileObj) {
                    //Insertado nuevo documento con ID fileObj._id
                });
                var imagenId = imagen._id;
            }
        }

        var nivel =  $(e.target).find('[id=seleccion-nivel]');
        var seleccionNivel = nivel.val();
        
        if(!nivel)
            seleccionNivel = "";

        var video =  $(e.target).find('[name=video]').val();

        if (video) {
            var expReg = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
            var match = video.match(expReg);
                if (match && match[2].length == 11) {
                    //la url es válida
                } else {
                    //la url es inválida
                    return lanzarError("La URL del video ingresada es inválida");
                }
        }
        
        var curso = {
            nombreCurso: $(e.target).find('[name=curso]').val(),
            areaId: areaId,
            imagenId: imagenId,
            objetivoCurso: $(e.target).find('[name=objetivoCurso]').val(),
            descripcionCurso: $(e.target).find('[name=descripcionCurso]').val(),
            nivel: seleccionNivel,
            notaAprobacion: $(e.target).find('[name=aprobacionCurso]').val(),
            duracionCurso: $(e.target).find('[name=duracion]').val(),
            mensajeBienvenida: $(e.target).find('[name=mensajeBienvenida]').val(),
            video: video,
            prerrequisitos: strings,
            estado: "pendiente"
        };
            
        var errores = validarCurso(curso);
        if (errores.nombreCurso || errores.imagenId || errores.areaId || errores.objetivoCurso || errores.descripcionCurso || errores.nivel 
            || errores.notaAprobacion || errores.duracionCurso || errores.mensajeBienvenida)
            return Session.set('erroresCrearCurso', errores);
        
        //llamada hacia metodo insertarCurso para insertar cursos que funciona desde el servidor
        Meteor.call('insertarCurso', curso, function(error, result) {
            if (error)
                return lanzarError(error.reason);
                
            if (result.existeCurso)
                lanzarError('Este curso ya ha sido creado');
                
            Router.go('paginaCurso', {_id: result._id});
        });
    },
    
    'click .checkbox': function() {
        
    }
});

Template.crearCurso.helpers({
    areas: function () {
        return Areas.find();
    },
    
    cursos: function () {
        var userId = Meteor.userId();
        return Cursos.find({userId: userId});
    },
    
    cursosPrerrequisitos: function () {
        return Cursos.find({estado: "publicado"});
    },
    
    nombreAreas: function () {
        var area = Areas.findOne({_id: this.areaId}); 
        return area.nombreArea;
    },
    
    prerrequisitos: function () {
        var pre = this.prerrequisitos;
        var tamaño = pre.length;
        
        var i = 0;
        while (i < tamaño) {
            pre[i] = Cursos.findOne({_id: pre[i]});
            i++;
        }
        return pre;
    },
    
    mensajeError: function(field) {
        return Session.get('erroresCrearCurso') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresCrearCurso') [field] ? 'has-error' : '';
    }
});











