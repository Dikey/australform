Template.definirPruebaCurso.onCreated(function() {
    Session.set('erroresDefinirPruebaCurso', {});
});

Template.definirPruebaCurso.events({
    'submit form': function(e, template) {
        e.preventDefault();

        var check3 = template.$("#test3")[0];
        var check4 = template.$("#test4")[0];

        if (!check3.checked && !check4.checked)
            return lanzarError("Debes seleccionar almenos una evaluación final");

        var curso = this;
        var idCurso = curso._id;

        //funcion cantidad de helper
        var cantidad = 0;

        var pos0 = 0;
        var modulos = Modulos.find({cursoId: idCurso}).count();

        while(pos0<modulos){

            var moduloUno = Modulos.findOne({cursoId: idCurso}, {skip: pos0});
            pos0++;
            
            var pos = 0;
            var contenidos = Contenidos.find({moduloId: moduloUno._id}).count();
            
            while(pos<contenidos) {
                var uno = Contenidos.findOne({moduloId: moduloUno._id}, {skip: pos});
                cantidad += Tests.find({contenidoId: uno._id}).count();
                pos++;
            }
        }
        //------------------------------

        var total = cantidad;
        var cantidadPreguntas = $(e.target).find('[id=cantidad-preguntas]').val();
        var cantidadPreguntasDesarrollo = $(e.target).find('[id=cantidad-preguntas-desarrollo]').val();

        if(cantidadPreguntas>total)
            return lanzarError("No tienes la cantidad suficiente de preguntas agregadas");

        var check1 = template.$("#test1")[0];
        var check2 = template.$("#test2")[0];

        var alternativas = false;
        var verdadero = false;

        if (check1.checked)
            alternativas = true;

        if(check2.checked)
            verdadero = true;

            
        var duracion = $(e.target).find('[id=duracion]').val();
        

        if(check3.checked) {
            
            //validación
            
            pruebaFinal = {
                aceptaAlternativas: alternativas,
                aceptaVerdaderoYFalso: verdadero,
                cantidadPreguntas: cantidadPreguntas,
                cantidadPreguntasDesarrollo: cantidadPreguntasDesarrollo,
                duracion: duracion
            }

            Cursos.update(idCurso, {$set: {pruebaFinal: pruebaFinal}}, function(error) {
                if (error) {
                    lanzarError(error.reason);
                } else {
                    Router.go('paginaCurso', {_id: idCurso});
                }
            });

        }

        if(check4.checked) {

            var enunciadoPrueba = $(e.target).find('[id=enunciado-prueba]').val();
            
            var ponderacion = $(e.target).find('[id=ponderacion]').val();
            
            var duracionTrabajo = $(e.target).find('[id=duracionTrabajo]').val();

            //validación
            
            trabajoFinal = {
                enunciadoPrueba: enunciadoPrueba,
                ponderacionPruebaFinal: ponderacion,
                duracionTrabajo: duracionTrabajo
            }

            Cursos.update(idCurso, {$set: {trabajoFinal: trabajoFinal}}, function(error) {
                if (error) {
                    lanzarError(error.reason);
                } else {
                    Router.go('paginaCurso', {_id: idCurso});
                }
            });
            
        }

    }
});

Template.definirPruebaCurso.helpers({
    cantidad: function() {
        var cantidad = 0;

        var pos0 = 0;
        var modulos = Modulos.find({cursoId: this._id}).count();

        while(pos0<modulos){

            var moduloUno = Modulos.findOne({cursoId: this._id}, {skip: pos0});
            pos0++;
            
            var pos = 0;
            var contenidos = Contenidos.find({moduloId: moduloUno._id}).count();
            
            while(pos<contenidos) {
                var uno = Contenidos.findOne({moduloId: moduloUno._id}, {skip: pos});
                cantidad += Tests.find({contenidoId: uno._id}).count();
                pos++;
            }
        }

        return cantidad;
    },

    cantidadDesarrollo: function() {
        var cantidad = Tests.find({cursoId: this._id}).count();

        return cantidad;
    },

    mensajeError: function(field) {
        return Session.get('erroresDefinirPruebaCurso') [field];
    },

    claseError: function(field) {
        return !!Session.get('erroresDefinirPruebaCurso') [field] ? 'has-error' : '';
    }
});