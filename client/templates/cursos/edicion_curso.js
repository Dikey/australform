Template.edicionCurso.onCreated(function () {
    Session.set('erroresEdicionCurso', {});
});

Template.edicionCurso.events({
    'submit form': function(e)  {
        e.preventDefault();
        
        var idCurso = this._id;
        
        var $area =  $(e.target).find('[id=seleccion-area]');
        var areaSeleccionada = $area.val();
        
        if(!areaSeleccionada) {
            var areaId = "";
        } else {
            var area = Areas.findOne({nombreArea: areaSeleccionada});
            var areaId = area._id;
        }

        
        var strings = [];
        /*
        var checkbox = document.forms[0];
        
        //recorremos la variable checkbox identificando si los checkbox del form estan checkeados, 
        //si es así, los guardamos en su string correspondiente
        
        var pos = -1;
        for (var i = 0; i < checkbox.length; i++){
            if (checkbox[i].checked) {
                pos++;
                strings[pos] = checkbox[i+1].textContent;
            }
        }
        */

        var imagenName = $(e.target).find('[id=imagenName]')[0].text;
        var nameFile = $(e.target).find('[id=archivoCurso]')[0].value;

        var files = document.getElementById("imagenCurso").files;

        if(!files)
            var imagenId = "";
        else {

            if(imagenName == nameFile) {

                var imagenId = $(e.target).find('[id=imagenId]')[0].text;

            } else {
                
                for(var i = 0, ln = files.length; i < ln; i++) {
                    var imagen = Imagenes.insert(files[i], function(err, fileObj) {
                        //Insertado nuevo documento con ID fileObj._id
                    });
                    var imagenId = imagen._id;
                }
            }
        }

        var nivel =  $(e.target).find('[id=seleccion-nivel]');
        var seleccionNivel = nivel.val();
        
        if(!nivel)
            seleccionNivel = "";

        var video =  $(e.target).find('[name=video]').val();

        if (video) {
            var expReg = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=|\?v=)([^#\&\?]*).*/;
            var match = video.match(expReg);
                if (match && match[2].length == 11) {
                    //la url es válida
                } else {
                    //la url es inválida
                    return lanzarError("La URL del video ingresada es inválida");
                }
        }
        
        var curso = {
            nombreCurso: $(e.target).find('[name=curso]').val(),
            areaId: areaId,
            imagenId: imagenId,
            objetivoCurso: $(e.target).find('[name=objetivoCurso]').val(),
            descripcionCurso: $(e.target).find('[name=descripcionCurso]').val(),
            nivel: seleccionNivel,
            notaAprobacion: $(e.target).find('[name=aprobacionCurso]').val(),
            duracionCurso: $(e.target).find('[name=duracion]').val(),
            mensajeBienvenida: $(e.target).find('[name=mensajeBienvenida]').val(),
            video: video,
            prerrequisitos: strings,
            estado: "pendiente"
        };
        
        var errores = validarCurso(curso);
        if (errores.nombreCurso || errores.imagenId || errores.areaId || errores.objetivoCurso || errores.descripcionCurso || errores.nivel 
            || errores.notaAprobacion || errores.duracionCurso || errores.mensajeBienvenida)
            return Session.set('erroresEdicionCurso', errores);
        
        Cursos.update(idCurso, {$set: curso}, function(error) {
            if (error) {
                lanzarError(error.reason);
            } else {
                Router.go('paginaCurso', {_id: idCurso});
            }
        });
    },
    
    'click #eliminar': function(e) {
        e.preventDefault();
    
        var idCurso = this._id;
        
        if (confirm("¿Seguro quieres eliminarlo?")) {
                
            Cursos.remove(idCurso);
            Router.go('listadoAreas');
        }
    }
});

Template.edicionCurso.helpers({
    //Devuelve todas las imágenes de la coleccion asociadas al ID del curso actual
    imagenes: function() {

        var imagenId = this.imagenId;

        return Imagenes.find({_id: imagenId});
    },

    areas: function () {
        return Areas.find();
    },

    areaSeleccionada: function() {
        return Areas.findOne({_id: this.areaId}).nombreArea;
    },
    
    cursos: function () {
        var userId = Meteor.userId();
        return Cursos.find({userId: userId});
    },
    
    cursosPrerrequisitos: function () {
        return Cursos.find({estado: "publicado"});
    },
    
    nombreAreas: function () {
        var area = Areas.findOne({_id: this.areaId}); 
        return area.nombreArea;
    },
    
    prerrequisitos: function () {
        var pre = this.prerrequisitos;
        var tamaño = pre.length;
        
        var i = 0;
        while (i < tamaño) {
            pre[i] = Cursos.findOne({_id: pre[i]});
            i++;
        }
        return pre;
    },
    
    mensajeError: function(field) {
        return Session.get('erroresEdicionCurso') [field];
    },
    
    claseError: function(field) {
        return !!Session.get('erroresEdicionCurso') [field] ? 'has-error' : '';
    }
});





