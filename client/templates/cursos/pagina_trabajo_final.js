Template.paginaTrabajoFinal.onCreated(function() {
    Session.set('erroresPaginaTrabajoFinal', {});
})

Template.paginaTrabajoFinal.events({ 
    'submit form': function(e, template) {
        e.preventDefault();

        var check = template.$("#checkbox-confirmacion")[0];

        if (!check.checked) {
            return lanzarError("Debes aceptar el campo al final de la prueba para enviar tus resultados");
        } else {

            var trabajoFinal = document.getElementById("trabajoFinal").files;

            if(!trabajoFinal)
                var trabajoFinalId = "";
            else {
                for(var i = 0, ln = trabajoFinal.length; i < ln; i++) {
                    var pdf = Pdfs.insert(trabajoFinal[i], function(err, fileObj) {
                        //Insertado nuevo documento con ID fileObj._id
                    });
                    var trabajoFinalId = pdf._id;
                }
            }
                
            var desarrollo = {
                cursoId: this._id,
                trabajoFinalId: trabajoFinalId,
                enEspera: true
            }

            //validaciones

            Meteor.call('realizarTrabajoFinal', desarrollo, function(error, result) {
                if (error)
                    return lanzarError(error.reason);
                    
                Router.go('paginaResultados', {_id: result._id});
            });

        }
    }
}); 

Template.paginaTrabajoFinal.helpers({ 
    duracion: function() {
        return this.trabajoFinal.duracionTrabajo;
    },

    nombreUsuario: function() {
        var usuario = Meteor.user();

        return usuario.profile.fullname;
    }
}); 