Template.paginaBienvenida.events({
    'submit form': function(e){
        e.preventDefault();

        var primerModulo = $(e.target).find('[id=primerModulo]');

        var moduloId = primerModulo.text();

        var cursoId = Modulos.findOne({_id: moduloId}).cursoId;

        if(Roles.userIsInRole(Meteor.user(), 'user')) {
            var variable = {
                cursoId: cursoId
            }
            
            Meteor.call('iniciarTimer', variable, function(error, result) {
                if (error)
                    return lanzarError(error.reason);
                    
                Router.go('paginaModulo', {_id: moduloId});
            });
        }
    }
});

Template.paginaBienvenida.helpers({
    primerModulo: function() {
        var cursoId = this._id;

        var primerModulo = Modulos.findOne({ $and: [ {cursoId: cursoId}, {orden: 1} ]});

        return primerModulo._id;
    },

    link: function() {
        var video = this.video;
        
        var embed = "embed/";
        
        var link = video.substring(0,24) + embed + video.substring(32);
        return link;
    },

    nombreUsuario: function() {
        var usuario = Meteor.user();

        return usuario.profile.fullname;
    },

    tieneVideo: function() {
        var video = this.video;

        if(video)
        return true;
    },

    imagenes: function() {

        var imagenId = this.imagenId;

        return Imagenes.find({_id: imagenId});
    }

});