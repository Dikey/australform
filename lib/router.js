Router.configure({
    layoutTemplate: 'diseño',
    loadingTemplate: 'cargando',
    notFoundTemplate: 'noEncontrado',
    waitOn: function() { 
        return [Meteor.subscribe('areas'), Meteor.subscribe('cursos'), 
                Meteor.subscribe('modulos'), Meteor.subscribe('contenidos'),
                Meteor.subscribe('metas'), Meteor.subscribe('notificaciones'),
                Meteor.subscribe('desarrollos'), Meteor.subscribe('inscripciones'), 
                Meteor.subscribe('imagenes'), Meteor.subscribe('tests'),
                Meteor.subscribe('pdfs'), Meteor.subscribe('audios'),
                Meteor.subscribe('configuracion'), Meteor.subscribe('timers')]
    }
});


Router.route('/listadoAreas', {name: 'listadoAreas'});

Router.route('/configuracion', {name: 'paginaConfiguracion'});

/*
Router.route('/configuracion', {
    name: 'paginaConfiguracion',
    data: function() { return Configuracion.findOne(); }
});*/

Router.route('/areas/:_id', {
    name: 'paginaArea',
    data: function() { return Areas.findOne(this.params._id); }
});

Router.route('/areas/:_id/editar', {
    name: 'edicionArea',
    data: function() { return Areas.findOne(this.params._id); }
});

Router.route('/cursos/:_id', {
    name: 'paginaCurso',
    data: function() { return Cursos.findOne(this.params._id); }
});

Router.route('/cursos/bienvenida/:_id', {
    name: 'paginaBienvenida',
    data: function() { return Cursos.findOne(this.params._id); }
});

Router.route('/cursos/:_id/crearModulo', {
    name: 'crearModuloDesdeCurso',
    data: function() { return Cursos.findOne(this.params._id); }
});

Router.route('/cursos/:_id/editar', {
    name: 'edicionCurso',
    data: function() { return Cursos.findOne(this.params._id); }
});

Router.route('/cursos/prueba/:_id', {
    name: 'pruebaCurso',
    data: function() { return Cursos.findOne(this.params._id); }
});

Router.route('/cursos/modulos/:_id', {
    name: 'paginaModulo',
    /*
    waitOn: function() {
        return Meteor.subscribe('comentarios', this.params._id);
    },*/
    data: function() { return Modulos.findOne(this.params._id); }
});

Router.route('/cursos/modulos/contenidos/:_id', {
    name: 'paginaContenido',
    waitOn: function() {
        return Meteor.subscribe('comentarios', this.params._id);
    },
    data: function() { return Contenidos.findOne(this.params._id); }
});

//historial de notificaciones
Router.route('/notificaciones', {
    name: 'historialNotificaciones',
    waitOn: function() {
        return Meteor.subscribe('historialNotificaciones');
    },
    data: function() { return Notificaciones.find(); }
});

//listado de comentarios
Router.route('/cursos/modulos/contenidos/:_id/comentarios', {
    name: 'listadoComentarios',
    waitOn: function() {
        return Meteor.subscribe('comentarios', this.params._id);
    },
    data: function() { return Contenidos.findOne(this.params._id); }
});

//edicion de comentarios
Router.route('/cursos/modulos/contenidos/:contenidoId/comentario/:_id/editar', {
    name: 'editarComentario',
    waitOn: function() {
        return Meteor.subscribe('comentarios', this.params.contenidoId);
    },
    data: function() { return Comentarios.findOne(this.params._id); }
});

Router.route('/cursos/modulos/contenidos/:_id/editarTituloContenido', {
    name: 'edicionTituloContenido',
    data: function() { return Contenidos.findOne(this.params._id); }
});

//ruta para añadir un archivo a los contenidos
Router.route('/cursos/modulos/contenidos/:_id/addArchivo', {
    name: 'addArchivo',
    data: function() { return Contenidos.findOne(this.params._id); }
});

//ruta para añadir un video a los contenidos
Router.route('/cursos/modulos/contenidos/:_id/addVideo', {
    name: 'addVideo',
    data: function() { return Contenidos.findOne(this.params._id); }
});

//ruta para añadir un audio a los contenidos
Router.route('/cursos/modulos/contenidos/:_id/addAudio', {
    name: 'addAudio',
    data: function() { return Contenidos.findOne(this.params._id); }
});

//rutas de edicion

Router.route('/cursos/modulos/contenidos/:_id/editarArchivo', {
    name: 'edicionArchivo',
    data: function() { return this.params; }
});

Router.route('/cursos/modulos/contenidos/:url/editarVideo', {
    name: 'edicionVideo',
    data: function() { return this.params; }
});

Router.route('/cursos/modulos/contenidos/:_id/editarAudio', {
    name: 'edicionAudio',
    data: function() { return this.params; }
});

//-----------------

Router.route('/cursos/modulos/:_id/crearContenido', {
    name: 'crearContenidoDesdeModulo',
    data: function() { return Modulos.findOne(this.params._id); }
});

Router.route('/cursos/modulos/:_id/editar', {
    name: 'edicionModulo',
    data: function() { return Modulos.findOne(this.params._id); } 
});

//deprecated
Router.route('/cursos/modulos/test/:_id', {
    name: 'paginaTest',
    data: function() { return Modulos.findOne(this.params._id); }
});

//deprecated
Router.route('/cursos/modulos/test/:_id/crearTest', {
    name: 'crearTestDesdeModulo',
    data: function() { return Modulos.findOne(this.params._id); }
});

//deprecated
Router.route('/cursos/modulos/test/:_id/editar', {
    name: 'edicionTest',
    data: function() { return Modulos.findOne(this.params._id); }
});

Router.route('/cursos/modulos/contenidos/:_id/crearPregunta', {
    name: 'agregarPreguntaDesdeContenido',
    data: function() { return Contenidos.findOne(this.params._id); }
});

Router.route('/cursos/:_id/crearPreguntaDesarrollo', {
    name: 'agregarPreguntaDesdeCurso',
    data: function() { return Cursos.findOne(this.params._id); }
});

Router.route('/cursos/modulos/contenidos/:_id/editarPregunta', {
    name: 'editarPregunta',
    data: function() { return Tests.findOne(this.params._id); }
});

Router.route('/cursos/modulos/contenido/:_id/listadoPreguntas', {
    name: 'paginaPreguntas',
    data: function() { return Contenidos.findOne(this.params._id); }
});

Router.route('/cursos/:_id/listadoPreguntasDesarrollo', {
    name: 'paginaPreguntasDesarrollo',
    data: function() { return Cursos.findOne(this.params._id); }
});

Router.route('/cursos/modulos/resultadosPrueba/:_id', {
    name: 'paginaResultados',
    data: function() { return Desarrollos.findOne(this.params._id); }
});

Router.route('/cursos/modulos/contenido/:_id/definirParametros', {
    name: 'definirPruebaContenido',
    data: function() { return Contenidos.findOne(this.params._id); }
});

Router.route('/cursos/modulos/contenido/:_id/prueba', {
    name: 'paginaPruebaContenidos',
    data: function() { return Contenidos.findOne(this.params._id); }
});

Router.route('/cursos/modulos/:_id/definirParametros', {
    name: 'definirPruebaModulo',
    data: function() { return Modulos.findOne(this.params._id); }
});

Router.route('/cursos/modulos/:_id/prueba', {
    name: 'paginaPruebaModulo',
    data: function() { return Modulos.findOne(this.params._id); }
});

Router.route('/cursos/:_id/definirParametros', {
    name: 'definirPruebaCurso',
    data: function() { return Cursos.findOne(this.params._id); }
});

Router.route('/cursos/:_id/pruebaFinal', {
    name: 'paginaPruebaCurso',
    data: function() { return Cursos.findOne(this.params._id); }
});

Router.route('/cursos/:_id/trabajoFinal', {
    name: 'paginaTrabajoFinal',
    data: function() { return Cursos.findOne(this.params._id); }
});

// ruta certificado
Router.route('/cursos/:_id/certificado', {
    name: 'paginaCertificado',
    data: function() { return Desarrollos.findOne(this.params._id); }
})

Router.route('/metas/:_id', {
    name: 'itemMeta',
    data: function() { return Metas.findOne(this.params._id); }
});

Router.route('/inscribirUsuario', {
    name: 'inscribirUsuario',
    waitOn: function() {
        return Meteor.subscribe('userList');
    },
    data: function() {
        return Meteor.users.find({});
    }
});

//ruta cambio password
Router.route('/cambiarPassword', {
    name: 'cambiarPassword',
    waitOn: function() {
        return Meteor.subscribe('userList');
    },
    data: function() {
        return Meteor.users.find({});
    }
});

//ruta verificacion password
Router.route('/:_id/verificarPassword', {
    name: 'verificarPassword',
    waitOn: function() {
        return Meteor.subscribe('userList');
    },
    data: function() {
        return Meteor.users.findOne(this.params._id);
    }
});

Router.route('/cursos/:_id/cambiarProfesor', {
    name: 'cambiarProfesor',
    waitOn: function() {
        return Meteor.subscribe('profesorList');
    },
    data: function() {
        //return Meteor.users.find({});

        return Cursos.findOne(this.params._id);
    }
});

Router.route('/crearProfesor', {
    name: 'crearProfesor',
    waitOn: function() {
        return Meteor.subscribe('profesorList');
    }
});

//editar Profesor
Router.route('/:_id/editarProfesor', {
    name: 'editarProfesor',
    waitOn: function() {
        return Meteor.subscribe('profesorList');
    },
    data: function() {
        return Meteor.users.findOne(this.params._id);
    }
});

Router.route('/registro', {
    name: 'nuevoUsuario',
    waitOn: function() {
        return Meteor.subscribe('userList');
    }
});

//editar Usuario
Router.route('/:_id/editarUsuario', {
    name: 'editarUsuario',
    waitOn: function() {
        return Meteor.subscribe('userList');
    },
    data: function() {
        return Meteor.users.findOne(this.params._id);
    }
});

Router.route('/crearCurso', {name: 'crearCurso'});
Router.route('/crearModulo', {name: 'crearModulo'});
Router.route('/crearContenido', {name: 'crearContenido'});
Router.route('/agregarPregunta', {name: 'agregarPregunta'});
Router.route('/crearTest', {name: 'crearTest'});
Router.route('/', {name: 'administracionUsuario'});
Router.route('/perfilUsuario', {name: 'perfilUsuario'});
Router.route('/crearMetas', {name: 'crearMetas'});
Router.route('/crearArea', {name: 'crearArea'});
Router.route('/listadoProfesores', {name: 'listadoProfesores'});
Router.route('/listadoMetas', {name: 'listadoMetas'});
Router.route('/crearComentario', {name: 'crearComentario'});
Router.route('/contactarSoporte', {name: 'contactarSoporte'});

var requireLogin = function() {
    if (! Meteor.user()) {
       if(Meteor.loggingIn()) {
           this.render(this.loadingTemplate);
       }else{
           this.render('accesoDenegado');
       }
    } else {
        this.next();
    }
}

//realizar metodo de comprobar si está inscrito al curso

//realizar metodo de comprobacion de modulosPrevio
//para verificar si se accede mediante la url

//realizar metodo de comprobacion de Cursos
//para verificar si se accede mediante la url

Router.onBeforeAction('dataNotFound', {only: 'paginaCurso'});
Router.onBeforeAction('dataNotFound', {only: 'paginaModulo'});
Router.onBeforeAction(requireLogin, {only: 'crearCurso'});
Router.onBeforeAction(requireLogin, {only: 'crearModulo'});
Router.onBeforeAction(requireLogin, {only: 'crearContenido'});
Router.onBeforeAction(requireLogin, {only: 'crearTest'});
Router.onBeforeAction(requireLogin, {only: 'perfilUsuario'});
Router.onBeforeAction(requireLogin, {only: 'crearProfesor'});
Router.onBeforeAction(requireLogin, {only: 'crearMetas'});
Router.onBeforeAction(requireLogin, {only: 'crearArea'});
Router.onBeforeAction(requireLogin, {only: 'paginaCurso'});
Router.onBeforeAction(requireLogin, {only: 'paginaModulo'});
Router.onBeforeAction(requireLogin, {only: 'paginaContenido'});
Router.onBeforeAction(requireLogin, {only: 'edicionTest'});
Router.onBeforeAction(requireLogin, {only: 'listadoAreas'});
Router.onBeforeAction(requireLogin, {only: 'listadoMetas'});
Router.onBeforeAction(requireLogin, {only: 'nuevoUsuario'});
Router.onBeforeAction(requireLogin, {only: 'inscribirUsuario'});
Router.onBeforeAction(requireLogin, {only: 'paginaBienvenida'});

Router.route('/descargar-data', function() {
    var data = Desarrollos.find().fetch();

    var fields = [
        {
            key: 'id',
            title: 'URL',
            transform: function(val, doc) {
                return Router.url('desarrollos.show', { _id: val });
            }
        },
        {
            key: 'message',
            title: 'Message'
        },
        {
            key: 'viewsCount',
            title: 'Views',
            type: 'number'
        }
    ];

    var title = 'Desarrollos';

    var file = Excel.export(title, fields, data);

    var headers = {
        'Content-type': 'application/vnd.openxmlformats',
        'Content-Disposition': 'attachment; filename=' + title + '.xlsx'
    };

    this.response.writeHead(200, headers);
    this.response.end(file, 'binary');
}, { where: 'server' });