validarUsuario = function(usuario) {
    var errores = {};
    if(!usuario.username)
        errores.username = "Por favor introduce un nombre de usuario";
    if(!usuario.email)
        errores.email = "Por favor introduce un correo electrónico";
    if(!usuario.profile.fullname)
        errores.fullname = "Por favor ingrese su nombre completo";
    if(!usuario.profile.rut)
        errores.rut = "Por favor ingrese su rut";
    if(!usuario.profile.nacionalidad)
        errores.nacionalidad = "Por favor ingrese su nacionalidad";
    if(!usuario.profile.direccion)
        errores.direccion = "Por favor ingrese su direccion";
    if(!usuario.profile.telefono)
        errores.telefono = "Por favor ingrese su telefono";
    if(!usuario.profile.escolaridad)
        errores.escolaridad = "Por favor ingrese su escolaridad";
    if(!usuario.profile.fechaNacimiento)
        errores.fechaNacimiento = "Por favor ingrese su fecha de nacimiento";
    if(!usuario.profile.ocupacion)
        errores.ocupacion = "Por favor ingrese su ocupacion";
    if(!usuario.profile.cargo)
        errores.cargo = "Por favor ingrese su cargo";
    if(!usuario.profile.descripcion)
        errores.descripcion = "Por favor ingrese una reseña";
    if(!usuario.profile.empresa)
        errores.empresa = "Por favor ingrese su empresa";
    if(!usuario.password)
        errores.password = "Por favor ingrese una contraseña";
    return errores;
}

validarUsuarioIngresado = function(usuario) {
    var errores = {};
    if(!usuario.username)
        errores.username = "Por favor introduce un nombre de usuario";
    return errores;
}

validarEmail = function(usuario) {
    var errores = {};
    if(!usuario.email)
        errores.email = "Por favor introduce un correo electrónico";
    return errores;
}

validarPassword = function(usuario) {
    var errores = {};
    if(!usuario.newPassword)
        errores.newPassword = "Por favor introduce una contraseña";
    return errores;
}

Meteor.methods({
    cambiarPassword: function(userAttributes) {
        check(userAttributes, {
            userId: String,
            newPassword: String
        });

        Accounts.setPassword(userAttributes.userId, userAttributes.newPassword);

    },

    crearUsuario: function(userAttributes) {
        check(Meteor.userId(), String);
        check(userAttributes, {
            username: String,
            email: String,
            password: String,
            profile: Object
        });

        var usuario = {
            username: userAttributes.username,
            email: userAttributes.email,
            password: userAttributes.password,
            profile: userAttributes.profile
        }

        var id = Accounts.createUser(usuario);

        Roles.addUsersToRoles(id, 'user');
    },

    editarUsuario: function(id, userAttributes) {
        /*
        check(Meteor.userId(), String);
        check(userAttributes, {
            username: String,
            email: String,
            password: String,
            profile: Object
        });
        */

        var username = userAttributes.username;
        var email = userAttributes.email;
        var password = userAttributes.password;
        var profile = userAttributes.profile;

        // Update user
        Meteor.users.update(id, {
            $set: {
                username: username,
                'emails.0.address': email,
                profile: profile
            }
        });

        // Update password
        if(password)
            Accounts.setPassword(id, password);

    },

    crearProfesor: function(userAttributes) {
        check(Meteor.userId(), String);
        check(userAttributes, {
            username: String,
            email: String,
            password: String,
            profile: Object
        });

        var usuario = {
            username: userAttributes.username,
            email: userAttributes.email,
            password: userAttributes.password,
            profile: userAttributes.profile
        }

        var id = Accounts.createUser(usuario);

        Roles.addUsersToRoles(id, 'profesor');
    },

    editarProfesor: function(id, userAttributes) {
        /*
        check(Meteor.userId(), String);
        check(userAttributes, {
            username: String,
            email: String,
            password: String,
            profile: Object
        });
        */

        var username = userAttributes.username;
        var email = userAttributes.email;
        var password = userAttributes.password;
        var profile = userAttributes.profile;

        // Update user
        Meteor.users.update(id, {
            $set: {
                username: username,
                'emails.0.address': email,
                profile: profile
            }
        });

        // Update password
        if(password)
            Accounts.setPassword(id, password);
    },

    insertRoleUser: function(userAttributes) {
        check(Meteor.userId(), String);
        check(userAttributes, {
            username: String,
            email: String,
            password: String,
            profile: Object
        });
            
        var user = Meteor.user();
            
        Roles.addUsersToRoles(user._id, 'user');
    },
        
    insertRoleProfesor: function(userAttributes) {
        check(Meteor.userId(), String);
        check(userAttributes, {
            username: String,
            email: String,
            password: String,
            profile: Object
        });
            
        var user = Meteor.user();
            
        Roles.addUsersToRoles(user._id, 'profesor');
    },

    insertRoleAdmin: function(userAttributes) {
        check(Meteor.userId(), String);
        check(userAttributes, {
            username: String,
            email: String,
            password: String,
            profile: Object
        });
            
        var user = Meteor.user();
            
        Roles.addUsersToRoles(user._id, 'admin');
    }
});