Tests = new Mongo.Collection('tests');

validarPreguntaAlternativa = function(pregunta) {
    var errores = {};
    if(!pregunta.contenidoId)
        errores.contenidoId = "Por favor selecciona un contenido";
    if(!pregunta.tipo)
        errores.tipo = "Por favor selecciona un tipo de pregunta";
    if(!pregunta.enunciado)
        errores.enunciado = "Por favor escribe el enunciado de la pregunta";
    if(!pregunta.correcta)
        errores.correcta = "Por favor escribe la respuesta correcta de la pregunta";
    if(!pregunta.alt1)
        errores.alt1 = "Por favor escribe la alternativa número 1";
    if(!pregunta.alt2)
        errores.alt2 = "Por favor escribe la alternativa número 2";
    if(!pregunta.alt3)
        errores.alt3 = "Por favor escribe la alternativa número 3";
    return errores;
}

validarPreguntaVerdadero = function(pregunta) {
    var errores = {};
    if(!pregunta.contenidoId)
        errores.contenidoId = "Por favor selecciona un contenido";
    if(!pregunta.tipo)
        errores.tipo = "Por favor selecciona un tipo de pregunta";
    if(!pregunta.sentencia)
        errores.sentencia = "Por favor escribe la sentencia de la oración";
    if(!pregunta.resultado)
        errores.resultado = "Por favor elige un resultado";
    return errores;
}

validarPreguntaDesarrollo = function(pregunta) {
    var errores = {};
    if(!pregunta.cursoId)
        errores.cursoId = "Por favor selecciona un curso";
    if(!pregunta.tipo)
        errores.tipo = "Por favor selecciona un tipo de pregunta";
    if(!pregunta.pregunta)
        errores.pregunta = "Por favor escribe la pregunta de desarrollo";
    return errores;
}

Meteor.methods({
    //método que te permite insertar preguntas de alternativas
    insertarPreguntaAlternativa: function(atributosTest) {
        check(Meteor.userId(), String);
        check(atributosTest, {
            contenidoId: String,
            tipo: String,
            enunciado: String,
            correcta: String,
            alt1: String,
            alt2: String,
            alt3: String
        });

        var cantidadPreguntas = Tests.find({contenidoId: atributosTest.contenidoId}).count();

        var usuario = Meteor.user();
        var pregunta = _.extend(atributosTest, {
            userId: usuario._id,
            autorPregunta: usuario.username,
            fechaPregunta: new Date(),
            orden: cantidadPreguntas+1
        });

        var testId = Tests.insert(pregunta);
        return {
            _id: testId
        };
    },

    //método que te permite insertar preguntas de verdadero y falso
    insertarPreguntaVerdadero: function(atributosTest) {
        check(Meteor.userId(), String);
        check(atributosTest, {
            contenidoId: String,
            tipo: String,
            sentencia: String,
            resultado: String
        });

        var cantidadPreguntas = Tests.find({contenidoId: atributosTest.contenidoId}).count();

        var usuario = Meteor.user();
        var pregunta = _.extend(atributosTest, {
            userId: usuario._id,
            autorPregunta: usuario.username,
            fechaPregunta: new Date(),
            orden: cantidadPreguntas+1
        });

        var testId = Tests.insert(pregunta);
        return {
            _id: testId
        };
    },

    insertarPreguntaDesarrollo: function(atributosTest) {
        check(Meteor.userId(), String);
        check(atributosTest, {
            cursoId: String,
            tipo: String,
            pregunta: String
        });

        var cantidadPreguntasDesarrollo = Tests.find({cursoId: atributosTest.cursoId}).count();

        var usuario = Meteor.user();
        var pregunta = _.extend(atributosTest, {
            userId: usuario._id,
            autorPregunta: usuario.username,
            fechaPregunta: new Date(),
            orden: cantidadPreguntasDesarrollo+1
        });

        var testId = Tests.insert(pregunta);
        return {
            _id: testId
        };
    }
});

Tests.allow({
    insert: function(userId, doc){
        //solo permite insertar cursos si estas logueado
        return true;
    },
    
    update: function(userId, doc){
        //solo permite insertar cursos si estas logueado
        return true;
    },
    
    remove: function(userId, doc){
        //solo permite insertar cursos si estas logueado
        return true;
    },
    
    remove: function(userId, comentario) {
        return propioDocumento(userId, comentario) ;
    }
})