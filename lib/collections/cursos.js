Cursos = new Mongo.Collection('cursos');

validarAsignacion = function (usuario) {
    var errores = {};
    if (!usuario.cursoId)
        errores.cursoId = "Por favor selecciona un curso";
    if (!usuario.userId)
        errores.userId = "Por favor selecciona una cuenta de profesor";
    return errores;
}

validarCurso = function (curso) {
    var errores = {};
    if (!curso.nombreCurso)
        errores.nombreCurso = "Por favor introduce un nombre";
    if (!curso.areaId)
        errores.areaId = "Por favor selecciona un área";
    if (!curso.imagenId)
        errores.imagenId = "Por favor inserta una imagen para el curso";
    if (!curso.objetivoCurso)
        errores.objetivoCurso = "Por favor introduce un objetivo";
    if (!curso.descripcionCurso)
        errores.descripcionCurso = "Por favor introduce una descripcion";
    if (!curso.nivel)
        errores.nivel = "Por favor elige un nivel para el curso";
    if (!curso.notaAprobacion)
        errores.notaAprobacion = "Por favor introduce una nota de aprobación del Curso";
    if (!curso.duracionCurso)
        errores.duracionCurso = "Por favor introduce la duración del Curso";
    if (!curso.mensajeBienvenida)
        errores.mensajeBienvenida = "Por favor introduce un mensaje de bienvenida";
    if (!curso.video)
        errores.video = "Por favor inserta un video de bienvenida para el curso";
    return errores;
}

Meteor.methods({    

    //método que te permite cambiar el profesor asignador del curso
    asignarProfesor: function(atributosUsuario) {
        check(Meteor.userId(), String);
        check(atributosUsuario, {
            cursoId: String,
            userId: String,
            autorCurso: String,
            emailAutor: String,
            reseña: String,
            linkedin: String,
            cargo: String
        });

        var cursoId = atributosUsuario.cursoId;

        var existeProfesor = Cursos.findOne({ $and: [{_id: cursoId}, {userId: atributosUsuario.userId}] });

        if(existeProfesor) {
            return {
                existeProfesor: true,
                _id: existeProfesor._id
            }
        }

        var curso = {
            userId: atributosUsuario.userId,
            autorCurso: atributosUsuario.autorCurso,
            emailAutor: atributosUsuario.emailAutor,
            reseña: atributosUsuario.reseña,
            linkedin: atributosUsuario.linkedin,
            cargo: atributosUsuario.cargo
        }

        //operación Update que actualiza los nuevos campos del profesor en el curso
        Cursos.update(cursoId, {$set: curso}, function(error) {
            if (error) {
                lanzarError(error.reason);
            }
        });

        return {
            _id: atributosUsuario.cursoId
        };
    },

    //método que te permite insertar Cursos
    insertarCurso: function(atributosCurso) {
        check(Meteor.userId(), String);
        check(atributosCurso, {
            nombreCurso: String,
            areaId: String,
            imagenId: String,
            objetivoCurso: String,
            descripcionCurso: String,
            nivel: String,
            notaAprobacion: String,
            duracionCurso: String,
            mensajeBienvenida: String,
            video: String,
            prerrequisitos: Array,
            estado: String
        });
        
        var cursoConMismoNombre = Cursos.findOne({nombreCurso: atributosCurso.nombreCurso});
        
        if (cursoConMismoNombre) {
            return {
                existeCurso: true,
                _id: cursoConMismoNombre._id
            }
        }
        
        var usuario = Meteor.user();
        var curso = _.extend(atributosCurso, {
            userId: usuario._id,
            autorCurso: usuario.profile.fullname,
            emailAutor: usuario.emails[0].address,
            reseña: usuario.profile.descripcion,
            linkedin: usuario.profile.linkedin,
            cargo: usuario.profile.cargo,
            fechaCurso: new Date()
        });
        
        var cursoId = Cursos.insert(curso);

        return {
            _id: cursoId
        };
    },

    definirPruebaCurso: function(atributosCurso) {
        
    }
});

Cursos.allow({
    /*
    insert: function(userId, doc){
        //solo permite insertar cursos si estas logueado
        return !! userId;
    },
    */
    
    /*
    update: function(userId, curso) {
        return propioDocumento(userId, curso) ; 
    },
    */
    update: function(userId, curso) {
        //solo permite insertar cursos si estas logueado
        return !! userId;
    },
    
    remove: function(userId, curso) {
        return propioDocumento(userId, curso) ;
    }
});

Cursos.allow({
    /*
    insert: function(userId, doc){
        //solo permite insertar cursos si estas logueado
        return !! userId;
    },
    */
    
    
    update: function(user) {
        return puedeEditar(user);
    },
    
    remove: function(user) {
        return puedeEditar(user);
    }
});







