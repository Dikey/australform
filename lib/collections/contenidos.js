Contenidos = new Mongo.Collection('contenidos');

validarContenido = function (contenido) {
    var errores = {};
    if(!contenido.tituloContenido)
        errores.tituloContenido = "Por favor ingresa el titulo del contenido";
    if(!contenido.moduloId)
        errores.moduloId = "Por favor selecciona un módulo";
    if(!contenido.duracionContenido)
        errores.duracionContenido = "Por favor ingresa la duración del contenido";
    return errores;
}

validarPdf = function (pdfs) {
    var errores = {};
    if(!pdfs.pdfId)
        errores.pdfId = "Por favor selecciona un archivo para insertar";
    return errores;
}

validarAudio = function (audios) {
    var errores = {};
    if(!audios.audioId)
        errores.audioId = "Por favor selecciona un audio para insertar";
    return errores;
}

validarVideo = function (videos) {
    var errores = {};
    if(!videos.url)
        errores.url = "Por favor ingresa un video para insertar";
    return errores;
}

Meteor.methods({

    /**
     * método que permite insertar contenidos
     * @param {*} atributosContenido 
     */
    insertarContenido: function(atributosContenido) {
        check(Meteor.userId(), String);
        check(atributosContenido, {
            tituloContenido: String,
            moduloId: String,
            duracionContenido: String,
            pdfs: Array,
            videos: Array,
            audios: Array,
            
        });

        var contenidoConMismoNombre = Contenidos.findOne({tituloContenido: atributosContenido.tituloContenido});

        if(contenidoConMismoNombre) {
            return {
                existeContenido: true,
                _id: contenidoConMismoNombre._id
            }
        }

        var cantidadContenidos = Contenidos.find({moduloId: atributosContenido.moduloId}).count();

        if(cantidadContenidos==0) {
            var contenidoAnteriorId = ""
        } else {
            var contenidoAnteriorId = Contenidos.findOne({orden: cantidadContenidos})._id;
        }

        var usuario = Meteor.user();
        var contenido = _.extend(atributosContenido, {
            userId: usuario._id,
            autorContenido: usuario.username,
            fechaContenido: new Date(),
            orden: cantidadContenidos+1,
            prerrequisito: contenidoAnteriorId
        });

        var contenidoId = Contenidos.insert(contenido);
        return {
            _id: contenidoId
        };
    },

    insertarRecursoVideo: function(atributosVideo) {
        check(Meteor.userId(), String);
        check(atributosVideo, {
            idContenido: String,
            url: String
        });

        var idContenido = atributosVideo.idContenido;
        var contenido = Contenidos.findOne({_id: idContenido});

        var ordenPdf = contenido.pdfs.length;

        var ordenAudios = contenido.audios.length;

        var ordenVideos = contenido.videos.length;

        var ordenRecurso = ordenPdf+ordenAudios+ordenVideos;

        var videos = {
            url: atributosVideo.url,
            ordenVideo: ordenVideos+1,
            ordenRecurso: ordenRecurso+1
        }

        Contenidos.update(idContenido,  { $push: { videos: videos } }, function(error) {
            if (error)
                lanzarError(error.reason);
        });

        return {
            _id: idContenido
        };
    },

    insertarRecursoAudio: function(atributosAudio) {
        check(Meteor.userId(), String);
        check(atributosAudio, {
            idContenido: String,
            audioId: String
        });

        var idContenido = atributosAudio.idContenido;
        var contenido = Contenidos.findOne({_id: idContenido});

        var ordenPdf = contenido.pdfs.length;

        var ordenAudios = contenido.audios.length;

        var ordenVideos = contenido.videos.length;

        var ordenRecurso = ordenPdf+ordenAudios+ordenVideos;

        var audios = {
            audioId: atributosAudio.audioId,
            ordenAudio: ordenAudios+1,
            ordenRecurso: ordenRecurso+1
        }

        Contenidos.update(idContenido,  { $push: { audios: audios } }, function(error) {
            if (error)
                lanzarError(error.reason);
        });

        return {
            _id: idContenido
        };
    },

    insertarRecursoPdf: function(atributosPdf) {
        check(Meteor.userId(), String);
        check(atributosPdf, {
            idContenido: String,
            pdfId: String
        });

        var idContenido = atributosPdf.idContenido;
        var contenido = Contenidos.findOne({_id: idContenido});

        var ordenPdf = contenido.pdfs.length;

        var ordenAudios = contenido.audios.length;

        var ordenVideos = contenido.videos.length;

        var ordenRecurso = ordenPdf+ordenAudios+ordenVideos;

        var pdfs = {
            pdfId: atributosPdf.pdfId,
            ordenPdf: ordenPdf+1,
            ordenRecurso: ordenRecurso+1
        }

        Contenidos.update(idContenido,  { $push: { pdfs: pdfs } }, function(error) {
            if (error)
                lanzarError(error.reason);
        });

        return {
            _id: idContenido
        };
    },

    editarRecursoPdf: function(atributosPdf) {
        check(Meteor.userId(), String);
        check(atributosPdf, {
            antiguoId: String,
            pdfId: String
        });

        var antiguoId = atributosPdf.antiguoId;
        var pdfId = atributosPdf.pdfId;

        Contenidos.update({"pdfs.pdfId": antiguoId},  { $set: { "pdfs.$.pdfId": pdfId } }, function(error) {
            if (error) {
                return lanzarError(error.reason);
            }
        });
    },

    editarRecursoAudio: function(atributosAudio) {
        check(Meteor.userId(), String);
        check(atributosAudio, {
            antiguoAudio: String,
            audioId: String
        });

        var antiguoId = atributosAudio.antiguoAudio;
        var audioId = atributosAudio.audioId;

        Contenidos.update({"audios.audioId": antiguoId},  { $set: { "audios.$.audioId": audioId } }, function(error) {
            if (error) {
                return lanzarError(error.reason);
            }
        });
    },

    /**
     * Deprecated
     * @param {*} atributosVideo 
     */
    editarRecursoVideo: function(atributosVideo) {
        check(Meteor.userId(), String);
        check(atributosVideo, {
            antiguoVideo: String,
            url: String
        });

        var antiguoId = atributosVideo.antiguoVideo;
        var url = atributosVideo.url;

        Contenidos.update({"videos.url": antiguoId},  { $set: { "videos.$.url": url } }, function(error) {
            if (error) {
                return lanzarError(error.reason);
            }
        });
    },

    eliminarRecursoPdf: function(atributosPdf) {
        check(Meteor.userId(), String);
        check(atributosPdf, {
            pdfId: String,
            idContenido: String
        });

        var pdfId = atributosPdf.pdfId;
        var idContenido = atributosPdf.idContenido;

        //obtenemos el tamaño del arreglo de pdfs
        var sizePdf = Contenidos.findOne({ _id: idContenido}).pdfs.length;

        //buscamos el orden del recurso que se está eliminando con el id del pdf
        //contenidos.findOne();

        var contenido = Contenidos.findOne({ "pdfs.pdfId": pdfId});

        var variable = 0
        var objetoEliminar;
        while(variable < sizePdf){
            var id = contenido.pdfs[variable].pdfId;
            if(pdfId==id) {
                objetoEliminar = contenido.pdfs[variable];
            }
            variable++;
        }

        var i = 0;
        while(i < sizePdf) {
            var orden = contenido.pdfs[i].ordenRecurso;
            var ordenPdf = contenido.pdfs[i].ordenPdf;
            var id = contenido.pdfs[i].pdfId;

            if(objetoEliminar.ordenRecurso<=orden) {

                //se le resta 1
                orden--;

                //se updatea en la tabla
                Contenidos.update({"pdfs.pdfId": id},  { $set: { "pdfs.$.ordenRecurso": orden } }, function(error) {
                    if (error) {
                        return lanzarError(error.reason);
                    }
                });

            }

            if(objetoEliminar.ordenPdf<=ordenPdf) {
                
                ordenPdf--;
                
                //se updatea en la tabla
                Contenidos.update({"pdfs.pdfId": id},  { $set: { "pdfs.$.ordenPdf": ordenPdf } }, function(error) {
                    if (error) {
                        return lanzarError(error.reason);
                    }
                });

            }  
            
            i++;
        }

        var sizeVideo = Contenidos.findOne({ _id: idContenido}).videos.length;
        var j = 0;
        while(j < sizeVideo) {
            var orden = contenido.videos[j].ordenRecurso;
            var id = contenido.videos[j].url;

            if(objetoEliminar.ordenRecurso<orden) {

                //se le resta 1
                orden--;

                //se updatea en la tabla
                Contenidos.update({"videos.url": id},  { $set: { "videos.$.ordenRecurso": orden } }, function(error) {
                    if (error) {
                        return lanzarError(error.reason);
                    }
                });

            }

            j++;
        }

        var sizeAudio = Contenidos.findOne({ _id: idContenido}).audios.length;
        var k = 0;
        while(k < sizeAudio) {
            var orden = contenido.audios[k].ordenRecurso;
            var id = contenido.audios[k].audioId;

            if(objetoEliminar.ordenRecurso<orden) {

                //se le resta 1
                orden--;

                //se updatea en la tabla
                Contenidos.update({"audios.audioId": id},  { $set: { "audios.$.ordenRecurso": orden } }, function(error) {
                    if (error) {
                        return lanzarError(error.reason);
                    }
                });

            }

            k++;
        }

        Contenidos.update({_id: idContenido},  { $pull: { pdfs: { pdfId: pdfId } } }, function(error) {
            if (error) {
                return lanzarError(error.reason);
            }
        });
    },

    eliminarRecursoAudio: function(atributosAudio) {
        check(Meteor.userId(), String);
        check(atributosAudio, {
            audioId: String,
            idContenido: String
        });

        var audioId = atributosAudio.audioId;
        var idContenido = atributosAudio.idContenido;

        //obtenemos el tamaño del arreglo de audios
        var sizeAudio = Contenidos.findOne({ _id: idContenido}).audios.length;

        //buscamos el orden del recurso que se está eliminando con el id del audio
        //contenidos.findOne();

        var contenido = Contenidos.findOne({ "audios.audioId": audioId});

        var variable = 0
        var objetoEliminar;
        while(variable < sizeAudio){
            var id = contenido.audios[variable].audioId;
            if(audioId==id) {
                objetoEliminar = contenido.audios[variable];
            }
            variable++;
        }

        var i = 0;
        while(i < sizeAudio) {
            var orden = contenido.audios[i].ordenRecurso;
            var ordenAudio = contenido.audios[i].ordenAudio;
            var id = contenido.audios[i].audioId;

            if(objetoEliminar.ordenRecurso<=orden) {

                //se le resta 1
                orden--;

                //se updatea en la tabla
                Contenidos.update({"audios.audioId": id},  { $set: { "audios.$.ordenRecurso": orden } }, function(error) {
                    if (error) {
                        return lanzarError(error.reason);
                    }
                });

            }

            if(objetoEliminar.ordenAudio<=ordenAudio) {
                
                ordenAudio--;
                
                //se updatea en la tabla
                Contenidos.update({"audios.audioId": id},  { $set: { "audios.$.ordenAudio": ordenAudio } }, function(error) {
                    if (error) {
                        return lanzarError(error.reason);
                    }
                });

            }  
            
            i++;
        }

        var sizeVideo = Contenidos.findOne({ _id: idContenido}).videos.length;
        var j = 0;
        while(j < sizeVideo) {
            var orden = contenido.videos[j].ordenRecurso;
            var id = contenido.videos[j].url;

            if(objetoEliminar.ordenRecurso<orden) {

                //se le resta 1
                orden--;

                //se updatea en la tabla
                Contenidos.update({"videos.url": id},  { $set: { "videos.$.ordenRecurso": orden } }, function(error) {
                    if (error) {
                        return lanzarError(error.reason);
                    }
                });

            }

            j++;
        }

        var sizePdf = Contenidos.findOne({ _id: idContenido}).pdfs.length;
        var k = 0;
        while(k < sizePdf) {
            var orden = contenido.pdfs[k].ordenRecurso;
            var id = contenido.pdfs[k].pdfId;

            if(objetoEliminar.ordenRecurso<orden) {

                //se le resta 1
                orden--;

                //se updatea en la tabla
                Contenidos.update({"pdfs.pdfId": id},  { $set: { "pdfs.$.ordenRecurso": orden } }, function(error) {
                    if (error) {
                        return lanzarError(error.reason);
                    }
                });

            }

            k++;
        }

        Contenidos.update({_id: idContenido},  { $pull: { audios: { audioId: audioId } } }, function(error) {
            if (error) {
                return lanzarError(error.reason);
            }
        });
    },

    /**
     * Método que elimina los recursos de video de los contenidos
     * @param {*} atributosVideo 
     */
    eliminarRecursoVideo: function(atributosVideo) {
        check(Meteor.userId(), String);
        check(atributosVideo, {
            url: String,
            idContenido: String,
            ordenRecurso: Number,
            ordenVideo: Number
        });

        var url = atributosVideo.url;
        var idContenido = atributosVideo.idContenido;

        // obtenemos la info del contenido mediante el ID
        var contenido = Contenidos.findOne({ _id: idContenido});

        // obtenemos el tamaño del arreglo de videos
        var sizeVideo = contenido.videos.length;

        // obtenemos la cantidad total de recursos del contenido
        var sizeTotal = contenido.audios.length + contenido.pdfs.length + sizeVideo;

        // obtenemos los datos del elemento a eliminar
        var recurso = atributosVideo.ordenRecurso;
        var orden = atributosVideo.ordenVideo;

        //preguntamos si hay mas recursos agregados despues del que se está agregando
        if(recurso<sizeTotal) {
            // Si es así, recorremos todos ellos y les restamos a sus recursos -1
            var arrow = recurso+1;
            while(arrow<=sizeTotal) {

                var matchAudio = Contenidos.findOne({ "audios.ordenRecurso": arrow });
                if(matchAudio) {

                    Contenidos.update({_id: idContenido, "audios.ordenRecurso": arrow},  { $set: { "audios.$.ordenRecurso": arrow-1 } }, function(error) {
                        if (error) {
                            return lanzarError(error.reason);
                        }
                    });

                } else {
                    var matchPdf = Contenidos.findOne({ "pdfs.ordenRecurso": arrow });
                    if(matchPdf) {

                        Contenidos.update({_id: idContenido, "pdfs.ordenRecurso": arrow},  { $set: { "pdfs.$.ordenRecurso": arrow-1 } }, function(error) {
                            if (error) {
                                return lanzarError(error.reason);
                            }
                        });

                    } else {
                        var matchVideo = Contenidos.findOne({"videos.ordenVideo": 1},{ "videos.ordenRecurso": arrow });
                        if(matchVideo) {

                            Contenidos.update({_id: idContenido, "videos.ordenRecurso": arrow},  { $set: { "videos.$.ordenRecurso": arrow-1 }}, function(error) {
                                if (error) {
                                    return lanzarError(error.reason);
                                }
                            });

                        }

                    }
                }

                arrow++;
            }
        }

        // preguntamos si hay mas videos agregados después del que se está eliminando
        if(orden<sizeVideo) {
            // Si es así, recorremos todos ellos y les restamos a sus recursos -1
            var arrow = orden+1;
            while(arrow<=sizeVideo) {

                var matchVideo = Contenidos.findOne({ "videos.ordenVideo": arrow });
                    if(matchVideo) {

                        Contenidos.update({_id: idContenido, "videos.ordenVideo": arrow},  { $set: { "videos.$.ordenVideo": arrow-1 } }, function(error) {
                            if (error) {
                                return lanzarError(error.reason);
                            }
                        });

                    }

                arrow++;
            }
        }

        // eliminamos el elemento
        Contenidos.update({_id: idContenido},  { $pull: { videos: { url: url } } }, function(error) {
            if (error) {
                return lanzarError(error.reason);
            }
        });
    },

    /**
     * Método que sube un elemento en los contenidos
     * @param {*} atributosRecurso 
     */
    subirRecurso: function(atributosRecurso) {
        check(Meteor.userId(), String);
        check(atributosRecurso, {
            ordenRecurso: Number,
            idRecurso: String,
            tipo: String,
            idContenido: String
        });

        var ordenRecurso = atributosRecurso.ordenRecurso;
        var tipo = atributosRecurso.tipo;
        var idRecurso = atributosRecurso.idRecurso;
        var idContenido = atributosRecurso.idContenido;

        var anteriorRecurso = ordenRecurso-1;

        if(tipo=="videos") {
            // preguntamos que tipo tiene el elemento anterior
            var matchAudio = Contenidos.findOne({ "audios.ordenRecurso": anteriorRecurso });
            if(matchAudio) {

                Contenidos.update({_id: idContenido, "audios.ordenRecurso": anteriorRecurso},  { $set: { "audios.$.ordenRecurso": ordenRecurso } }, function(error) {
                    if (error) {
                        return lanzarError(error.reason);
                    }
                });

            } else {
                var matchPdf = Contenidos.findOne({ "pdfs.ordenRecurso": anteriorRecurso });
                if(matchPdf) {

                    Contenidos.update({_id: idContenido, "pdfs.ordenRecurso": anteriorRecurso },  { $set: { "pdfs.$.ordenRecurso": ordenRecurso } }, function(error) {
                        if (error) {
                            return lanzarError(error.reason);
                        }
                    });

                } else {
                    var matchVideo = Contenidos.findOne({ "videos.ordenRecurso": anteriorRecurso });
                    if(matchVideo) {

                        Contenidos.update({_id: idContenido, "videos.ordenRecurso": anteriorRecurso },  { $set: { "videos.$.ordenRecurso": ordenRecurso }}, function(error) {
                            if (error) {
                                return lanzarError(error.reason);
                            }
                        });

                    }

                }
            }
            // tomamos el elemento clickeado y le restamos 1 unidad al ordenRecurso
            Contenidos.update({_id: idContenido, "videos.ordenRecurso": ordenRecurso, "videos.url": idRecurso },  { $set: { "videos.$.ordenRecurso": anteriorRecurso } }, function(error) {
                if (error) {
                    return lanzarError(error.reason);
                }
            });
        }

        if(tipo=="audios") {
            // preguntamos que tipo tiene el elemento anterior
            var matchAudio = Contenidos.findOne({ "audios.ordenRecurso": anteriorRecurso });
            if(matchAudio) {

                Contenidos.update({_id: idContenido, "audios.ordenRecurso": anteriorRecurso},  { $set: { "audios.$.ordenRecurso": ordenRecurso } }, function(error) {
                    if (error) {
                        return lanzarError(error.reason);
                    }
                });

            } else {
                var matchPdf = Contenidos.findOne({ "pdfs.ordenRecurso": anteriorRecurso });
                if(matchPdf) {

                    Contenidos.update({_id: idContenido, "pdfs.ordenRecurso": anteriorRecurso },  { $set: { "pdfs.$.ordenRecurso": ordenRecurso } }, function(error) {
                        if (error) {
                            return lanzarError(error.reason);
                        }
                    });

                } else {
                    var matchVideo = Contenidos.findOne({ "videos.ordenRecurso": anteriorRecurso });
                    if(matchVideo) {

                        Contenidos.update({_id: idContenido, "videos.ordenRecurso": anteriorRecurso },  { $set: { "videos.$.ordenRecurso": ordenRecurso }}, function(error) {
                            if (error) {
                                return lanzarError(error.reason);
                            }
                        });

                    }

                }
            }
            // tomamos el elemento clickeado y le restamos 1 unidad al ordenRecurso
            Contenidos.update({_id: idContenido, "audios.ordenRecurso": ordenRecurso, "audios.audioId": idRecurso },  { $set: { "audios.$.ordenRecurso": anteriorRecurso } }, function(error) {
                if (error) {
                    return lanzarError(error.reason);
                }
            });
        }

        if(tipo=="pdfs") {
            // preguntamos que tipo tiene el elemento anterior
            var matchAudio = Contenidos.findOne({ "audios.ordenRecurso": anteriorRecurso });
            if(matchAudio) {

                Contenidos.update({_id: idContenido, "audios.ordenRecurso": anteriorRecurso},  { $set: { "audios.$.ordenRecurso": ordenRecurso } }, function(error) {
                    if (error) {
                        return lanzarError(error.reason);
                    }
                });

            } else {
                var matchPdf = Contenidos.findOne({ "pdfs.ordenRecurso": anteriorRecurso });
                if(matchPdf) {

                    Contenidos.update({_id: idContenido, "pdfs.ordenRecurso": anteriorRecurso },  { $set: { "pdfs.$.ordenRecurso": ordenRecurso } }, function(error) {
                        if (error) {
                            return lanzarError(error.reason);
                        }
                    });

                } else {
                    var matchVideo = Contenidos.findOne({ "videos.ordenRecurso": anteriorRecurso });
                    if(matchVideo) {

                        Contenidos.update({_id: idContenido, "videos.ordenRecurso": anteriorRecurso },  { $set: { "videos.$.ordenRecurso": ordenRecurso }}, function(error) {
                            if (error) {
                                return lanzarError(error.reason);
                            }
                        });
                    }
                }
            }
            // tomamos el elemento clickeado y le restamos 1 unidad al ordenRecurso
            Contenidos.update({_id: idContenido, "pdfs.ordenRecurso": ordenRecurso, "pdfs.pdfId": idRecurso },  { $set: { "pdfs.$.ordenRecurso": anteriorRecurso } }, function(error) {
                if (error) {
                    return lanzarError(error.reason);
                }
            });
        }
    },

    /**
     * Método que baja un elemento en los contenidos
     * @param {*} atributosRecurso 
     */
    bajarRecurso: function(atributosRecurso) {
        check(Meteor.userId(), String);
        check(atributosRecurso, {
            ordenRecurso: Number,
            idRecurso: String,
            tipo: String,
            idContenido: String
        });

        var ordenRecurso = atributosRecurso.ordenRecurso;
        var tipo = atributosRecurso.tipo;
        var idRecurso = atributosRecurso.idRecurso;
        var idContenido = atributosRecurso.idContenido;

        var siguienteRecurso = ordenRecurso+1;

        if(tipo=="videos") {
            // preguntamos que tipo tiene el elemento siguiente
            var matchAudio = Contenidos.findOne({ "audios.ordenRecurso": siguienteRecurso });
            if(matchAudio) {

                Contenidos.update({_id: idContenido, "audios.ordenRecurso": siguienteRecurso},  { $set: { "audios.$.ordenRecurso": ordenRecurso } }, function(error) {
                    if (error) {
                        return lanzarError(error.reason);
                    }
                });

            } else {
                var matchPdf = Contenidos.findOne({ "pdfs.ordenRecurso": siguienteRecurso });
                if(matchPdf) {

                    Contenidos.update({_id: idContenido, "pdfs.ordenRecurso": siguienteRecurso },  { $set: { "pdfs.$.ordenRecurso": ordenRecurso } }, function(error) {
                        if (error) {
                            return lanzarError(error.reason);
                        }
                    });

                } else {
                    var matchVideo = Contenidos.findOne({ "videos.ordenRecurso": siguienteRecurso });
                    if(matchVideo) {

                        Contenidos.update({_id: idContenido, "videos.ordenRecurso": siguienteRecurso },  { $set: { "videos.$.ordenRecurso": ordenRecurso }}, function(error) {
                            if (error) {
                                return lanzarError(error.reason);
                            }
                        });

                    }

                }
            }
            // tomamos el elemento clickeado y le sumamos 1 unidad al ordenRecurso
            Contenidos.update({_id: idContenido, "videos.ordenRecurso": ordenRecurso, "videos.url": idRecurso },  { $set: { "videos.$.ordenRecurso": siguienteRecurso } }, function(error) {
                if (error) {
                    return lanzarError(error.reason);
                }
            });
        }

        if(tipo=="audios") {
            // preguntamos que tipo tiene el elemento siguiente
            var matchAudio = Contenidos.findOne({ "audios.ordenRecurso": siguienteRecurso });
            if(matchAudio) {

                Contenidos.update({_id: idContenido, "audios.ordenRecurso": siguienteRecurso},  { $set: { "audios.$.ordenRecurso": ordenRecurso } }, function(error) {
                    if (error) {
                        return lanzarError(error.reason);
                    }
                });

            } else {
                var matchPdf = Contenidos.findOne({ "pdfs.ordenRecurso": siguienteRecurso });
                if(matchPdf) {

                    Contenidos.update({_id: idContenido, "pdfs.ordenRecurso": siguienteRecurso },  { $set: { "pdfs.$.ordenRecurso": ordenRecurso } }, function(error) {
                        if (error) {
                            return lanzarError(error.reason);
                        }
                    });

                } else {
                    var matchVideo = Contenidos.findOne({ "videos.ordenRecurso": siguienteRecurso });
                    if(matchVideo) {

                        Contenidos.update({_id: idContenido, "videos.ordenRecurso": siguienteRecurso },  { $set: { "videos.$.ordenRecurso": ordenRecurso }}, function(error) {
                            if (error) {
                                return lanzarError(error.reason);
                            }
                        });

                    }

                }
            }
            // tomamos el elemento clickeado y le sumamos 1 unidad al ordenRecurso
            Contenidos.update({_id: idContenido, "audios.ordenRecurso": ordenRecurso, "audios.audioId": idRecurso },  { $set: { "audios.$.ordenRecurso": siguienteRecurso } }, function(error) {
                if (error) {
                    return lanzarError(error.reason);
                }
            });
        }

        if(tipo=="pdfs") {
            // preguntamos que tipo tiene el elemento siguiente
            var matchAudio = Contenidos.findOne({ "audios.ordenRecurso": siguienteRecurso });
            if(matchAudio) {

                Contenidos.update({_id: idContenido, "audios.ordenRecurso": siguienteRecurso},  { $set: { "audios.$.ordenRecurso": ordenRecurso } }, function(error) {
                    if (error) {
                        return lanzarError(error.reason);
                    }
                });

            } else {
                var matchPdf = Contenidos.findOne({ "pdfs.ordenRecurso": siguienteRecurso });
                if(matchPdf) {

                    Contenidos.update({_id: idContenido, "pdfs.ordenRecurso": siguienteRecurso },  { $set: { "pdfs.$.ordenRecurso": ordenRecurso } }, function(error) {
                        if (error) {
                            return lanzarError(error.reason);
                        }
                    });

                } else {
                    var matchVideo = Contenidos.findOne({ "videos.ordenRecurso": siguienteRecurso });
                    if(matchVideo) {

                        Contenidos.update({_id: idContenido, "videos.ordenRecurso": siguienteRecurso },  { $set: { "videos.$.ordenRecurso": ordenRecurso }}, function(error) {
                            if (error) {
                                return lanzarError(error.reason);
                            }
                        });
                    }
                }
            }
            // tomamos el elemento clickeado y le sumamos 1 unidad al ordenRecurso
            Contenidos.update({_id: idContenido, "pdfs.ordenRecurso": ordenRecurso, "pdfs.pdfId": idRecurso },  { $set: { "pdfs.$.ordenRecurso": siguienteRecurso } }, function(error) {
                if (error) {
                    return lanzarError(error.reason);
                }
            });
        }
    }
});

Contenidos.allow({
    update: function(userId, modulo) {
        return propioDocumento(userId, modulo) ; 
    },
    
    remove: function(userId, modulo) {
        return propioDocumento(userId, modulo) ;
    }
});