Timers = new Mongo.Collection('timers');

Meteor.methods({
    iniciarTimer: function(atributosCurso) {
        check(Meteor.userId(), String);
        check(atributosCurso, {
            cursoId: String
        });

        var timerYaIniciado = Timers.findOne( { $and: [ {cursoId: atributosCurso.cursoId}, {userId: Meteor.userId()}] });

        if(timerYaIniciado) {
            return {
                _id: timerYaIniciado._id
            }
        }
        var usuario = Meteor.user();
        var timer = _.extend(atributosCurso, {
            userId: usuario._id,
            fechaInicio: new Date()
        });

        var timerId = Timers.insert(timer);
        
        //creamos la notificacion para el administrador en la coleccion Notificaciones del inicio del curso
        var curso = Cursos.findOne({_id: atributosCurso.cursoId});
        crearNotificacionInicio(curso);

        return {
            _id: timerId
        };
    }
});

Timers.allow({ 
    insert: function() { 
        return true; 
    }, 
    update: function() { 
        return true; 
    }, 
    remove: function() { 
        return true; 
    } 
});
