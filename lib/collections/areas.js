Areas = new Mongo.Collection('areas');

validarArea = function (area) {
    var errores = {};
    if (!area.nombreArea)
        errores.nombreArea = "Por favor ingresa el nombre de la nueva categoría";
    return errores;
}

Meteor.methods({
    insertarArea: function(atributosArea) {
        check(Meteor.userId(), String);
        check(atributosArea, {
            nombreArea: String 
        });
        
        var areaConMismoNombre = Areas.findOne({nombreArea: atributosArea.nombreArea});
        
        if (areaConMismoNombre) {
            return {
                existeArea: true,
                _id: areaConMismoNombre._id
            }
        }
        
        var usuario = Meteor.user();
        var area = _.extend(atributosArea, {
            userId: usuario._id,
            autorArea: usuario.username,
            fechaArea: new Date()
        });
        
        var areaId = Areas.insert(area);
        return {
            _id: areaId
        };
    }
});

Areas.allow({
    update: function(userId, area) {
        return propioDocumento(userId, area) ; 
    },
    
    remove: function(userId, area) {
        return propioDocumento(userId, area) ;
    }
});

Areas.allow({
    //permiso para que los administradores puedan updatear y eliminar
});

Areas.deny({
    update: function(userId, area, nombreCampos) {
        //solo se pueden editar el siguiente campo
        return (_.without(nombreCampos, 'nombreArea').length > 0);
    }
});