Metas = new Mongo.Collection('metas');

validarMeta = function (meta) {
    var errores = {};
    if(!meta.nombreMeta)
        errores.nombreMeta = "Por favor ingresa un nombre";
    if(!meta.descripcion)
        errores.descripcion = "Por favor ingresa una descripción";
    if(!meta.dificultad)
        errores.dificultad = "Por favor elige una dificultad";
    return errores;
}

Meteor.methods({
    insertarMeta: function(atributosMeta) {
        check(this.userId, String);
        check(atributosMeta, {
            nombreMeta: String,
            descripcion: String,
            dificultad: String,
            cursosMeta: Array
        });
        
        var metaConMismoNombre = Metas.findOne({nombreMeta: atributosMeta.nombreMeta});
        
        if (metaConMismoNombre) {
            return {
                existeMeta: true,
                _id: metaConMismoNombre._id
            }
        }
        
        var usuario = Meteor.user();
        var meta = _.extend(atributosMeta, {
            userId: usuario._id,
            autorMeta: usuario.username,
            fechaMeta: new Date()
        });
        
        var metaId = Metas.insert(meta);
        return {
            _id: metaId
        };
    }
});

Metas.allow({
    /*
    insert: function(userId, doc){
        //solo permite insertar cursos si estas logueado
        return !! userId;
    },
    
    update: function(userId, doc){
        //solo permite insertar cursos si estas logueado
        return !! userId;
    },
    
    remove: function(userId, doc){
        //solo permite insertar cursos si estas logueado
        return !! userId;
    }
    */
    
    update: function(userId, meta) {
        return propioDocumento(userId, meta) ; 
    },
    
    remove: function(userId, meta) {
        return propioDocumento(userId, meta) ;
    }
});