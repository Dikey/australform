Configuracion = new Mongo.Collection('configuracion');

Meteor.methods({
    updateConfig: function(atributosConfig) {
        check(Meteor.userId(), String);
        check(atributosConfig, {
            datos: String,
            descripcion: String,
            objetivo: String,
            curso: String,
            modulo: String,
            contenido: String,
            categoria: String,
            usuario: String,
            profesor: String
        });

        var config = Configuracion.findOne({});

        Configuracion.update(config._id, { $set: {
            datos: atributosConfig.datos,
            descripcion: atributosConfig.descripcion,
            objetivo: atributosConfig.objetivo,
            curso: atributosConfig.curso,
            modulo: atributosConfig.modulo,
            contenido: atributosConfig.contenido,
            categoria: atributosConfig.categoria,
            usuario: atributosConfig.usuario,
            profesor: atributosConfig.profesor
        }});

        //envia notificación y email al alumno

        return {
            _id: config._id
        };
    }
});

Configuracion.allow({
    insert: function(userId, modulo) {
        return true; 
    },

    update: function(userId, modulo) {
        return true ; 
    },
    
    remove: function(userId, modulo) {
        return true ;
    }
});