Comentarios = new Mongo.Collection('comentarios');

Meteor.methods({
    insertarComentario: function(atributosComentario) {
        check(this.userId, String);
        check(atributosComentario, {
            texto: String,
            contenidoId: String
        });
        
        var usuario = Meteor.user();
        var contenido = Contenidos.findOne(atributosComentario.contenidoId);
        if (!contenido)
            throw new Meteor.Error('invalid-comment', 'Debes comentar en un contenido');
        
        var comentario = _.extend(atributosComentario, {
            userId: usuario._id,
            autorComentario: usuario.username,
            emailComentario: usuario.emails[0].address,
            fechaComentario: new Date()
        });
        
        //crea el comentario, guarda el id
        comentario._id = Comentarios.insert(comentario);
        
        //ahora crea una notificacion, informando al profesor que ha habido un comentario
        crearNotificacionComentario(comentario);

        //buscamos el email del profesor en el curso
        var cursoId = Modulos.findOne({_id: contenido.moduloId}).cursoId;
        var emailAutor = Cursos.findOne({_id: cursoId}).emailAutor;

        //enviamos un correo con el comentario al email del profesor
        /*
        Meteor.call('sendEmail',
            emailAutor,
            'Comentario en FormAustral',
            'El usuario '+ usuario.username + ' ha comentado en tu contenido ' + contenido.tituloContenido +
            ' lo siguiente: ' + comentario);
        */

        return comentario._id;
    }
});

Comentarios.allow({
    insert: function(userId, doc){
        //solo permite insertar cursos si estas logueado
        return !! userId;
    },
    
    update: function(userId, doc){
        //solo permite insertar cursos si estas logueado
        return !! userId;
    },
    
    remove: function(userId, doc){
        //solo permite insertar cursos si estas logueado
        return !! userId;
    }
    /*
    
    update: function(userId, comentario) {
        return propioDocumento(userId, comentario) ; 
    },
    
    remove: function(userId, comentario) {
        return propioDocumento(userId, comentario) ;
    }
 */
});
