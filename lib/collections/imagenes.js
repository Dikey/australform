
Imagenes = new FS.Collection("imagenes", {
    //stores: [imageStore]
    stores: [new FS.Store.FileSystem("imagenes", {path: '/uploads'})]
  });
  
Imagenes.allow({
  insert:function(userId,project) {
    // add custom authentication code here
    return true;
  },

  update:function(userId,project,fields,modifier) {
    // add custom authentication code here
    return true;
  },

  remove:function(userId,project) {
    // add custom authentication code here
    return true;
  },
  download:function(){
    return true;
  }
});