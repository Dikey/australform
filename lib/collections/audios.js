Audios = new FS.Collection("audios", {
    //stores: [imageStore]
    stores: [new FS.Store.FileSystem("audios", {path: '/uploads'})]
});

Audios.allow({
    insert:function(userId,project) {
      // add custom authentication code here
      return true;
    },
  
    update:function(userId,project,fields,modifier) {
      // add custom authentication code here
      return true;
    },
  
    remove:function(userId,project) {
      // add custom authentication code here
      return true;
    },
    
    download:function(){
      return true;
    }
});