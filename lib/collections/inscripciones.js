Inscripciones = new Mongo.Collection('inscripciones');

//funcion que valida los campos de rut y curso dentro del formulario de creación de inscripciones
validarInscripcion = function (inscripcion) {
    var errores = {};
    if (!inscripcion.cursoId)
        errores.cursoId = "Por favor selecciona un curso";
    if (!inscripcion.usuarioId)
        errores.usuarioId = "Por favor introduce un correo electrónico";
    return errores;
}

Meteor.methods({
    //método que inserta un usuario a un curso en la tabla inscripciones
    insertarInscripcion: function(atributosInscripcion) {
        check(Meteor.userId(), String);
        check(atributosInscripcion, {
            cursoId: String,
            usuarioId: String,
            username: String,
            rut: String,
            cargo: String
        });

        var usuarioYaIngresado = Inscripciones.findOne({cursoId: atributosInscripcion.cursoId, usuarioId: atributosInscripcion.usuarioId});

        if (usuarioYaIngresado) {
            return {
                existeInscripcion: true,
                _id: usuarioYaIngresado._id
            }
        }

        var usuario = Meteor.user();
        var inscripcion = _.extend(atributosInscripcion, {
            inscriptor: usuario.username,
            fechaInscripcion: new Date()
        });

        var inscripcionId = Inscripciones.insert(inscripcion);

        return {
            _id: inscripcionId
        };
    }
});

Inscripciones.allow({
    insert: function(userId, doc){
        //solo permite insertar Desarrollos si estas logueado
        return !! userId;
    },
    
    update: function(userId, doc){
        //solo permite insertar Desarrollos si estas logueado
        return !! userId;
    }
});


