Modulos = new Mongo.Collection('modulos');

//funcion que valida los campos de nombre y curso dentro del formulario de creación de modulos
validarModulo = function (modulo) {
    var errores = {};
    if (!modulo.nombreModulo)
        errores.nombreModulo = "Por favor introduce un nombre";
    if (!modulo.cursoId)
        errores.cursoId = "Por favor selecciona un curso";
    if (!modulo.introduccionModulo)
        errores.introduccionModulo = "Por favor ingresa una introduccion para el módulo";
    return errores;
}

Meteor.methods({
    //método que te permite insertar Módulos
    insertarModulo: function(atributosModulo) {
        check(Meteor.userId(), String);
        check(atributosModulo, {
            nombreModulo: String,
            cursoId: String,
            introduccionModulo: String,
            requisitoModuloId: String
        });
        
        var moduloConMismoNombre = Modulos.findOne({nombreModulo: atributosModulo.nombreModulo});
        
        if (moduloConMismoNombre) {
            return {
                existeModulo: true,
                _id: moduloConMismoNombre._id
            }
        }
        
        var cantidadModulos = Modulos.find({cursoId: atributosModulo.cursoId}).count();
        
        var usuario = Meteor.user();
        var modulo = _.extend(atributosModulo, {
            userId: usuario._id,
            autorModulo: usuario.username,
            fechaModulo: new Date(),
            orden: cantidadModulos+1
        });
        
        var moduloId = Modulos.insert(modulo);
        return {
            _id: moduloId
        };
    }

    /*
    definirPruebaModulo: function(atributosModulo) {


        Modulos.update(idModulo,  {$set: {prueba: prueba} }, function(error) {
            if (error) {
                lanzarError(error.reason);
            } else {
                Router.go('paginaModulo', {_id: idModulo});
            }
        });
    }
    */
});

Modulos.allow({
    /*
    insert: function(userId, doc){
        //solo permite insertar cursos si estas logueado
        return !! userId;
    },
    
    update: function(userId, doc){
        //solo permite insertar cursos si estas logueado
        return !! userId;
    },
    
    remove: function(userId, doc){
        //solo permite insertar cursos si estas logueado
        return !! userId;
    },
    */
    
    update: function(userId, modulo) {
        return propioDocumento(userId, modulo) ; 
    },
    
    remove: function(userId, modulo) {
        return propioDocumento(userId, modulo) ;
    }
}); 