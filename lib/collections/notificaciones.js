Notificaciones = new Mongo.Collection('notificaciones');

Meteor.methods({
    sendEmail: function (to, subject, text) {
        check(this.userId, String);
        check(to, String);
        check(subject, String);
        check(text, String);

        // Let other method calls from the same client start running,
        // without waiting for the email sending to complete.
        this.unblock();
    
        // don’t allow sending email unless the user is logged in
        if (!Meteor.user())
            throw new Meteor.Error(403, "not logged in");
    
        // and here is where you can throttle the number of emails this user
        // is allowed to send per day
    
        Meteor.Mailgun.send({
            to: to,
            from: Meteor.user().emails[0].address,
            subject: subject,
            text: text
        });

        console.log("email enviado!");
    }
});

Notificaciones.allow({
    insert: function(userId, project){
        //solo permite insertar Notificaciones si estas logueado
        return true;
    },
    update: function(userId, doc, nombreCampos) {
        return propioDocumento(userId, doc) &&
            nombreCampos.length === 1 && nombreCampos[0] === 'leido';
    }
});

crearNotificacionComentario = function(comentario) {
    var contenido = Contenidos.findOne(comentario.contenidoId);
    if (comentario.userId !== contenido.userId) {
        Notificaciones.insert({
            userId: contenido.userId,
            contenidoId: contenido._id,
            comentarioId: comentario._id,
            nombreComentador: comentario.autorComentario,
            tipo: "comentario",
            fecha: new Date(),
            leido: false
        });
    }
};

crearNotificacionCurso = function(curso) {
    var areaCurso = Areas.findOne(curso.areaId);
    var adminId = areaCurso.userId;
    
    Notificaciones.insert({
        userId: adminId,
        cursoId: curso._id,
        nombreCurso: curso.nombreCurso,
        nombrePublicador: curso.autorCurso,
        tipo: "curso creado",
        fecha: new Date(),
        leido: false
    });
};

crearNotificacionPublicado = function(curso) {
    var areaCurso = Areas.findOne(curso.areaId);
    var admin = areaCurso.autorArea;
    
    Notificaciones.insert({
        userId: curso.userId,
        cursoId: curso._id,
        nombreCurso: curso.nombreCurso,
        nombreQuienAcepto: admin,
        tipo: "curso aceptado",
        fecha: new Date(),
        leido: false
    });
};

crearNotificacionInicio = function(curso) {
    var areaCurso = Areas.findOne(curso.areaId);
    var adminId = areaCurso.userId;

    Notificaciones.insert({
        userId: adminId,
        cursoId: curso._id,
        nombreCurso: curso.nombreCurso,
        nombreUsuario: Meteor.user().profile.fullname,
        usuarioId: Meteor.user()._id,
        tipo: "curso iniciado",
        fecha: new Date(),
        leido: false
    });
};

crearNotificacionTermino = function(curso) {
    var areaCurso = Areas.findOne(curso.areaId);
    var adminId = areaCurso.userId;

    Notificaciones.insert({
        userId: adminId,
        cursoId: curso._id,
        nombreCurso: curso.nombreCurso,
        nombreUsuario: Meteor.user().profile.fullname,
        usuarioId: Meteor.user()._id,
        tipo: "curso terminado",
        fecha: new Date(),
        leido: false
    });
};

crearNotificacionTrabajo = function(curso) {
    var areaCurso = Areas.findOne(curso.areaId);
    var adminId = areaCurso.userId;

    Notificaciones.insert({
        userId: adminId,
        cursoId: curso._id,
        nombreCurso: curso.nombreCurso,
        nombreUsuario: Meteor.user().profile.fullname,
        usuarioId: Meteor.user()._id,
        tipo: "entrega trabajo",
        fecha: new Date(),
        leido: false
    });
};







