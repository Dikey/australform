Desarrollos = new Mongo.Collection('desarrollos');

Desarrollos.allow({
    insert: function(userId, doc){
        //solo permite insertar Desarrollos si estas logueado
        return true;
    },
    
    update: function(userId, doc){
        //solo permite insertar Desarrollos si estas logueado
        return true;
    }
});

Meteor.methods({
    realizarEncuesta: function(atributosDesarrollo) {
        check(Meteor.userId(), String);
        check(atributosDesarrollo, {
            cursoId: String,
            tipo: String
        });

        var encuestaRealizada = Desarrollos.findOne( { $and: [ {cursoId: atributosDesarrollo.cursoId}, {userId: Meteor.userId()}, {tipo: "Encuesta"}] });

        if(encuestaRealizada) {
            return {
                _id: encuestaRealizada._id
            }
        }

        var usuario = Meteor.user();
        var desarrollo = _.extend(atributosDesarrollo, {
            userId: usuario._id,
            fechaDesarrollo: new Date()
        });

        var desarrolloId = Desarrollos.insert(desarrollo);
        
        //creamos la notificacion para el administrador en la coleccion Notificaciones del termino del curso
        var curso = Cursos.findOne({_id: atributosDesarrollo.cursoId});
        crearNotificacionTermino(curso);


        return {
            _id: desarrolloId
        };
    },

    //método que inserta la aprobación de los contenidos
    realizarContenido: function(atributosDesarrollo) {
        check(Meteor.userId(), String);
        check(atributosDesarrollo, {
            contenidoId: String
        });

        var yaDesarrollado = Desarrollos.findOne( { $and: [ {contenidoId: atributosDesarrollo.contenidoId}, {userId: Meteor.userId()}] });
    
        if(yaDesarrollado) {
            return {
                _id: yaDesarrollado._id
            }
        }

        var usuario = Meteor.user();
        var desarrollo = _.extend(atributosDesarrollo, {
            userId: usuario._id,
            fechaDesarrollo: new Date(),
            aprobado: true
        });

        var desarrolloId = Desarrollos.insert(desarrollo);

        //----

        var contenido = Contenidos.findOne({_id: atributosDesarrollo.contenidoId});
        var cursoId = Modulos.findOne({_id: contenido.moduloId}).cursoId;        
        var timer = Timers.findOne({ $and: [{cursoId: cursoId}, {userId: Meteor.userId()}] });

        //actualizar la tabla timer del usuario y sumarle la duración de este contenido
        var duracion;
        if(!timer.duracionTimer) {
            duracion = contenido.duracionContenido;
        } else {
            var timeContenido = contenido.duracionContenido;
            duracion = Number(timer.duracionTimer) + Number(timeContenido);
        }

        Timers.update(timer._id, {$set: {duracionTimer: duracion} }, function(error) {
            if (error)
            return lanzarError(error.reason);
        });

        //---

        return {
            _id: desarrolloId
        };
    },

    //método que inserta los resultados de la prueba del módulo
    realizarPruebaModulo: function(atributosDesarrollo) {
        check(Meteor.userId(), String);
        check(atributosDesarrollo, {
            moduloId: String,
            preguntasCorrectas: Number,
            totalPreguntas: String
        });

        //buscar la nota de aprobación del curso y comparar con la nota de la prueba
        var porcentajeObtenido = (atributosDesarrollo.preguntasCorrectas*100)/atributosDesarrollo.totalPreguntas;
        var notaObtenida = (porcentajeObtenido*7)/100;

        var modulo = Modulos.findOne({_id: atributosDesarrollo.moduloId});
        var curso = Cursos.findOne({_id: modulo.cursoId});
        var notaAprobacion = curso.notaAprobacion;

        if(notaObtenida>=notaAprobacion){
            var aprobado = true;
        } else {
            var aprobado = false;
        }

        var yaDesarrollado = Desarrollos.findOne( { $and: [ {moduloId: atributosDesarrollo.moduloId}, {userId: Meteor.userId()}] });

        if(yaDesarrollado) {
            if(notaObtenida>yaDesarrollado.notaObtenida) {

                if(!aprobado){
                    aprobado = true;
                }

                var desarrolloId = Desarrollos.update(yaDesarrollado._id, { $set: {
                    preguntasCorrectas: atributosDesarrollo.preguntasCorrectas, 
                    porcentajeObtenido: porcentajeObtenido,
                    notaObtenida: notaObtenida,
                    fechaDesarrollo: new Date(),
                    aprobado: aprobado
                }});
                return {
                    _id: yaDesarrollado._id
                };
            }

            return {
                _id: yaDesarrollado._id
            };
        } 

        var usuario = Meteor.user();
        var desarrollo = _.extend(atributosDesarrollo, {
            porcentajeObtenido: porcentajeObtenido,
            notaObtenida: notaObtenida,
            userId: usuario._id,
            fechaDesarrollo: new Date(),
            aprobado: aprobado
        });

        var desarrolloId = Desarrollos.insert(desarrollo);
        return {
            _id: desarrolloId
        };
    },

    //método que inserta los resultados de la prueba del curso
    realizarPruebaCurso: function(atributosDesarrollo) {
        check(Meteor.userId(), String);
        check(atributosDesarrollo, {
            cursoId: String,
            preguntasCorrectas: Number,
            totalPreguntas: String,
            totalPreguntasDesarrollo: String,
            enEspera: Boolean,
            respuestasDesarrollo: Array
        });

        //buscar la nota de aprobación del curso y comparar con la nota de la prueba
        var porcentajeObtenido = (atributosDesarrollo.preguntasCorrectas*100)/atributosDesarrollo.totalPreguntas;
        var notaObtenida = (porcentajeObtenido*7)/100;

        // Nuevo código

        var curso = Cursos.findOne({_id: atributosDesarrollo.cursoId});
        var notaAprobacion = curso.notaAprobacion;

        var enEspera = atributosDesarrollo.enEspera;
        if(!enEspera) {

            var aprobado = false;

            //buscamos si el curso posee un trabajo final
            if(curso.trabajoFinal) {
                
                var desarrolloTrabajo = Desarrollos.findOne({ $and: [{cursoId: curso._id}, {userId: atributosDesarrollo.userId}, {tipo: "trabajo"}] });
                
                if(desarrolloTrabajo) {
                    
                    var notaTrabajo = Number(desarrolloTrabajo.notaObtenida);

                    var ponderacionTrabajo = Number(curso.trabajoFinal.ponderacionPruebaFinal);
                    var ponderacionPrueba = Number(100-ponderacionTrabajo);

                    var pruebaPonderada = (notaPrueba*ponderacionPrueba)/100;
                    var trabajoPonderado = (notaTrabajo*ponderacionTrabajo)/100;

                    notaObtenida = Math.round((pruebaPonderada+trabajoPonderado)*100)/100;
                    
                }
            }
            
            if(notaObtenida>=notaAprobacion)
                aprobado = true;

            var usuario = Meteor.user();
            var desarrollo = _.extend(atributosDesarrollo, {
                porcentajeObtenido: porcentajeObtenido,
                notaObtenida: notaObtenida,
                userId: usuario._id,
                username: usuario.profile.fullname,
                fechaDesarrollo: new Date(),
                aprobado: aprobado,
                tipo: "prueba"
            });

        // ------ Fin ------
        } else {
            var usuario = Meteor.user();
            var desarrollo = _.extend(atributosDesarrollo, {
                porcentajeObtenido: porcentajeObtenido,
                notaObtenida: notaObtenida,
                userId: usuario._id,
                username: usuario.profile.fullname,
                fechaDesarrollo: new Date(),
                tipo: "prueba"
            });
        }

        //envia notificación y email al profesor

        var desarrolloId = Desarrollos.insert(desarrollo);
        return {
            _id: desarrolloId
        };

    },

    realizarTrabajoFinal: function(atributosDesarrollo) {
        check(Meteor.userId(), String);
        check(atributosDesarrollo, {
            cursoId: String,
            trabajoFinalId: String,
            enEspera: Boolean,
        });

        var usuario = Meteor.user();
        var desarrollo = _.extend(atributosDesarrollo, {
            userId: usuario._id,
            username: usuario.profile.fullname,
            fechaDesarrollo: new Date(),
            tipo: "trabajo"
        });

        var curso = Cursos.findOne({_id: atributosDesarrollo.cursoId});

        //envia notificación y email al profesor
        crearNotificacionTrabajo(curso);

        var desarrolloId = Desarrollos.insert(desarrollo);
        return {
            _id: desarrolloId
        };

    },

    /*
        Editar Método:
            1.- Agregar los calculos a las preguntas de desarrollo.
    */
    realizarEvaluacionPrueba: function(atributosDesarrollo) {
        check(Meteor.userId(), String);
        check(atributosDesarrollo, {
            desarrolloId: String,
            cursoId: String,
            userId: String,
            preguntasCorrectas: Number,
            porcentajeObtenido: Number,
            notaObtenida: Number,
            totalPreguntasSeleccion: String,
            puntosDesarrolloObtenidos: Number,
            totalPreguntasDesarrollo: Number
        });

        /*
            Aquí van los calculos de las preguntas de desarrollo
        */

        var puntajeTotal = Number(atributosDesarrollo.totalPreguntasSeleccion)+(atributosDesarrollo.totalPreguntasDesarrollo*2);
        var puntajeObtenido = atributosDesarrollo.puntosDesarrolloObtenidos+atributosDesarrollo.preguntasCorrectas;

        var notaPrueba = Math.round(((puntajeObtenido*7)/puntajeTotal)*100)/100;

        var notaObtenida = 0;
        var aprobado = false;

        var curso = Cursos.findOne({_id: atributosDesarrollo.cursoId});
        var notaAprobacion = curso.notaAprobacion;

        //buscamos si el curso posee un trabajo final
        if(curso.trabajoFinal) {

            notaObtenida = notaPrueba;
            
            var desarrolloTrabajo = Desarrollos.findOne({ $and: [{cursoId: curso._id}, {userId: atributosDesarrollo.userId}, {tipo: "trabajo"}] });
            
            if(desarrolloTrabajo) {
                
                var notaTrabajo = Number(desarrolloTrabajo.notaObtenida);

                var ponderacionTrabajo = Number(curso.trabajoFinal.ponderacionPruebaFinal);
                var ponderacionPrueba = Number(100-ponderacionTrabajo);

                var pruebaPonderada = (notaPrueba*ponderacionPrueba)/100;
                var trabajoPonderado = (notaTrabajo*ponderacionTrabajo)/100;

                notaObtenida = Math.round((pruebaPonderada+trabajoPonderado)*100)/100;
                
            }

        } else {
            notaObtenida = notaPrueba;
        }
        
        if(notaObtenida>=notaAprobacion)
            aprobado = true;

        Desarrollos.update(atributosDesarrollo.desarrolloId, { $set: {
            puntosObtenidos: puntajeObtenido,
            notaObtenida: notaObtenida,
            fechaRevision: new Date(),
            aprobado: aprobado,
            enEspera: false
        }});

        //envia notificación y email al alumno

        return {
            _id: curso._id
        };
    },

    realizarEvaluacionTrabajo: function(atributosDesarrollo) {
        check(Meteor.userId(), String);
        check(atributosDesarrollo, {
            desarrolloId: String,
            cursoId: String,
            userId: String,
            trabajoFinalId: String,
            notaTrabajoFinal: String,
            enEspera: Boolean,
        });

        var notaTrabajo = Number(atributosDesarrollo.notaTrabajoFinal);

        var notaObtenida = 0;
        var aprobado = false;

        var curso = Cursos.findOne({_id: atributosDesarrollo.cursoId});
        var notaAprobacion = curso.notaAprobacion;

        //buscamos si el curso posee una prueba final
        if(curso.pruebaFinal) {
            //si es así preguntamos si el estudiante tiene un desarrollo de esa prueba
            var desarrolloPrueba = Desarrollos.findOne({ $and: [{cursoId: curso._id}, {userId: atributosDesarrollo.userId}, {tipo: "prueba"}] });

            //si no se ha realizado la prueba, no se aprueba el curso pero se guarda la nota del trabajo
            notaObtenida = notaTrabajo;

            //si es así obtenemos la nota de esa prueba para promediarla con el trabajo
            if(desarrolloPrueba){
                var notaPrueba = Number(desarrolloPrueba.notaObtenida);

                var ponderacionTrabajo = Number(curso.trabajoFinal.ponderacionPruebaFinal);
                var ponderacionPrueba = Number(100-ponderacionTrabajo);

                var pruebaPonderada = (notaPrueba*ponderacionPrueba)/100;
                var trabajoPonderado = (notaTrabajo*ponderacionTrabajo)/100;

                notaObtenida = Math.round((pruebaPonderada+trabajoPonderado)*100)/100;
            }

        } else {
            notaObtenida = notaTrabajo;
        }

        if(notaObtenida>=notaAprobacion)
            aprobado = true;

        Desarrollos.update(atributosDesarrollo.desarrolloId, { $set: {
            notaObtenida: notaObtenida,
            fechaRevision: new Date(),
            aprobado: aprobado,
            enEspera: false
        }});

        //envia notificación y email al alumno

        return {
            _id: curso._id
        };
    }
});

iniciarDesarrolloModulo = function(modulo) {
    var user = Meteor.user();
    
    var desarrolloIngresado = Desarrollos.findOne({ $and: [{userId: user._id}, {moduloId: modulo._id}]});
    
    if(!desarrolloIngresado) {
        if (modulo.userId !== user._id) {
            Desarrollos.insert({
                userId: user._id,
                moduloId: modulo._id,
                nombreUsuario: user.username,
                fechaInicio: new Date()
            });
        }
    }
};

finalizarDesarrolloModulo = function(modulo) {
    var user = Meteor.user();
    
    if (modulo.userId !== user._id) {
        
        var desarrolloIngresado = Desarrollos.findOne({ $and: [{userId: user._id}, {moduloId: modulo._id}]});
    
        if(!desarrolloIngresado.aprobado) {
            Desarrollos.update(desarrolloIngresado._id, {$set: {aprobado: true, fechaTermino: new Date}});
        }
    }
};

iniciarDesarrolloCurso = function(curso) {
    var user = Meteor.user();
    
    var desarrolloIngresado = Desarrollos.findOne({ $and: [{userId: user._id}, {cursoId: curso._id}]});
    
    if(!desarrolloIngresado) {
        if (curso.userId !== user._id){
            Desarrollos.insert({
                userId: user._id,
                cursoId: curso._id,
                nombreUsuario: user.username,
                fechaInicio: new Date()
            });
        }
    }
};

finalizarDesarrolloCurso = function(curso, nota) {
    var user = Meteor.user();
    
    if(curso.userId !== user._id) {
        
        var desarrolloIngresado = Desarrollos.findOne({ $and: [{userId: user._id}, {cursoId: curso._id}]});
        
        if(!desarrolloIngresado.aprobado) {
            Desarrollos.update(desarrolloIngresado._id, {$set: {aprobado: true, notaObtenida: nota, fechaTermino: new Date}});
        
            //creamos la notificacion para el administrador en la coleccion Notificaciones del termino del curso
            crearNotificacionTermino(curso);
        }
    }
};

//No implementado actualmente
finalizarDesarrolloMeta = function(meta, nota) {
    var user = Meteor.user();
 
    Desarrollos.insert({
            userId: user._id,
            metaId: meta._id,
            nombreUsuario: user.username,
            promedioMeta: nota,
            aprobado: true,
            fechaTermino: new Date()
    });
};






