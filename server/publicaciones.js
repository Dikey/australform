Meteor.publish('areas', function() {
    return Areas.find();
});

Meteor.publish('cursos', function() {
    return Cursos.find();
});

Meteor.publish('modulos', function() {
    return Modulos.find();
});

Meteor.publish('contenidos', function() {
    return Contenidos.find();
})

Meteor.publish('comentarios', function(contenidoId) {
    check(contenidoId, String);
    return Comentarios.find({contenidoId: contenidoId});
});

/*
Meteor.publish('comentarios', function(comentarioId) {
    check(comentarioId, String);
    return Comentarios.findOne({_id: comentarioId});
});
*/

Meteor.publish('metas', function()  {
    return Metas.find();
});

Meteor.publish('notificaciones', function() {
    return Notificaciones.find({userId: this.userId, leido: false});
});

//Publica el historial de notificaciones
Meteor.publish('historialNotificaciones', function() {
    return Notificaciones.find();
});

Meteor.publish('desarrollos', function() {
    return Desarrollos.find();
});

Meteor.publish('inscripciones', function() {
    return Inscripciones.find();
});

/*
//publica la lista completa de usuarios
Meteor.publish('userList', function() {
    return Meteor.users.find();
});
*/

//publica la lista completa solo de profesores
Meteor.publish('profesorList', function() {
    return Meteor.users.find({roles: 'profesor'});
});

//publica la lista completa solo de usuarios
Meteor.publish('userList', function() {
    return Meteor.users.find({roles: 'user'});
});

Meteor.publish('imagenes', function() { 
    return Imagenes.find(); 
});

Meteor.publish('pdfs', function() { 
    return Pdfs.find(); 
});

Meteor.publish('audios', function() { 
    return Audios.find(); 
});

Meteor.publish('tests', function() {
    return Tests.find();
});

Meteor.publish('configuracion', function() {
    return Configuracion.find();
});

Meteor.publish('timers', function() {
    return Timers.find();
});

Meteor.publish(null, function() {
    return Meteor.roles.find({});
});