
if (Meteor.users.find().count() === 0) {
    
    var users = [
        {
            username: "SuperAdmin",
            email: "diegofierro@primedevelopers.cl",
            password: "diegofierro,.-2505",
            profile: {
                fullname: "Diego Fierro Fuentes",
                rut: "18.017.824-4",
                nacionalidad: "Chilena",
                direccion: "Los Arroyos 110",
                telefono: "997975697",
                escolaridad: "Universitaria Completa",
                fechaNacimiento: new Date(1992, 5, 25),
                ocupacion:"Ingeniero en Computación",
                cargo: "Desarrollador de Software",
                empresa: "Prime Developers"
            },
            roles:['admin']
        },
        {
            username: "Admin",
            email: "constanzaperezr@yahoo.es",
            password: "constanzaformaustral",
            profile: {
                fullname: "Constanza Perez",
                rut: "",
                nacionalidad: "Chilena",
                direccion: "",
                telefono: "",
                escolaridad: "",
                fechaNacimiento: "",
                ocupacion: "",
                cargo: "",
                empresa: ""
            },
            roles:['admin']
        }
      ];
  
    _.each(users, function (user) {
        var id;
  
        id = Accounts.createUser(user);
  
        if (user.roles.length > 0) {
            // Need _id of existing user record so this call must come
            // after `Accounts.createUser` or `Accounts.onCreate`
            Roles.addUsersToRoles(id, user.roles);
        }
    }); 
}

if (Configuracion.find().count() === 0) {
    Configuracion.insert({
        datos: 'datos',
        descripcion: 'descripción',
        objetivo: 'objetivo',
        curso: 'curso',
        modulo: 'módulo',
        contenido: 'contenido',
        categoria: 'categoría',
        usuario: 'usuario',
        profesor: 'profesor'
    });
}


/*

if (Cursos.find().count() === 0) {
    Cursos.insert({
        nombreCurso: 'Estudios I',
        nombreArea: 'Finanzas'
    });
    
    Cursos.insert({
        nombreCurso: 'Calculo I',
        nombreArea: 'Finanzas'
    });
    
     Cursos.insert({
        nombreCurso: 'Calculo II',
        nombreArea: 'Finanzas'
    });
    
    Cursos.insert({
        nombreCurso: 'Uoac',
        nombreArea: 'Computación'
    });
    
    Cursos.insert({
        nombreCurso: 'Estructuras',
        nombreArea: 'Finanzas'
    });
}

if (Modulos.find().count() === 0) {
    Modulos.insert({
        nombreModulo: 'Introducción a las encuestas',
        nombreCurso: 'Estudios I',
        requisitoModulo: null,
        aprobado: true,
        contenidos: [],
        usuarios: []
    });
    
    Modulos.insert({
        nombreModulo: 'Análisis de datos',
        nombreCurso: 'Estudios I',
        requisitoModulo: 'Introducción a las encuestas',
        aprobado: false,
        contenidos: [],
        usuarios: []
    });
    
    Modulos.insert({
        nombreModulo: 'Análisis de datos II',
        nombreCurso: 'Estudios I',
        requisitoModulo: 'Análisis de datos',
        aprobado: false,
        contenidos: [],
        usuarios: []
    });
    
    Modulos.insert({
        nombreModulo: 'Algebra',
        nombreCurso: 'Calculo II',
        requisitoModulo: null,
        aprobado: false,
        contenidos: [],
        usuarios: []
    });
    
    Modulos.insert({
        nombreModulo: 'Redes',
        nombreCurso: 'Uoac',
        requisitoModulo: null,
        aprobado: false,
        contenidos: [],
        usuarios: []
    });
    
    Modulos.insert({
        nombreModulo: 'Router',
        nombreCurso: 'Uoac',
        requisitoModulo: 'Redes',
        aprobado: false,
        contenidos: [],
        usuarios: []
    });
    
    Modulos.insert({
        nombreModulo: 'Servidor',
        nombreCurso: 'Uoac',
        requisitoModulo: 'Router',
        aprobado: false,
        contenidos: [],
        usuarios: []
    });   
}

*/





